/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov (Главная страница Поставщика http://test.platformasoft.ru)
 */
public class SupplierMainPage {
    
    /*
     * xpath locators
     */
    
    //Поле поиска на главной странице поставщика
    public static final String SearchField = "//*[@name='q']";
    //Кнопка "Поиск" на главной странице поставщика
    public static final String SearchButton = "//*[@name='simple-search']";
    
    //Ссылка "Подать заявку" в найденной закупке
    public static final String SubmitProposal = "//*[@class='ps-grid-action action-request-add']";
    
    
}
