/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov
 */
public class SummungResultsPage {
    
     //Блок "Сведения о заявках"
    //Столбец "Решение по заявке"
    //Первая завяка (победитель)
    public static final String FirstResultOffer = "//*[@id='protocolReview-lot-requestList-0-admittance-select']";
    //Вторая завяка
    public static final String SecondResultOffer = "//*[@id='protocolReview-lot-requestList-1-admittance-select']";
    //Столбец "Причины несоответствия заявки требованиям"
    //Первая завяка (победитель)
    //Чекбокс "непредставление документов и информации"
    public static final String FirstDisparityInfCheckbox = "//*[@id='protocolReview-lot-requestList-0-notAdmittedReason-request-nonconformity-unreliable-documents-code']";
    //Чекбокс "несоответствие участника"
    public static final String FirstDisparitySupplierCheckbox = "//*[@id='protocolReview-lot-requestList-0-notAdmittedReason-request-nonconformity-participant-variance-code']";
    //Вторая завяка
    //Чекбокс "непредставление документов и информации"
    public static final String SecondDisparityInfCheckbox = "//*[@id='protocolReview-lot-requestList-1-notAdmittedReason-request-nonconformity-unreliable-documents-code']";
    //Чекбокс "несоответствие участника"
    public static final String SecondDisparitySupplierCheckbox = "//*[@id='protocolReview-lot-requestList-1-notAdmittedReason-request-nonconformity-participant-variance-code']";
    
    
    //Поле "Дополнительная информация"
    public static final String AdditionalInfoResultPage = "//*[@id='protocolReview-additionalInfo-additionalInfo']";
    
    //Поле "Протокол подведения итогов"
    public static final String ResultProtocol = "//*[@id='protocolReview-documentsBlock-protocolFinalDocument']";
    
    //Поле "Дополнительные документы"
    //Столбец "Название документа"
    public static final String ResultAdditionalDocName = "//input[@name='protocolReview[documentsBlock][documents][0][title]']";
    //Столбец "Afqk"
    public static final String ResultAdditionalDocFile = "//input[@name='protocolReview[documentsBlock][documents][0][file][upload]']";
    
    //Поле "Новое предложение, руб."
    public static final String CheckOffer = "//*[@id='trade-offerInfo-offer']";
    
    //Кнопки
    //Сохранить в черновик
    public static final String ResultsSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String ResultsSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String ResultsBackButton = "//*[@name='back']";
    
}


