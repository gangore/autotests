//Страница логина пользователя по паролю 
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;

import test.java.TestMethodBase;
import fw.Helpers;

public class LoginPagePassword {
	
	WebDriver driver;
	By userName = By.id("login-username");
	By password = By.id("login-password");
	By submit = By.name("submit");
	Helpers helper;

	public LoginPagePassword(WebDriver driver) throws Exception {
		this.driver = driver;
		this.helper = new Helpers(driver);
	}
	
	//Set user name in textbox
	public void setUserName(String strUserName){
		helper.type(strUserName, userName);
	}
		
	//Set password in password textbox
	public void setPassword(String strPassword){
		helper.type(strPassword, password);
	}
		
	//Click on login button
	public void clickLogin() throws InterruptedException{
		helper.jsClick(submit);
		}
	
	public void logout(){
		helper.open(TestMethodBase.baseUrl + "/admin/account/access/logout/");
		helper.open(TestMethodBase.demoUrl + "/logout");
	}
	
	public void openLoginPagePassword(){
		helper.open(TestMethodBase.demoUrl + "/login");
	}
	
	public void userLogin(String strUserName, String strPassword) throws InterruptedException{
		this.logout();
		this.openLoginPagePassword();
		this.setUserName(strUserName);
		this.setPassword(strPassword);
		this.clickLogin();
	}
	
}
