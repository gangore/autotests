/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
* @author emelianov (Страница подачи заявки поставщиком http://test.platformasoft.ru/login)
 */
public class SubmitProposalPage {
    
    //Строка "Дополнительные документы"
    //поле ввода наименования строки "Дополнительные документы"
    public static final String AdditionalDocumentName = "//*[@name='request[firstPart][documents][firstPartDocuments][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String AdditionalDocumentFile = "//*[@name='request[firstPart][documents][firstPartDocuments][0][file][upload]']";
    
    //Чекбокс "Участник претендует на предоставление преимуществ в соответствии со статьей 28 Федерального закона № 44-ФЗ"
     public static final String PenalLawCheckbox = "//*[@id='request-secondPart-features-features-penalLaw-penalLaw']";
     //Чекбокс "Участник претендует на предоставление преимуществ в соответствии со статьей 29 Федерального закона № 44-ФЗ"
     public static final String HandicappedOrganizationCheckbox = "//*[@id='request-secondPart-features-features-handicappedOrganization-handicappedOrganization']";
     //Чекбокс "Декларация о принадлежности участника к субъектам малого предпринимательства или социально ориентированным некоммерческим организациям"
     public static final String Declarationheckbox = "//*[@id='request-secondPart-features-features-ipBidding-ipBidding']";
    //Чекбокс "Документы, подтверждающие соответствие участника закупки и (или) предлагаемых им товара, работы или услуги условиям, запретам и ограничениям, 
    //установленным заказчиком в соответствии со статьей 14 Федерального закона 44-ФЗ"
     public static final String ForeignOrganizationCheckbox = "//*[@id='request-secondPart-features-features-foreignOrganization-foreignOrganization']";

     //Блок "Документы, подтверждающие соответствие участника электронного аукциона требованиям к участникам аукциона, установленным заказчиком в документации об электронном аукционе"
     
     //Строка "Документы, подтверждающие соответствие участника требованиям, установленным в соответствии с пунктом 1 части 1 и частью 2 статьи 31 Федерального закона №44-ФЗ"
    //поле ввода наименования строки "Документы"
    public static final String FirstItemName = "//input[@name='request[secondPart][documents][confirm-participant-compliance][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String FirstItemFile = "//input[@name='request[secondPart][documents][confirm-participant-compliance][0][file][upload]']";
    
     //Декларация о соответствии участника требованиям, установленным в соответствии с пунктами 3-9 части 1 статьи 31 Федерального закона 44-ФЗ
    //поле ввода наименования строки "Документы"
    public static final String ThirdItemName = "//input[@name='request[secondPart][documents][confirm-participant-compliance][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String ThirdItemFile = "//input[@name='request[secondPart][documents][conformity-declaration][0][file][upload]']";
    
     //Копии документов, подтверждающих соответствие товара, работы или услуги требованиям в соответствии с пунктом 3 части 5 статьи 66 Федерального закона №44-ФЗ
    //поле ввода наименования строки "Документы"
    public static final String CopyDocumentsName = "//input[@name='request[secondPart][documents][confirm-requirements][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String  CopyDocumentsFile = "//input[@name='request[secondPart][documents][confirm-requirements][0][file][upload]']";
    
     //Решение об одобрении или о совершении крупной сделки 
    //поле ввода наименования строки "Документы"
    public static final String ApprovalDealingsName = "//input[@name='request[secondPart][documents][approve-large-transactions][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String  ApprovalDealingsFile = "//input[@name='request[secondPart][documents][approve-large-transactions][0][file][upload]']";
    
     //Идентификационный номер налогоплательщика (при наличии) учредителей, членов коллегиального исполнительного органа, лица, исполняющего функции единоличного исполнительного органа участника
    //поле ввода наименования строки "Документы"
    public static final String InnFieldName = "//input[@name='request[secondPart][documents][founders-inn][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String  InnFieldFile = "//input[@name='request[secondPart][documents][founders-inn][0][file][upload]']";
    
    //Иные документы
    //поле ввода наименования строки "Документы"
    public static final String OtherDocumentsdName = "//input[@name='request[secondPart][documents][other-documents][0][title]']";
    //Кнопка "Обзор" в столбце "Файл" строки "Дополнительные документы"
    public static final String  OtherDocumentsFile = "//input[@name='request[secondPart][documents][other-documents][0][file][upload]']";
    
    
    // Кнопка "Сохранить в черновик"
    public static final String  SaveDraftButton = "//input[@name='saveDraft']";
    
     // Кнопка "Подписать и отправить"
    public static final String  SubmitButton = "//input[@name='sign']";
    
     // Кнопка "Вернуться в реестр"
    public static final String  ReturnButton = "//button[@name='back']";
}