/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Страница расторжения контракта
 */
public class RescissionContractPage {
    
    //Поле "Тип"
     public static final String RescissionReasonField = "//*[@id='rescission-rescissionReason']";
     
     
     //Строка "Документы"
    //Столбец "Название документа"
    public static final String RescissionDocName = "//input[@name='rescission[rescissionDocuments][0][title]']";
    //Столбец "Afqk"
    public static final String RescissionDocFile = "//input[@name='rescission[rescissionDocuments][0][file][upload]']";
     
     //Кнопки
    //Подписать и отправить
     public static final String RescissionSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String RescissionBackButton = "//*[@name='back']";
}
