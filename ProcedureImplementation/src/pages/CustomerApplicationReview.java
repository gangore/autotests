/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;
/**
 *
 * @author emelianov
 */
public class CustomerApplicationReview {
    
    //Блок "Сведения о заявках"
    //Выпадающий список в столбце "Решение о допуске
    //Для первой заявки
    public static final String SolutionFirstProposal = "//*[@id='protocolReview-lot-requestList-0-admittance-select']";
    //Для второй заявки
    public static final String SolutionSecondProposal = "//*[@id='protocolReview-lot-requestList-1-admittance-select']";
    
    //Блок "дополнительная информация
    public static final String AdditionalInformationField = "//*[@id='protocolReview-additionalInfo-additionalInfo']";
    
    //Блок"Документы"
    //Протокол рассмотрения заявок
    public static final String ProtocolRewiewField = "//*[@id='protocolReview-documentsBlock-protocolDocument']";
    //Приложение к протоколу рассмотрения заявок
    public static final String ProtocolRewiewSupplementField = "//*[@id='protocolReview-documentsBlock-applicationToProtocolDocument']";
    
    //Кнопки
    //Сохранить в черновик
    public static final String ReviewSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String ReviewSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String ReviewBackButton = "//*[@name='back']";
}
