/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov (Страница входа по логину-паролю http://test.platformasoft.ru/login)
 */
public class LoginPage {
    
    /*
     * xpath locators
     */
    
    //Поле для ввода логина пользователя
    public static final String UserNameField = "//*[@id='login-username']";
    //Поле для ввода пароля пользователя
    public static final String UserPswdField = "//*[@id='login-password']";
    //Кнопка "Войти"
    public static final String SubmitLoginButton = "//*[@name='submit']";
    
}
