/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Страница торгов
 */
public class TradePage {
    
    //Блок "Сведения о предложении участника"
    //Поле "Диапазон допустимого ценового предложения с учетом шага цены"
    //Минимальная 
    public static final String OfferWithStepMin = "//*[@id='trade-offerInfo-offerWithStep']/a[1]";
    //Максимальная 
    public static final String OfferWithStepMax = "//*[@id='trade-offerInfo-offerWithStep']/a[2]";
    
    //Поле "Диапазон допустимого ценового предложения без учета шага цены"
    //Минимальная 
    public static final String OfferWithoutStepMin = "//*[@id='trade-offerInfo-offerWithoutStep']/a[1]";
    //Максимальная 
    public static final String OfferWithoutStepMax = "//*[@id='trade-offerInfo-offerWithoutStep']/a[2]";
    
    //Поле "Новое предложение, руб."
    public static final String CheckOffer = "//*[@id='trade-offerInfo-offer']";
    
    //Кнопки
    //Закрыть окно
    public static final String CloseTradeWindow = "//*[@id='closeBtn']";
    //Обновить
    public static final String RefreshTradeWindow = "//*[@id='refreshBtn']";
    //Подписать и отправить
    public static final String SubmitOffer = "//*[@id='saveOfferBtn']";
}
