/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Подписание контракта участником
 */
public class ContractSignSupplierPage {
    
     
    //Строка "Требование обеспечения контракта"
    //Столбец "Название документа"
    public static final String SupplierSignContarctName = "//input[@name='contract[contractInfo][executionRequirementDocuments][0][title]']";
    //Столбец "Afqk"
    public static final String SupplierSignContarctFile = "//input[@name='contract[contractInfo][executionRequirementDocuments][0][file][upload]']";
    
     //Строка "Иные документы"
    //Столбец "Название документа"
    public static final String SupplierSignOtherDocName = "//input[@name='contract[documentsInfo][supplierDocuments][0][title]']";
    //Столбец "Файл"
    public static final String SupplierSignOtherDocFile = "//input[@name='contract[documentsInfo][supplierDocuments][0][file][upload]']";
    
    //Кнопки
    //Сохранить в черновик
    public static final String SupplierSignSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String SupplierSignSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String SupplierSignBackButton = "//*[@name='back']";
    
}
