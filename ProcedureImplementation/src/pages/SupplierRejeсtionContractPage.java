/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Страница формирования протокола разногласий участником
 */
public class SupplierRejeсtionContractPage {
    
    //Строка "Протокол разногласий"
    //Столбец "Название документа"
    public static final String SupplierRejeсtionProtocolName = "//input[@name='contractRejection[documents][0][title]']";
    //Столбец "Файл"
    public static final String SupplierRejeсtionProtocolFile = "//input[@name='contractRejection[documents][0][file][upload]']";
    
    
    //Кнопки
    //Сохранить в черновик
    public static final String SupplierRejeсtionSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String SupplierRejeсtionSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String SupplierRejeсtionBackButton = "//*[@name='back']";
    
}
