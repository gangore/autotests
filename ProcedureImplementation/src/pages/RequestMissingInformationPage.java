/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Страница "Запрос недостающих сведений"
 */
public class RequestMissingInformationPage {
    
    //Поле "Недостающие сведения по контракту"
    public static final String RequestMissingInformationField = "//*[@id='informationRequest-information']";
    //Поле "Недостающие сведения должны быть предоставлены не позднее"
    public static final String RequestMissingInformationTime = "//*[@id='informationRequest-executionToDateTime']";
    
    
    //Кнопки
    //Сохранить в черновик
    public static final String RequestMissingInformationSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String RequestMissingInformationSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String RequestMissingInformationBackButton = "//*[@name='back']";
    
}
