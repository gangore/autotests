/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov
 */
public class ContractFormationPage {
    
     
    //Строка "Файл проекта контракта"
    public static final String ContractFileField = "//*[@id='contract-contractInfo-contractDocument']";
    
    //Блок "иные документы по контракту
    //Столбец "Название документа"
    public static final String FormationOtherDocName = "//input[@name='contract[documentsInfo][customerDocuments][0][title]']";
    //Столбец "Файл"
    public static final String FormationOtherDocFile = "//input[@name='contract[documentsInfo][customerDocuments][0][file][upload]']";
    
    //Кнопки
    //Сохранить в черновик
    public static final String FormationResultsSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String FormationResultsSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String FormationResultsBackButton = "//*[@name='back']";
    
}
