/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

/**
 *
 * @author emelianov Страница "отказ от заключения контракта"
 */
public class ContractRefusePage {
    
    //Поле "Основание отказа от заключения контракта"
    public static final String ReasonRefuseField = "//*[@id='contractRefuse-refuseStatus']";
    //Поле "Дата и время составления протокола"
    public static final String RefuseProtocolTime = "//*[@id='contractRefuse-composeDateTime']";
    
    //Строка "Документы по отказу от заключения ко;нтракта"
    //Столбец "Название документа"
    public static final String RefuseDocName = "//input[@name='contractRefuse[documents][0][title]']";
    //Столбец "Файл"
    public static final String RefuseDocFile = "//input[@name='contractRefuse[documents][0][file][upload]']";
    
    
    //Кнопки
    //Сохранить в черновик
    public static final String RefuseSaveButton = "//*[@name='saveDraft']";
    //Подписать и отправить
     public static final String RefuseSubmitButton = "//*[@name='signAndSubmit']";
     //Вернуться
      public static final String RefuseBackButton = "//*[@name='back']";
    
}
