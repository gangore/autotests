package testMethods;

import test.java.TestMethodBase;

import static org.testng.Assert.assertTrue;

import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test; 

public class testMethods extends TestMethodBase 
{
	
	@Test
	public void checkServerTime() {
		
		//TestMethodBase s1 = new TestMethodBase();
		int howManySecondTolerance = 3; //допустимое отклонение в секундах серверного времени от московского 
		
		Calendar c = Calendar.getInstance(); 
		int year = c.get(Calendar.YEAR); 
		int month = c.get(Calendar.MONTH)+1; 
		int day = c.get(Calendar.DAY_OF_MONTH);
		int hours = c.get(Calendar.HOUR_OF_DAY);
		int minutes = c.get(Calendar.MINUTE);
		int secondes = c.get(Calendar.SECOND);

		open(demoUrl);
		//driver.get("http://base.gz.lot-online.ru/");
		/*System.out.println(year);
		System.out.println(month);
		System.out.println(day);*/
		
		String serverDataTime = driver.findElement(By.className("pull-left")).getText();
		//print(serverDataTime);
		int serverDay = Integer.parseInt(serverDataTime.substring(17,19));
		int serverMonth = Integer.parseInt(serverDataTime.substring(20,22));
		int serverYear = Integer.parseInt(serverDataTime.substring(23,27));
		int serverHour = Integer.parseInt(serverDataTime.substring(28,30));
		int serverMinutes = Integer.parseInt(serverDataTime.substring(31,33));
		int serverSeсondes = Integer.parseInt(serverDataTime.substring(34,36));
		
		/*System.out.println(serverYear);
		System.out.println(serverMonth);
		System.out.println(serverDay);
		System.out.println(serverHour);
		System.out.println(serverMinutes);
		System.out.println(serverSeсondes);*/
		
		//переводим время в секунды для сравнения
		int serverSeconds = serverHour*360 + serverMinutes*60 + serverSeсondes;  
		int secondsNow = hours*360 + minutes*60 + secondes;
		
		//проверка дат
		assertTrue(serverDay == day);
		assertTrue(serverMonth == month);
		assertTrue(serverYear == year);
		
		System.out.println("Разница между серверным и системным временем \"" + (serverSeconds-secondsNow) + "\" сек");
		//проверка времени
		//серверное время должно находиться в диапазоне: меньше чем время сейчас плюс допустимое отклонение и больше чем время сейчас минус допустимое отклонение
		assertTrue((serverSeconds < secondsNow + howManySecondTolerance) & (serverSeconds > secondsNow - howManySecondTolerance));
}


}