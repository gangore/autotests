package test.java;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;


import pages.*;


public class TestMethodBase {
	
	
	LoginPagePassword loginPagePassword;
	public WebDriver driver; 
    
	//Расположение файла для загрузки, подбирается как параметр из testng.xml 
	String fileForLoadPath;
	
	//Переменные для доступа к БД
	String loginForDataBase;
	String passwordForDataBase;
	String urlSectionks_trade;
	String urlSectionks;
	String urlAuction;
	
	//Переменные с номером ЕИС организации, используются при загрузке извещений
	String customer1EisNumber;
    String customer2EisNumber;
    String customer3EisNumber;
    
    //Логины участников
	String supplierLogin1;
	String supplierLogin2;
	String supplierLogin3;   
	String supplierLogin4;    
	String supplierLogin5;
	
	//Пароли участника и заказчика
	String customerPassword;
	String supplierPassword;
	
	String operatorLogin;
	String operatorPassword;
	
	//Логины заказчиков
	String customerLogin1;
	String customerLogin2;
	String customerLogin3;
	
	String certificateInn; //Номер ИНН участника из сертификата
	String certificateSerial;
	
	String purposeOfPayment;
	String creditRequestID;
	
	String lastOfferSumm;
	String creditFilePath = "c:\\XML ММВБ\\Кредит\\creditEnrollment.txt";
	String creditSumm;
	private String procedureid;
	private String contractid;
	public static String demoUrl;
	public static String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	public String procedureNumber;
	private int result1;
	private ITestResult result;
	String emailSecureCode;

	private String orgRole;
	private String organisationName;
	private String supplierLogin0;
	private String loginForNewSupplier;
	private String passwordForNewSupplier;
	private String passwordForNewSupplierUL;
	private String passwordForNewSupplierIP;
	private String loginForNewSupplierUL;
	private String loginForNewSupplierIP;
	private String loginForNewSupplierFL;
	private String passwordForNewSupplierFL;
	private String loginForNewSupplierULIG;
	private String passwordForNewSupplierULIG;
	private String eisNumberNewCustomer;
	private String certificateUserLastName;
	private String certificateUserFirstName;
	private String certificateOgrn;
	private String certificateIssuer;
	private String certificateUserMiddleName;
	private String loginForNewCustomer;
	private String passwordForNewCustomer;
	private String baseName94;
	
	
	//SETUP---------------------------------------------------------------------------------------------------------------------------------------------------
	
	@Parameters({"myBrowser","fileForLoadPath","testEnvironment"})
	@BeforeClass
	public void setUp(String myBrowser, String fileForLoadPath, String testEnvironment) throws Exception {
		if (testEnvironment.equalsIgnoreCase("boy"))
		{
			//настройки URL для RADBOY 
			demoUrl = "http://gz.lot-online.ru";
			baseUrl = "http://base.gz.lot-online.ru";
			//настройки БД для RADBOY 
			loginForDataBase = "tester";
			passwordForDataBase = "tester";
			urlSectionks_trade = "jdbc:mysql://185.147.81.17:3004/sectionks_trade";
			urlSectionks = "jdbc:mysql://185.147.81.17:3001/sectionks";
			urlAuction = "jdbc:mysql://185.147.81.17:3002/base";
			baseName94 = "base";
			
			//Настройки юзеров для RADBOY---------------------------------------
			
			//Номер заказчика на ЕИС
			
			customer1EisNumber = "15990016415";
			customer2EisNumber = "10021200077";
			//customer1EisNumber = "01105000004";
			//customer1EisNumber = "36742236813";
			//customer1EisNumber = "15990016415";
			//customer2EisNumber = "16990019223";
		    customer3EisNumber = "16990019224";
		    eisNumberNewCustomer = null;
		    
		    
			//Учетки заказчиков
			
		    customerLogin1 = "varoncov!";
		    customerLogin2 = "мubravin1!";
			//customerLogin1 = "filonenko1!";
			//customerLogin1 = "masodov1!";
			//customerLogin2 = "lednev1!";
			
			customerPassword = "Qwerty12";
		    
		    
		    //Учетки участников торгов
			
		    supplierLogin1 = "voinoviv";
		    supplierLogin2 = "grigoryev";
		    //supplierLogin3 = "voinoviv";
			//supplierLogin4 = "glazunova";
			//supplierLogin5 = "glazunova";
		    
		    //Пароль участника
			supplierPassword = "Qwerty12";
			
			operatorLogin = "operator2";
			operatorPassword = "Qwerty12!";
		}
		
		if (testEnvironment.equalsIgnoreCase("test"))
		{
			//настройки URL для test
			demoUrl = "http://test.platformasoft.ru";
			baseUrl = "http://94-test.platformasoft.ru";
			//настройки БД для TEST
			loginForDataBase = "developer";
			passwordForDataBase = "developer";
			urlSectionks_trade = "jdbc:mysql://192.168.170.151:3306/sectionks_trade";
			urlSectionks = "jdbc:mysql://192.168.170.151:3306/sectionks";
			urlAuction =   "jdbc:mysql://192.168.170.152:3306/auction";
			baseName94 = "auction";
			//настройки юзеров для RADBOY
			//Номер заказчика и организатора
			customer1EisNumber = "2655662240";  //Заказов
		    customer2EisNumber = "16990019224";  //Varoncov Юникор      
		    customer3EisNumber = "15990015922"; //ООО "Акапулько"
		    eisNumberNewCustomer = null;
		    //Учетки участников 
			supplierLogin1 = "grigoryev";    //ЮЛ
			supplierLogin2 = "glazunova";  //ИП
			supplierLogin3 = "voinoviv";    //ИП
			supplierLogin4 = "klikaduev1!";     //ИП
			supplierLogin5 = "larinnik";
			//Пароль участника
			supplierPassword = "Qwerty12";
			//Учетки заказчиков
			customerLogin1 = "zakazov";
			customerLogin2 = "varoncov1!";
			customerPassword = "Qwerty12";
			
			operatorLogin = "operator1";
			operatorPassword = "Qwerty12";
		}
		
		if (testEnvironment.equalsIgnoreCase("demo"))
		{
			//настройки URL для test
			demoUrl = "http://demo.gz.lot-online.ru";
			baseUrl = "http://base.demo.gz.lot-online.ru";
			//настройки БД для TEST
			loginForDataBase = "developer";
			passwordForDataBase = "developer";
			urlSectionks_trade = "jdbc:mysql://77.232.63.5:3003/sectionks_trade";
			urlSectionks = "jdbc:mysql://77.232.63.5:3003/sectionks";
			urlAuction =   "jdbc:mysql://77.232.63.5:3004/auction";
			baseName94 = "auction";
			//настройки юзеров для RADBOY
			//Номер заказчика и организатора
			customer1EisNumber = "15990015908";  //Зайцев
		    customer2EisNumber = "15900010022";  //Varoncov Юникор      
		    customer3EisNumber = "15990015922"; //ООО "Акапулько" masodov1
		    eisNumberNewCustomer = null;
		    //Учетки участников 
			supplierLogin1 = "gudz123!";    //ЮЛ
			supplierLogin2 = "ipselin1!"; //ИП
			supplierLogin3 = "voinoviv";    //ИП
			supplierLogin4 = "";    //ИП
			supplierLogin5 = "";
			supplierPassword = "Qwerty12";
			//Учетки заказчиков
			customerLogin1 = "zaytsev1!";
			customerLogin2 = "varoncov1!";
			customerLogin2 = "masodov1!";
			customerPassword = "Qwerty12";
			//Учетка оператора
			operatorLogin = "operator1!";
			operatorPassword = "Qwerty12";
		}
		
		this.fileForLoadPath = fileForLoadPath;
		print("Путь к загружаемому файлу: " + this.fileForLoadPath);
		
		if (myBrowser.equalsIgnoreCase("firefox"))
	    {
			
	    ProfilesIni profile = new ProfilesIni();
		FirefoxProfile myprofile = profile.getProfile("WebDriver");        
		myprofile.setPreference("browser.download.folderList",2);
		myprofile.setPreference("browser.download.manager.showWhenStarting",false);
		myprofile.setPreference("browser.download.dir","C:\\TestDownloads");
		myprofile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/msword, application/csv, application/ris, text/csv, text/txt, application/txt, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
		this.driver = new FirefoxDriver(myprofile);
			
			/*DesiredCapabilities caps = DesiredCapabilities.firefox();
			URL hostURL = new URL("http://192.168.160.141:4444/wd/hub");
	        this.driver = new RemoteWebDriver(hostURL, caps);*/

	    this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    
	    }
	    else if (myBrowser.equalsIgnoreCase("IE"))
	    {
	    	//DesiredCapabilities caps = new DesiredCapabilities();
	    	DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
	    	//caps = DesiredCapabilities.internetExplorer();
	        caps.setCapability("ignoreZoomSetting", true);
	        // Setting attribute nativeEvents to false enable click button in IE
	        caps.setCapability("nativeEvents",false);
	    	//System.setProperty("webdriver.ie.driver", "c:\\Programs\\IEDriverServer.exe");
	        //System.setProperty("webdriver.ie.driver", "c:\\autoTest\\lib\\IEDriverServer.exe");
	        
	        this.driver = new InternetExplorerDriver();
	        
	        //URL hostURL = new URL("http://192.168.160.141:4444/wd/hub");
	        //this.driver = new RemoteWebDriver(hostURL, caps);
	        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	        this.driver.manage().window().maximize();
	    	System.out.println(driver.toString());
	    	//System.out.println(System.("webdriver.ie.driver"));
	    }
	  }
	
	@AfterMethod
	public void screenOnFail(ITestResult result) throws IOException {
	    if (result.getStatus() == ITestResult.FAILURE) 
	   {
		   getScreenShot(result);
	   }}
	@AfterMethod
	public void performingTestMethodResult (ITestResult result){
		String status;
		if (result.getStatus()==1) {status="success";} else status="fail";
		System.out.println("Статус теста " + result.getName() + ":" + status);
	}
	

	
public void eis() throws InterruptedException, AWTException {
	open("http://fks.lanit.ru/auth/pcabinet.jsp");
	//click(By.cssSelector("input[type=\"button\"]"));
	Thread.sleep(60000);
	Robot rr = new Robot();
	rr.keyPress(KeyEvent.VK_ENTER);
	rr.keyRelease(KeyEvent.VK_ENTER);
	System.out.println("Enter");
	Thread.sleep(30000);
	//waitForElementPresent(By.cssSelector("header"));
	open("https://fks.lanit.ru/44fz/priz/search.html?statuses=CF&statuses=R&statuses=M&statuses=CS&statuses=CI&statuses=F&statuses=CR&statuses=E&statuses=CA&statuses=A&searchStages=NOTICE_PREPARATION&searchStages=COMMISSION_ACTIVITIES&searchStages=NOTICE_PREPARATION_CHANGES&searchStages=APPLICATION_SUBMISSION&currentSearchStage=NOTICE_PREPARATION");
	click(By.linkText("Создать"));
	click(By.linkText("Извещение об осуществлении закупки"));
	select("Электронный аукцион", By.id("popupPlacingWaySelect"));
	click(By.cssSelector("btn"));
	click(By.id("confirm"));
	Thread.sleep(5000);
	click(By.id("confirm"));
	Thread.sleep(40000);
}	
	
	
	
protected String randomDigits(int n) throws InterruptedException {
	Random rand = new Random();
	StringBuffer randomDigits = new StringBuffer();
	for (int i=0; i<n; i++){
		int m = rand.nextInt(9);
		randomDigits.append(m);
	}
	String str = randomDigits.toString();
	return str;
}

public static boolean checkTrueOrder (LinkedList<String> elementNames){

    String previous = ""; // empty string

    for (final String current: elementNames) {
    	System.out.println(current.compareTo(previous));
        if (current.compareTo(previous) < 0)
            return false;
        previous = current; System.out.println(current);
    }
    return true;
    }

public List<String> getDateFromPageToListForCheckOrder (List<String> list, By by) throws Exception {
	
	int sizeOfElements = driver.findElements(by).size();
	List<WebElement> webElements = driver.findElements(by);
	for(int j=0; j < sizeOfElements; j++)
	{
	waitForElementPresent(by);
	String dateString = webElements.get(j).getText().substring(0, 19);
	list.add(dateString);
	//listOfEndRequesDate.add(requestDate);
	//listOfTradeStartDate.add(tradeDate);
	System.out.println(dateString);
	}
	return list;
}

public void waitForDomLoad () throws Exception {
	
	int countToRefresh=0;
	//Thread.sleep(3000);
	JavascriptExecutor js = (JavascriptExecutor)driver;
	while (!(js.executeScript("return document.readyState").equals("complete"))){
	//js.executeScript("window.addEventListener(\"load\", function(event) {alert('complete')});");
	System.out.println("wait for dom");
		Thread.sleep(3000);
		countToRefresh++;
		if (countToRefresh>20){driver.navigate().refresh();countToRefresh=0;System.out.println("Refresh");}
	}
	System.out.println("dom - ok");
	//Thread.sleep(1000);
	}


protected void checkSortOrderForDate (String fieldForCheckOrder) throws Exception {
	
	String fieldName=null;
	String cssSelectorForDate=null;
	
	
	switch (fieldForCheckOrder){
	case "Дата и время публикации": fieldName="publicationDateTime"; cssSelectorForDate=".row-publicationDateTime"; break;
	case "Дата и время окончания подачи заявок": fieldName="requestEndGiveDateTime"; cssSelectorForDate=".row-requestEndGiveDateTime"; break;
	case "Дата и время начала торгов": fieldName="conditionalHoldingDateTime"; cssSelectorForDate=".row-conditionalHoldingDateTime"; break;
	}
	
	open(demoUrl + "/procedure/catalog/?order=" + fieldName + "&dir=asc&limit=25");
	
	List<String> listOfDate = new LinkedList<String>();
	
	//Wait<WebDriver> wait = new WebDriverWait(driver, 30, 100);
	//wait.until(ExpectedConditions.(js.executeScript("return document.readyState").equals("complete")));	
	//сбор данных с первой страницы

	
	getDateFromPageToListForCheckOrder(listOfDate, By.cssSelector(cssSelectorForDate));
	
	WebDriverWait wait = new WebDriverWait(driver, 60);
	
	
	int pageNumber=2;
	String currentUrl = driver.getCurrentUrl();
	print(currentUrl);
	//Сбор дат со страниц в список если страниц больше одной
	while (elementExist(By.linkText("" + pageNumber))) {
		print("page " + pageNumber);
		
		
		//Пока урл страницы не изменится: кликаем на сл. страницу и ждем что цветовое выделение предыдущей страницы пропадет
		//если оно не пропало (подвис браузер), проверяем что УРЛ не изменился (он не изменится если предыдущий клик не отработался) и кликаем на сл. страницу еще раз
		while (currentUrl.equals(driver.getCurrentUrl())){
		jsClick(By.linkText("" + pageNumber));print("click");
		//ждать пока не исчезнет подсвеченная отметка с номером предыдущей активной страницы 
		try {
			wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//li[@class='active']/span[contains(text(),'" + (pageNumber-1) + "')]"))));
			} 
		catch (TimeoutException ignore) {
			print("ignore");
			};
		//try {wait.until(ExpectedConditions.not(ExpectedConditions.urlToBe(currentUrl)));} catch (TimeoutException ignore) {print("ignore");Thread.sleep(5000);};
		}
		
		currentUrl = driver.getCurrentUrl();

		waitForDomLoad();
		waitForElementPresent(By.cssSelector(".floatLeft.footer-inner"));
	getDateFromPageToListForCheckOrder(listOfDate, By.cssSelector(cssSelectorForDate));
	pageNumber++;
	}
	
	SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	Date previous = format.parse(listOfDate.get(0));
	
	//Проверка: последующая дата больше чем предыдущая (previous.compareTo(currentData)<=0);
    for (String current: listOfDate) {
    	Date currentData = format.parse(current);
    	System.out.println(previous);
    	System.out.println(currentData);
    	System.out.println(previous.compareTo(currentData));
    	assertTrue(previous.compareTo(currentData)<=0);
    	previous = currentData;
    }
    
    
    //проверка сортировки по убыванию
	open(demoUrl + "/procedure/catalog/?order=" + fieldName + "&dir=desc&limit=25");
	//обнуление списка
	listOfDate = new LinkedList<String>();
	
	//сбор данных с первой страницы
	getDateFromPageToListForCheckOrder(listOfDate, By.cssSelector(cssSelectorForDate));
	currentUrl = driver.getCurrentUrl();
	pageNumber=2;
	
	//Сбор дат со страниц в список
	while (elementExist(By.linkText("" + pageNumber))) {
		print("page " + pageNumber);
		
		while (currentUrl.equals(driver.getCurrentUrl())){
			jsClick(By.linkText("" + pageNumber));print("click");
			
			//ждать пока не исчезнет подсвеченная отметка с номером предыдущей активной страницы 
			try {
				wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//li[@class='active']/span[contains(text(),'" + (pageNumber-1) + "')]"))));
				} 
			catch (TimeoutException ignore) {
				print("ignore");
				};
			}
			

			currentUrl = driver.getCurrentUrl();
		waitForDomLoad();
		waitForElementPresent(By.cssSelector(".floatLeft.footer-inner"));
	getDateFromPageToListForCheckOrder(listOfDate, By.cssSelector(cssSelectorForDate));
	pageNumber++;
	}
    
    	previous = format.parse(listOfDate.get(0));
	
    //Проверка: последующая дата больше чем предыдущая (previous.compareTo(currentData)>=0)
    for (String current: listOfDate) {
    	Date currentData = format.parse(current);
    	System.out.println(previous);
    	System.out.println(currentData);
    	System.out.println(previous.compareTo(currentData));
    	assertTrue(previous.compareTo(currentData)>=0);

    	//System.out.println(listOfDate.size());
    	previous = currentData;
    }
	}



protected void checkSortOrderForContractPrice () throws Exception {
	
	open(demoUrl + "/procedure/catalog/");
	select("25", By.cssSelector(".form-control.pager-dropdown"));
	Thread.sleep(5000);
	jsClick(By.linkText("Начальная (макс.) цена контракта"));
	Thread.sleep(5000);
	
	List<WebElement> listOfWebelement = new LinkedList<WebElement>();
	int pageNumber=2;
	
	click(By.cssSelector(".row-maxSum.sortable.sortable.ps-grid-column-money"));
	listOfWebelement = driver.findElements(By.cssSelector(".row-maxSum.sortable.sortable.ps-grid-column-money"));
	LinkedList<Double> elementNames =  new LinkedList<Double>();
	

	for(int j=0; j<listOfWebelement.size(); j++)
	{
	    System.out.println(listOfWebelement.get(j).getText());
	    double s = Double.parseDouble(listOfWebelement.get(j).getText().replace(" ", ""));
	    elementNames.add(s);
	}

	while (elementExist(By.linkText("" + pageNumber))) {
		jsClick(By.linkText("" + pageNumber));
		System.out.println(pageNumber);
		waitForElementPresentRefresh(By.xpath("//li[@class='active']/span[contains(text(),'" + pageNumber + "')]"));
	
		listOfWebelement = driver.findElements(By.cssSelector(".row-maxSum.sortable.sortable.ps-grid-column-money"));
		
		for(int j=0; j < listOfWebelement.size(); j++){
			double s = Double.parseDouble(listOfWebelement.get(j).getText().replace(" ", ""));
		    elementNames.add(s);
		    System.out.println(listOfWebelement.get(j).getText());
		}
		pageNumber++;
	}
	
	Double previous = 0.0; // empty string

    for (final Double current: elementNames) {
    	assertTrue(Double.compare(previous, current) <= 0);
    	System.out.println(previous);
    	System.out.println(Double.compare(previous, current));
    	previous = current;
    }
	
    
    //проверка сортировки по возрастанию
    pageNumber=2;
    open(demoUrl + "/procedure/catalog/");
    select("25", By.cssSelector(".form-control.pager-dropdown"));
    Thread.sleep(5000);
    jsClick(By.linkText("Начальная (макс.) цена контракта"));
	//waitForElementPresent(By.cssSelector(".glyphicon.pull-right.glyphicon-sort-by-attributes-alt"));
    Thread.sleep(5000);
    jsClick(By.linkText("Начальная (макс.) цена контракта"));
    Thread.sleep(5000);
    
	listOfWebelement = driver.findElements(By.cssSelector(".row-maxSum.sortable.sortable.ps-grid-column-money"));
	elementNames =  new LinkedList<Double>();

	for(int j=0; j<listOfWebelement.size(); j++)
	{
	    System.out.println(listOfWebelement.get(j).getText());
	    double s = Double.parseDouble(listOfWebelement.get(j).getText().replace(" ", ""));
	    elementNames.add(s);
	}

	while (elementExist(By.linkText("" + pageNumber))) {
		jsClick(By.linkText("" + pageNumber));
		System.out.println(pageNumber);
		waitForElementPresentRefresh(By.xpath("//li[@class='active']/span[contains(text(),'" + pageNumber + "')]"));
	
		listOfWebelement = driver.findElements(By.cssSelector(".row-maxSum.sortable.sortable.ps-grid-column-money"));
		
		for(int j=0; j < listOfWebelement.size(); j++){
			double s = Double.parseDouble(listOfWebelement.get(j).getText().replace(" ", ""));
		    elementNames.add(s);
		    System.out.println(listOfWebelement.get(j).getText());
		}
		pageNumber++;
	}
	
	previous = elementNames.get(0);
	System.out.println(elementNames.get(0));
	
    for (final Double current: elementNames) {
    	assertTrue(Double.compare(previous, current) >= 0);
    	System.out.println(previous);
    	System.out.println(Double.compare(previous, current));
    	previous = current;
    }
	}



protected void checkSortOrderForOrg () throws Exception {
	
	open(demoUrl + "/procedure/catalog/");
	select("25", By.cssSelector(".form-control.pager-dropdown"));
	Thread.sleep(5000);
	//int column=2;
	
	/*
	switch (filedForOrder){
	case "Начальная (макс.) цена контракта": column = 2; break;
	case "Организатор закупки": column = 3; break;
	case "Дата и время публикации": column = 4; break;
	case "Дата и время окончания подачи заявок": column = 5; break;
	case "Дата и время начала торгов": column = 6; break;
	}
	*/

	jsClick(By.linkText("Организатор закупки"));
	Thread.sleep(5000);
    //int i = 1;
	int pageNumber=2;
	//String previous = "";
	
	
	List<WebElement> listOfWebelement = new LinkedList<WebElement>();
	listOfWebelement = driver.findElements(By.cssSelector(".row-customer"));
	LinkedList<String> elementNames =  new LinkedList<String>();
	
	for(int j=0; j<listOfWebelement.size(); j++){
	    String s = listOfWebelement.get(j).getText();
	    elementNames.add(s);
	    System.out.println(listOfWebelement.get(j).getText());
	}
	//checkTrueOrder(elementNames);
	
	
	while (elementExist(By.linkText("" + pageNumber))) {
		jsClick(By.linkText("" + pageNumber));
		System.out.println(pageNumber);
		Thread.sleep(2000);
		//waitForElementPresent(By.linkText("" + pageNumber));
		//waitForElementPresentRefresh(By.xpath("//li[@class='active']/span[contains(text(),'" + pageNumber + "')]"));
	
		listOfWebelement = driver.findElements(By.cssSelector(".row-customer"));
		
		for(int j=0; j < listOfWebelement.size(); j++){
		    String s = listOfWebelement.get(j).getText();
		    elementNames.add(s);
		    print(listOfWebelement.get(j).getText());
		}
		pageNumber++;
	}
	assertTrue(checkTrueOrder(elementNames));
	
	
	//Проверка сортировки по убыванию
	open(demoUrl + "/procedure/catalog/");
    select("25", By.cssSelector(".form-control.pager-dropdown"));
    Thread.sleep(5000);
    jsClick(By.linkText("Организатор закупки"));
	//waitForElementPresent(By.cssSelector(".glyphicon.pull-right.glyphicon-sort-by-attributes-alt"));
    Thread.sleep(5000);
    jsClick(By.linkText("Организатор закупки"));
    Thread.sleep(5000);
    waitForElementPresent(By.cssSelector(".glyphicon.pull-right.glyphicon-sort-by-attributes-alt"));
	
	pageNumber=2;

	//listOfWebelement = new LinkedList<WebElement>();
	listOfWebelement = driver.findElements(By.cssSelector(".row-customer"));
	elementNames =  new LinkedList<String>();
	
	for(int j=0; j<listOfWebelement.size(); j++){
	    String s = listOfWebelement.get(j).getText();
	    elementNames.add(s);
	    print(listOfWebelement.get(j).getText());
	}
	
	while (elementExist(By.linkText("" + pageNumber))) {
		jsClick(By.linkText("" + pageNumber));
		System.out.println(pageNumber);
		Thread.sleep(2000);
		//waitForElementPresent(By.linkText("" + pageNumber));
		//waitForElementPresentRefresh(By.xpath("//li[@class='active']/span[contains(text(),'" + pageNumber + "')]"));
	
		listOfWebelement = driver.findElements(By.cssSelector(".row-customer"));
		
		for(int j=0; j < listOfWebelement.size(); j++){
		    String s = listOfWebelement.get(j).getText();
		    elementNames.add(s);
		    print(listOfWebelement.get(j).getText());
		}
		pageNumber++;
	}
	
	String previous = elementNames.get(0); // empty string
	print("Элемент 0 " + elementNames.get(0));
    for (final String current: elementNames) {
    	System.out.println(current.compareTo(previous));
        assertTrue((current.compareTo(previous) <= 0));
        previous = current; System.out.println(current);
    }
    
	
	
	
	
	
	
	/*
	
	//проверка сортировки на первой странице 
	while (i <= driver.findElements(By.cssSelector(".row-customer")).size()) {
		System.out.println(i + " " + driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText());
		System.out.println(driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText().compareTo(previous));
		assertTrue(driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText().compareTo(previous) >= 0); 
		previous = driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText();
		i++;
	}
	
	i = 1;
	
	//Если есть еще страница, выполняется переход, проверяется сортировка
	while (elementExist(By.linkText("" + pageNumber))) {
		jsClick(By.linkText("" + pageNumber));
		System.out.println(pageNumber);
		waitForElementPresent(By.xpath("//li[@class='active']/span[contains(text(),'" + pageNumber + "')]"));
	
	while (i <= driver.findElements(By.cssSelector(".row-customer")).size()) {
		waitForElementPresent(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]"));
		System.out.println(i + " " + driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText());
		System.out.println(driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText().compareTo(previous));
		assertTrue(driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText().compareTo(previous) >= 0);
		previous = driver.findElement(By.xpath("//div[@id='procedureCatalog']/div[2]/table/tbody/tr[" + i + "]/td[" + column + "]")).getText();
		i++;
	}
	pageNumber++;
	i = 1;
	}*/
	}




protected void LoadCoopProcedure() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 15);
    
    String purchaseName = "Закупка компьютеров совместная для цеха №" + randomDigits(5);
    String maxPrice = "10000.00"; //1000.99
    
    LocalDateTime time = LocalDateTime.now();
	String dateToday = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +  "T" + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"))+"+03:00";
	String dateEndZayav = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T23:00:00+03:00";
	System.out.println(dateToday);

	String IDPack = randomDigits(36);
    this.procedureNumber = randomDigits(19);
    System.out.println("Номер процедуры: " + procedureNumber);
    
	//String procedureXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://zakupki.gov.ru/oos/integration/1 file:///C:/Users/Vasily/Downloads/%d0%a1%d1%85%d0%b5%d0%bc%d1%8b%20%d0%b8%d0%bd%d1%82%d0%b5%d0%b3%d1%80%d0%b0%d1%86%d0%b8%d0%b8%20%d0%a4%d0%9a%d0%a1%20v4.3.100%2001.07.2014/%d0%a1%d1%85%d0%b5%d0%bc%d1%8b%20%d0%b8%d0%bd%d1%82%d0%b5%d0%b3%d1%80%d0%b0%d1%86%d0%b8%d0%b8%20%d0%a4%d0%9a%d0%a1%20v4.3.100%2001.07.2014/fcsIntegration.xsd\"><index><id>" + IDPack + "</id>  <sender>OOC</sender>  <receiver>ETP_MMVB</receiver>  <createDateTime>2016-03-02T17:50:57.16+03:00</createDateTime>  <objectType>EF</objectType><objectId>" + procedureNumber + "</objectId>  <indexNum>1</indexNum>  <signature type=\"CAdES-BES\"/>  <mode>PROD</mode>  </index>   <data schemeVersion=\"4.3\">  <ns2:id>2170877</ns2:id>  <ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber>  <ns2:docPublishDate>2016-03-01T17:50:57.16+03:00</ns2:docPublishDate> <ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=315777553332177</ns2:href>  <ns2:printForm>  <ns2:url>http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=2170819</ns2:url> <ns2:signature type=\"CAdES-BES\"> TulJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBV0haVHRzYWYzWXIyY0RpT3R4RWpLSGxVTm5mUUhwY1RtZHFUVDNHV0hDYXNndTFmTkdzb05DbmxRR0JtRWZXUmpNaDQ2K0N2M0xlMFd1ZHphWjQ1ZFE9PQ== </ns2:signature> </ns2:printForm> <ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Самарский водоканал</ns2:fullName> <ns2:postAddress>Российская Федерация, 183006, Мурманская обл, пр. Ленина, д. 75</ns2:postAddress> <ns2:factAddress>Российская Федерация, 183038, Мурманская обл, Мурманск г, К.Маркса, 3, - </ns2:factAddress> </ns2:responsibleOrg> <ns2:responsibleRole>ORA</ns2:responsibleRole> <ns2:responsibleInfo> <ns2:orgPostAddress>Российская Федерация, 183006, Мурманская обл, пр. Ленина, д. 75</ns2:orgPostAddress> <ns2:orgFactAddress>Российская Федерация, 183038, Мурманская обл, Мурманск г, К.Маркса, 3, - </ns2:orgFactAddress> <ns2:contactPerson> <ns2:lastName>Сурков</ns2:lastName> <ns2:firstName>Дмитрий</ns2:firstName> <ns2:middleName>Романович</ns2:middleName> </ns2:contactPerson> <ns2:contactEMail>goszakaz@gov-murman.ru</ns2:contactEMail>  <ns2:contactPhone>8-8152-486592</ns2:contactPhone> <ns2:contactFax>8-8152-693512</ns2:contactFax> </ns2:responsibleInfo> </ns2:purchaseResponsible> <ns2:placingWay> <ns2:code>EA44</ns2:code> <ns2:name>Электронный аукцион</ns2:name> </ns2:placingWay> <ns2:ETP> <ns2:code>ETP_MMVB</ns2:code> <ns2:name>ЭТП РАД</ns2:name> <ns2:url>http://www.lot-online.ru</ns2:url> </ns2:ETP> <ns2:procedureInfo> <ns2:collecting> <ns2:startDate>2016-01-16T12:30:00+03:00</ns2:startDate> <ns2:place>адрес площадки в сети Интернет:http://www.lot-online.ru</ns2:place> <ns2:order>В соответствии с частью 1 раздела 3 тома 1 документации о проведении аукциона в электронной форме. </ns2:order>  <ns2:endDate>" + dateEndZayav + "</ns2:endDate>  </ns2:collecting>  <ns2:scoring>  <ns2:date>" + dateEndZayav + "</ns2:date>  </ns2:scoring>  <ns2:bidding>  <ns2:date>" + dateEndZayav + "</ns2:date>  </ns2:bidding>  </ns2:procedureInfo>  <ns2:lot>  <ns2:maxPrice>"+ maxPrice +"</ns2:maxPrice>  <ns2:currency>  <ns2:code>RUB</ns2:code>  <ns2:name>Российский рубль</ns2:name>  </ns2:currency> <ns2:financeSource>Городская администрация</ns2:financeSource> <ns2:quantityUndefined>false</ns2:quantityUndefined> <ns2:customerRequirements> <ns2:customerRequirement> <ns2:customer> <ns2:regNum>" + Zak + "</ns2:regNum> <ns2:fullName>Государственное областное бюджетное учреждение здравоохранения \"Мурманский областной психоневрологический диспансер\" </ns2:fullName> </ns2:customer> <ns2:maxPrice>"+ maxPrice +"</ns2:maxPrice> <ns2:deliveryPlace>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме.</ns2:deliveryPlace> <ns2:deliveryTerm>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме. </ns2:deliveryTerm> <ns2:applicationGuarantee> <ns2:amount>99.99</ns2:amount> <ns2:part>1.0</ns2:part> <ns2:procedureInfo>Участник перечисляет сумму, указанную в пункте 5.1 тома 2 документации о проведении аукциона в электронной форме, на счет, открытый электронной торговой площадкой, адрес площадки в сети Интернет: http://www.lot-online.ru. </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:applicationGuarantee> <ns2:contractGuarantee> <ns2:amount>48137.32</ns2:amount> <ns2:part>10.0</ns2:part> <ns2:procedureInfo>В соответствии с п. 6 тома 2 документации о проведении аукциона в электронной форме </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:contractGuarantee> </ns2:customerRequirement> <ns2:customerRequirement> <ns2:customer> <ns2:regNum>" + Zak1 + "</ns2:regNum> <ns2:fullName>Государственное областное бюджетное учреждение здравоохранения \"Мурманский областной психоневрологический диспансер\" </ns2:fullName> </ns2:customer> <ns2:maxPrice>"+ maxPrice +"</ns2:maxPrice> <ns2:deliveryPlace>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме.</ns2:deliveryPlace> <ns2:deliveryTerm>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме. </ns2:deliveryTerm> <ns2:applicationGuarantee> <ns2:amount>99.99</ns2:amount> <ns2:part>1.0</ns2:part> <ns2:procedureInfo>Участник перечисляет сумму, указанную в пункте 5.1 тома 2 документации о проведении аукциона в электронной форме, на счет, открытый электронной торговой площадкой, адрес площадки в сети Интернет: http://www.lot-online.ru. </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:applicationGuarantee> <ns2:contractGuarantee> <ns2:amount>48137.32</ns2:amount> <ns2:part>10.0</ns2:part> <ns2:procedureInfo>В соответствии с п. 6 тома 2 документации о проведении аукциона в электронной форме </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:contractGuarantee> </ns2:customerRequirement> </ns2:customerRequirements> <ns2:purchaseObjects> <ns2:purchaseObject> <ns2:OKPD> <ns2:code>24.42.13.744</ns2:code> <ns2:name>Препараты, влияющие на процессы обмена, прочие</ns2:name> </ns2:OKPD> <ns2:name>Поставка средств, влияющих на липидно-холестериновый обмен (Фосфолипиды) для нужд учреждений здравоохранения Мурманской области. </ns2:name> <ns2:customerQuantities> <ns2:customerQuantity> <ns2:customer> <ns2:regNum>" + Zak + "</ns2:regNum> <ns2:fullName>Государственное областное бюджетное учреждение здравоохранения \"Мурманский областной Дом ребенка специализированный для детей с органическим поражением центральной нервной системы с нарушением психики\" </ns2:fullName> </ns2:customer> <ns2:quantity>1</ns2:quantity> </ns2:customerQuantity> </ns2:customerQuantities> <ns2:price>"+ maxPrice +"</ns2:price> <ns2:quantity> <ns2:value>1</ns2:value> </ns2:quantity> <ns2:sum>"+ maxPrice +"</ns2:sum> </ns2:purchaseObject> <ns2:totalSum>"+ maxPrice +"</ns2:totalSum> </ns2:purchaseObjects> <ns2:preferenses> <ns2:preferense> <ns2:code/> <ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со Статьей 30 Федерального закона № 44-ФЗ)</ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Социально ориентированным некоммерческим организациям (в соответствии со Статьей 30 Федерального закона № 44-ФЗ)</ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для исполнения контракта (в соответствии со Статьей 30 Федерального закона № 44-ФЗ) </ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для исполнения контракта (в соответствии со Статьей 30 Федерального закона № 44-ФЗ) </ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров российского, белорусского и (или) казахстанского происхождения (в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014)</ns2:name> <ns2:prefValue>15</ns2:prefValue> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со Статьей 28 Федерального закона № 44-ФЗ)</ns2:name> <ns2:prefValue>5</ns2:prefValue> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Преимущество для организаций инвалидов в отношении предлагаемой цены контракта</ns2:name> <ns2:prefValue/> </ns2:preferense> </ns2:preferenses> <ns2:requirements> <ns2:requirement> <ns2:code/> <ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование о наличии финансовых ресурсов для исполнения контракта (в соответствии с пунктом 1 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование о наличии финансовых ресурсов для исполнения контракта (в соответствии с пунктом 1 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование о наличии на праве собственности или ином законном основании оборудования и других материальных ресурсов для исполнения контракта (в соответствии с пунктом 2 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование о наличии на праве собственности или ином законном основании оборудования и других материальных ресурсов для исполнения контракта (в соответствии с пунктом 2 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требования о наличии опыта работы, связанного с предметом контракта, и деловой репутации (в соответствии с пунктом 3 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требования о наличии опыта работы, связанного с предметом контракта, и деловой репутации (в соответствии с пунктом 3 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требования о наличии необходимого количества специалистов и иных работников определенного уровня квалификации для исполнения контракта (в соответствии с пунктом 4 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требования о наличии необходимого количества специалистов и иных работников определенного уровня квалификации для исполнения контракта (в соответствии с пунктом 4 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Иные дополнительные требования к участникам (в соответствии с частью 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Иные дополнительные требования к участникам (в соответствии с частью 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> </ns2:requirements> <ns2:restrictInfo>Установлено</ns2:restrictInfo> <ns2:restrictForeignsInfo/> <ns2:addInfo/> </ns2:lot> <ns2:attachments> <ns2:attachment> <ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEA</ns2:publishedContentId> <ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName> <ns2:docDescription>Аукционная документация</ns2:docDescription> <ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature> </ns2:cryptoSigns> </ns2:attachment> <ns2:attachment> <ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEB</ns2:publishedContentId> <ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName> <ns2:docDescription>Аукционная документация</ns2:docDescription> <ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature> </ns2:cryptoSigns> </ns2:attachment><ns2:attachment><ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEC</ns2:publishedContentId><ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName><ns2:docDescription>Аукционная документация</ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments></data></fcsNotificationEF>";
	String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>TEST</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:directDate>2017-03-06T13:21:29.167+03:00</ns2:directDate><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=464160000154139</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + customer1EisNumber + "</ns2:regNum><!--<ns2:regNum>15990015908</ns2:regNum>--><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>OCU</ns2:responsibleRole><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo/></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo/></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:quantityUndefined>true</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + customer2EisNumber + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement><ns2:customerRequirement><ns2:customer><ns2:regNum>" + customer3EisNumber + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью Юникор</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:deliveryPlace>северная часть пгт. Ижморский и п. Березовский в соответствии с техническим заданием документации о проведении электронного аукциона</ns2:deliveryPlace><ns2:deliveryTerm>С момента подписания муниципального контракта по 30 сентября 2014 г. включительно</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>15</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>Средства обеспечения заявок перечисляются по банковским реквизитам оператора электронной площадки. Обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе</ns2:procedureInfo><ns2:settlementAccount>40302810400003000169</ns2:settlementAccount><ns2:personalAccount>05393018780</ns2:personalAccount><ns2:bik>043207001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>75</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется до подписания контракта победителем электронного аукциона или иным участником, с которым заключается контракт при уклонении победителя от подписания контракта; копия документа, подтверждающего предоставление обеспечения исполнения контракта, представляется заказчику одновременно с подписанием победителем электронного аукциона или иным участником, с которым заключается контракт при уклонении победителя от подписания контракта, проекта контракта. Исполнение контракта может обеспечиваться банковской гарантией или внесением денежных средств. Способ обеспечения исполнения контракта определяется победителем электронного аукциона или иным участником, с которым заключается контракт при уклонении победителя от подписания контракта, самостоятельно. Реквизиты для внесения денежных средств в качестве обеспечения исполнения контракта Получатель: (Администрация Ижморского городского поселения) л/с л/с 05393018780 р/с 40204810100000000052 БИК 043207001 ГРКЦ ГУ Банка России по Кемеровской области г. Кемерово ОГРН 1054216025195 ИНН 4246005976 КПП 424601001</ns2:procedureInfo><ns2:settlementAccount>40302810400003000169</ns2:settlementAccount><ns2:personalAccount>05393018780</ns2:personalAccount><ns2:bik>043207001</ns2:bik></ns2:contractGuarantee></ns2:customerRequirement></ns2:customerRequirements>				<ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>20000</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined></ns2:quantity><ns2:sum>10</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><!--<ns2:code>8361039</ns2:code>--><ns2:shortName>3G44</ns2:shortName><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Единые требования к участникам13</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361040</ns2:code>--><ns2:shortName>ET44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам1</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361974</ns2:code>--><ns2:shortName>TF44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам2</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361975</ns2:code>--><ns2:shortName>TS44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам3</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361976</ns2:code>--><ns2:shortName>TO44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам4</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361977</ns2:code>--><ns2:shortName>TK44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам5</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8361978</ns2:code>--><ns2:shortName>IDT44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам6</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8403975</ns2:code>--><ns2:shortName>TR44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам7</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632415</ns2:code>--><ns2:shortName>TR442</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам8</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632416</ns2:code>--><ns2:shortName>IDT442</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам9</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632417</ns2:code>--><ns2:shortName>MB44330</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам10</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632418</ns2:code>--><ns2:shortName>TS44530</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам11</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632419</ns2:code>--><ns2:shortName>ET44312</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам12</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>3G44</ns2:shortName><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>ET44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TF44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TS44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TO44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TK44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>IDT44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TR44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TR442</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>IDT442</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>MB44330</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>TS44530</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction><ns2:restriction><ns2:shortName>ET44312</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>установлено</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>не установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	
	operatorLogin();
	
	open(baseUrl + "/integration/events/import");
	waitForElementPresent(By.id("content"));
	WebElement element = driver.findElement(By.id("content"));
    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, procedureXml);
    driver.findElement(By.id("send")).click();
    open(baseUrl + "/integration/events/index/objectId/" + procedureNumber + "/");
    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));
    getProcedureID();	
}
	
	
protected void editProcedure() throws InterruptedException {

    //новые данные     
    String purchaseName = "Закупка бумаги";
    String maxPrice = "91.99";
    String contractGuarantee="98.99";
    
    
    LocalDateTime time = LocalDateTime.now();
	
	String dateToday = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +  "T" + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"))+"+03:00";  
	String dateEndZayav = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T23:00:00+03:00";
	//String dateStartAuction = time.plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T23:00:00+03:00";
	//System.out.println(dateToday);
	//System.out.println(dateEndZayav);
	
    String IDPack = randomDigits(36);

    System.out.println("id пакета: " + IDPack); //<ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=464160000154139</ns2:href>
    
    //String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEFDateChange xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>2017-03-06T13:20:24.653+03:00</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"4.3\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>2017-03-06T13:21:29.167+03:00</ns2:docPublishDate><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo/></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>2017-05-15T13:22:00+03:00</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateStartAuction + "</ns2:date><ns2:addInfo/></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:quantityUndefined>true</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + Zak + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>2000</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><!--<ns2:name>Начальная (максимальная) цена единицы услуги: Стоимость сутодачи</ns2:name>--><ns2:price>20000</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined></ns2:quantity><ns2:sum>10</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><!--<ns2:code>8403975</ns2:code>--><ns2:shortName>TR44</ns2:shortName><ns2:name>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632410</ns2:code>--><ns2:shortName>ET44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии  с пунктом 1 части 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>не установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments>";
	//String procedureXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://zakupki.gov.ru/oos/integration/1 file:///C:/Users/Vasily/Downloads/%d0%a1%d1%85%d0%b5%d0%bc%d1%8b%20%d0%b8%d0%bd%d1%82%d0%b5%d0%b3%d1%80%d0%b0%d1%86%d0%b8%d0%b8%20%d0%a4%d0%9a%d0%a1%20v4.3.100%2001.07.2014/%d0%a1%d1%85%d0%b5%d0%bc%d1%8b%20%d0%b8%d0%bd%d1%82%d0%b5%d0%b3%d1%80%d0%b0%d1%86%d0%b8%d0%b8%20%d0%a4%d0%9a%d0%a1%20v4.3.100%2001.07.2014/fcsIntegration.xsd\"><index><id>" + IDPack + "</id>  <sender>OOC</sender>  <receiver>ETP_MMVB</receiver>  <createDateTime>2016-03-02T17:50:57.16+03:00</createDateTime>  <objectType>EF</objectType><objectId>" + procedureNumber + "</objectId>  <indexNum>1</indexNum>  <signature type=\"CAdES-BES\"/>  <mode>PROD</mode>  </index>   <data schemeVersion=\"4.3\">  <ns2:id>2170877</ns2:id>  <ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber>  <ns2:docPublishDate>2016-03-01T17:50:57.16+03:00</ns2:docPublishDate> <ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=315777553332177</ns2:href>  <ns2:printForm>  <ns2:url>http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=2170819</ns2:url> <ns2:signature type=\"CAdES-BES\"> TulJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBV0haVHRzYWYzWXIyY0RpT3R4RWpLSGxVTm5mUUhwY1RtZHFUVDNHV0hDYXNndTFmTkdzb05DbmxRR0JtRWZXUmpNaDQ2K0N2M0xlMFd1ZHphWjQ1ZFE9PQ== </ns2:signature> </ns2:printForm> <ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Самарский водоканал</ns2:fullName> <ns2:postAddress>Российская Федерация, 183006, Мурманская обл, пр. Ленина, д. 75</ns2:postAddress> <ns2:factAddress>Российская Федерация, 183038, Мурманская обл, Мурманск г, К.Маркса, 3, - </ns2:factAddress> </ns2:responsibleOrg> <ns2:responsibleRole>ORA</ns2:responsibleRole> <ns2:responsibleInfo> <ns2:orgPostAddress>Российская Федерация, 183006, Мурманская обл, пр. Ленина, д. 75</ns2:orgPostAddress> <ns2:orgFactAddress>Российская Федерация, 183038, Мурманская обл, Мурманск г, К.Маркса, 3, - </ns2:orgFactAddress> <ns2:contactPerson> <ns2:lastName>Сурков</ns2:lastName> <ns2:firstName>Дмитрий</ns2:firstName> <ns2:middleName>Романович</ns2:middleName> </ns2:contactPerson> <ns2:contactEMail>goszakaz@gov-murman.ru</ns2:contactEMail>  <ns2:contactPhone>8-8152-486592</ns2:contactPhone> <ns2:contactFax>8-8152-693512</ns2:contactFax> </ns2:responsibleInfo> </ns2:purchaseResponsible> <ns2:placingWay> <ns2:code>EA44</ns2:code> <ns2:name>Электронный аукцион</ns2:name> </ns2:placingWay> <ns2:ETP> <ns2:code>ETP_MMVB</ns2:code> <ns2:name>ЭТП РАД</ns2:name> <ns2:url>http://www.lot-online.ru</ns2:url> </ns2:ETP> <ns2:procedureInfo> <ns2:collecting> <ns2:startDate>2016-01-16T12:30:00+03:00</ns2:startDate> <ns2:place>адрес площадки в сети Интернет:http://www.lot-online.ru</ns2:place> <ns2:order>В соответствии с частью 1 раздела 3 тома 1 документации о проведении аукциона в электронной форме. </ns2:order>  <ns2:endDate>" + dateEndZayav + "</ns2:endDate>  </ns2:collecting>  <ns2:scoring>  <ns2:date>" + dateEndZayav + "</ns2:date>  </ns2:scoring>  <ns2:bidding>  <ns2:date>" + dateEndZayav + "</ns2:date>  </ns2:bidding>  </ns2:procedureInfo>  <ns2:lot>  <ns2:maxPrice>"+ maxPrice +"</ns2:maxPrice>  <ns2:currency>  <ns2:code>RUB</ns2:code>  <ns2:name>Российский рубль</ns2:name>  </ns2:currency> <ns2:financeSource>Городская администрация</ns2:financeSource> <ns2:quantityUndefined>false</ns2:quantityUndefined> <ns2:customerRequirements> <ns2:customerRequirement> <ns2:customer> <ns2:regNum>" + Zak + "</ns2:regNum> <ns2:fullName>Государственное областное бюджетное учреждение здравоохранения \"Мурманский областной психоневрологический диспансер\" </ns2:fullName> </ns2:customer> <ns2:maxPrice>"+ maxPrice +"</ns2:maxPrice> <ns2:deliveryPlace>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме.</ns2:deliveryPlace> <ns2:deliveryTerm>В соответствии с п. 5 тома 3 документации о проведении аукциона в электронной форме. </ns2:deliveryTerm> <ns2:applicationGuarantee> <ns2:amount>"+contractGuarantee+"</ns2:amount> <ns2:part>1.0</ns2:part> <ns2:procedureInfo>Участник перечисляет сумму, указанную в пункте 5.1 тома 2 документации о проведении аукциона в электронной форме, на счет, открытый электронной торговой площадкой, адрес площадки в сети Интернет: http://www.lot-online.ru. </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:applicationGuarantee> <ns2:contractGuarantee> <ns2:amount>48137.32</ns2:amount> <ns2:part>10.0</ns2:part> <ns2:procedureInfo>В соответствии с п. 6 тома 2 документации о проведении аукциона в электронной форме </ns2:procedureInfo> <ns2:settlementAccount>40302810500002001111</ns2:settlementAccount> <ns2:personalAccount>05492001111</ns2:personalAccount> <ns2:bik>042206759</ns2:bik> </ns2:contractGuarantee> </ns2:customerRequirement> </ns2:customerRequirements> <ns2:purchaseObjects> <ns2:purchaseObject> <ns2:OKPD> <ns2:code>24.42.13.744</ns2:code> <ns2:name>Препараты, влияющие на процессы обмена, прочие</ns2:name> </ns2:OKPD> <ns2:name>Поставка средств, влияющих на липидно-холестериновый обмен (Фосфолипиды) для нужд учреждений здравоохранения Мурманской области. </ns2:name> <ns2:customerQuantities> <ns2:customerQuantity> <ns2:customer> <ns2:regNum>" + Zak + "</ns2:regNum> <ns2:fullName>Государственное областное бюджетное учреждение здравоохранения \"Мурманский областной Дом ребенка специализированный для детей с органическим поражением центральной нервной системы с нарушением психики\" </ns2:fullName> </ns2:customer> <ns2:quantity>1</ns2:quantity> </ns2:customerQuantity> </ns2:customerQuantities> <ns2:price>"+ maxPrice +"</ns2:price> <ns2:quantity> <ns2:value>1</ns2:value> </ns2:quantity> <ns2:sum>"+ maxPrice +"</ns2:sum> </ns2:purchaseObject> <ns2:totalSum>"+ maxPrice +"</ns2:totalSum> </ns2:purchaseObjects> <ns2:preferenses> <ns2:preferense> <ns2:code/> <ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со Статьей 30 Федерального закона № 44-ФЗ)</ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Социально ориентированным некоммерческим организациям (в соответствии со Статьей 30 Федерального закона № 44-ФЗ)</ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для исполнения контракта (в соответствии со Статьей 30 Федерального закона № 44-ФЗ) </ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для исполнения контракта (в соответствии со Статьей 30 Федерального закона № 44-ФЗ) </ns2:name> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров российского, белорусского и (или) казахстанского происхождения (в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014)</ns2:name> <ns2:prefValue>15</ns2:prefValue> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со Статьей 28 Федерального закона № 44-ФЗ)</ns2:name> <ns2:prefValue>5</ns2:prefValue> </ns2:preferense> <ns2:preferense> <ns2:code/> <ns2:name>Преимущество для организаций инвалидов в отношении предлагаемой цены контракта</ns2:name> <ns2:prefValue/> </ns2:preferense> </ns2:preferenses> <ns2:requirements> <ns2:requirement> <ns2:code/> <ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование о наличии финансовых ресурсов для исполнения контракта (в соответствии с пунктом 1 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование о наличии финансовых ресурсов для исполнения контракта (в соответствии с пунктом 1 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование о наличии на праве собственности или ином законном основании оборудования и других материальных ресурсов для исполнения контракта (в соответствии с пунктом 2 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование о наличии на праве собственности или ином законном основании оборудования и других материальных ресурсов для исполнения контракта (в соответствии с пунктом 2 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требования о наличии опыта работы, связанного с предметом контракта, и деловой репутации (в соответствии с пунктом 3 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требования о наличии опыта работы, связанного с предметом контракта, и деловой репутации (в соответствии с пунктом 3 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требования о наличии необходимого количества специалистов и иных работников определенного уровня квалификации для исполнения контракта (в соответствии с пунктом 4 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требования о наличии необходимого количества специалистов и иных работников определенного уровня квалификации для исполнения контракта (в соответствии с пунктом 4 части 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Иные дополнительные требования к участникам (в соответствии с частью 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Иные дополнительные требования к участникам (в соответствии с частью 2 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> <ns2:requirement> <ns2:code/> <ns2:name>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name> <ns2:content>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:content> </ns2:requirement> </ns2:requirements> <ns2:restrictInfo>Установлено</ns2:restrictInfo> <ns2:restrictForeignsInfo/> <ns2:addInfo/> </ns2:lot> <ns2:attachments> <ns2:attachment> <ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEA</ns2:publishedContentId> <ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName> <ns2:docDescription>Аукционная документация</ns2:docDescription> <ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature> </ns2:cryptoSigns> </ns2:attachment> <ns2:attachment> <ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEB</ns2:publishedContentId> <ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName> <ns2:docDescription>Аукционная документация</ns2:docDescription> <ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature> </ns2:cryptoSigns> </ns2:attachment><ns2:attachment><ns2:publishedContentId>F7108D171EC740BEB94FF67B0E2E3BEC</ns2:publishedContentId><ns2:fileName>АД_Фосфолипиды_(Совместный)_140-фз.doc</ns2:fileName><ns2:docDescription>Аукционная документация</ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=F7108D171EC740BEB94FF67B0E2E3BEA</ns2:url> <ns2:cryptoSigns> <ns2:signature type=\"CAdES-BES\">TUlJSm53WUpLb1pJaHZjTkFRY0NvSUlKa0RDQ0NZd0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2dhMU1JSUdzVENDQm1DZ0F3SUJBZ0lESlJUaU1BZ0dCaXFGQXdJQ0F6Q0NBa014SVRBZkJna3Foa2lHOXcwQkNRRVdFblYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVpTUNBR0ExVUVDUXdaMFlQUXV5NGcwSmpRdTlHTTBMalF2ZEM2MExBZzBMUXVOekVaTUJjR0ExVUVCd3dRMExNdUlOQ2MwTDdSZ2RDNjBMTFFzREdCb2pDQm53WUpLb1pJaHZjTkFRa0NESUdSMEpUUXNOQzkwTDNSaTlDNUlOR0IwTFhSZ05HQzBMalJoTkM0MExyUXNOR0NJTkMrMFlMUXV0R0EwWXZSZ3RDKzBMUFF2aURRdXRDNzBZN1JoOUN3SU5DNDBZSFF2OUMrMEx2UmpOQzMwWVBRdGRHQzBZSFJqeURSZ2RDK0lOR0IwWURRdGRDMDBZSFJndEN5MEw3UXZDRFFvZENhMEpmUW1DRFFtdEdBMExqUXY5R0MwTDRnMEovUmdOQytJRU5UVURFTE1Ba0dBMVVFQmhNQ1VsVXhjakJ3QmdOVkJBc01hZENqMEwvUmdOQ3cwTExRdTlDMTBMM1F1TkMxSU5HQTBMWFF0dEM0MEx6UXNDRFJnZEMxMExyUmdOQzEwWUxRdmRDKzBZSFJndEM0SU5DNElOQ3gwTFhRdDlDKzBML1FzTkdCMEwzUXZ0R0IwWUxRdUNEUXVOQzkwWVRRdnRHQTBMelFzTkdHMExqUXVERTRNRFlHQTFVRUNnd3YwS1RRdGRDMDBMWFJnTkN3MEx2UmpOQzkwTDdRdFNEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTDR4ZnpCOUJnTlZCQU1NZHRDajBML1F2dEM3MEwzUXZ0QzgwTDdSaDlDMTBMM1F2ZEdMMExrZzBZUFF0TkMrMFlIUmd0QyswTExRdGRHQTBZL1JqdEdKMExqUXVTRFJodEMxMEwzUmd0R0FJTkNrMExYUXROQzEwWURRc05DNzBZelF2ZEMrMExQUXZpRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MExBd0hoY05NVFF3TXpBME1UTXhNelEzV2hjTk1UVXdNekEwTVRNeE16UTNXakNDQWFveEpUQWpCZ2txaGtpRzl3MEJDUUVXRm1kdmMzcGhhMkY2UUdkdmRpMXRkWEp0WVc0dWNuVXhDekFKQmdOVkJBWVRBbEpWTVN3d0tnWURWUVFJRENQUW5OR0QwWURRdk5DdzBMM1JnZEM2MExEUmp5RFF2dEN4MEx2UXNOR0IwWUxSakRFZE1Cc0dBMVVFQnd3VTBMTXVJTkNjMFlQUmdOQzgwTERRdmRHQjBMb3hhVEJuQmdOVkJBb01ZTkNhMEw3UXZOQzQwWUxRdGRHQ0lOQ3owTDdSZ2RHRDBMVFFzTkdBMFlIUmd0Q3kwTFhRdmRDOTBZdlJoU0RRdDlDdzBMclJnOUMvMEw3UXVpRFFuTkdEMFlEUXZOQ3cwTDNSZ2RDNjBMN1F1U0RRdnRDeDBMdlFzTkdCMFlMUXVERTBNRElHQTFVRUtnd3IwSjNRc05HQzBMRFF1OUdNMFk4ZzBKclF2dEM5MFlIUmd0Q3cwTDNSZ3RDNDBMM1F2dEN5MEwzUXNERVhNQlVHQTFVRUJBd08wSkhRc05DNTBMclF2dEN5MExBeEtEQW1CZ05WQkF3TUg5Q2QwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEF4UXpCQkJnTlZCQU1NT3RDUjBMRFF1ZEM2MEw3UXN0Q3dJTkNkMExEUmd0Q3cwTHZSak5HUElOQ2EwTDdRdmRHQjBZTFFzTkM5MFlMUXVOQzkwTDdRc3RDOTBMQXdZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFOREFBUkFsbm5ZVk1RSjBFTFRPQ05tbFNEa2g5blhJSEZhNDV3ZW4vOWpzSjdoTmh1SEg0cDB6eEFxOVRycHFiUUtvS1ZXL3NOK0FxNkY2Z1RNZ3MwOUFNRnpFcU9DQWM0d2dnSEtNQXdHQTFVZEV3RUIvd1FDTUFBd0dBWURWUjBnQkJFd0R6QU5CZ3NxaFFNRFBaN1hOZ0VDQWpDQndBWURWUjBSQklHNE1JRzFvQklHQTFVRURLQUxFd2t5TWpjeE56SXpNakdnR0FZSUtvVURBNEVEQVFHZ0RCTUtOVEU1TURFNU9ETTJNcUFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVFV4T1RBd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF4TkRreU1EQXdNREl6b0EwR0J5cUZBd0hnT1FHZ0FoTUFvQThHQ1NxR1NJYjNEUUVKRktBQ0V3Q2dFQVlLS29VREF6MmUxellCQ0tBQ0V3Q2dHQVlGS29VRFpBR2dEeE1OTVRBNU5URTVNREF3TURreE5vWUJNREFPQmdOVkhROEJBZjhFQkFNQ0ErZ3dVd1lEVlIwbEJFd3dTZ1lJS3dZQkJRVUhBd0lHRGlxRkF3TTludGMyQVFZREJBSUNCZzRxaFFNRFBaN1hOZ0VHQXdRQ0F3WU9Lb1VEQXoyZTF6WUJCZ01FQWdRR0RpcUZBd005bnRjMkFRWURCQUlGTUI4R0ExVWRJd1FZTUJhQUZHZWQrMi92V1RyekN5RXBPNE5Zd3hZTkVNT2lNRGdHQTFVZEh3UXhNQzh3TGFBcm9DbUdKMmgwZEhBNkx5OWpjbXd1Y205emEyRjZibUV1Y25VdlkzSnNMMHhCVTFSZmJtVjNMbU55YkRBZEJnTlZIUTRFRmdRVTlhWk5tOVVJY0I3SXU0Ung0T1FNOEFTckRCUXdDQVlHS29VREFnSURBMEVBRWhSekVwR1VzV2tGcENPcVBtOUZYeEhqdGt0eW0rVEpZVnFYOUZNV1h4U2x0K0t6RzBoL3lJNU0wRlpFVWRXQi9oSGdXSGdFclpLUlVWNzc5NEVtcERHQ0FyRXdnZ0t0QWdFQk1JSUNURENDQWtNeElUQWZCZ2txaGtpRzl3MEJDUUVXRW5WMVkxOW1hMEJ5YjNOcllYcHVZUzV5ZFRFaU1DQUdBMVVFQ1F3WjBZUFF1eTRnMEpqUXU5R00wTGpRdmRDNjBMQWcwTFF1TnpFWk1CY0dBMVVFQnd3UTBMTXVJTkNjMEw3UmdkQzYwTExRc0RHQm9qQ0Jud1lKS29aSWh2Y05BUWtDRElHUjBKVFFzTkM5MEwzUmk5QzVJTkdCMExYUmdOR0MwTGpSaE5DNDBMclFzTkdDSU5DKzBZTFF1dEdBMFl2Umd0QyswTFBRdmlEUXV0QzcwWTdSaDlDd0lOQzQwWUhRdjlDKzBMdlJqTkMzMFlQUXRkR0MwWUhSanlEUmdkQytJTkdCMFlEUXRkQzAwWUhSZ3RDeTBMN1F2Q0RRb2RDYTBKZlFtQ0RRbXRHQTBMalF2OUdDMEw0ZzBKL1JnTkMrSUVOVFVERUxNQWtHQTFVRUJoTUNVbFV4Y2pCd0JnTlZCQXNNYWRDajBML1JnTkN3MExMUXU5QzEwTDNRdU5DMUlOR0EwTFhRdHRDNDBMelFzQ0RSZ2RDMTBMclJnTkMxMFlMUXZkQyswWUhSZ3RDNElOQzRJTkN4MExYUXQ5QyswTC9Rc05HQjBMM1F2dEdCMFlMUXVDRFF1TkM5MFlUUXZ0R0EwTHpRc05HRzBMalF1REU0TURZR0ExVUVDZ3d2MEtUUXRkQzAwTFhSZ05DdzBMdlJqTkM5MEw3UXRTRFF1dEN3MExmUXZkQ3cwWWZRdGRDNTBZSFJndEN5MEw0eGZ6QjlCZ05WQkFNTWR0Q2owTC9RdnRDNzBMM1F2dEM4MEw3Umg5QzEwTDNRdmRHTDBMa2cwWVBRdE5DKzBZSFJndEMrMExMUXRkR0EwWS9SanRHSjBMalF1U0RSaHRDMTBMM1JndEdBSU5DazBMWFF0TkMxMFlEUXNOQzcwWXpRdmRDKzBMUFF2aURRdXRDdzBMZlF2ZEN3MFlmUXRkQzUwWUhSZ3RDeTBMQUNBeVVVNGpBS0JnWXFoUU1DQWdrRkFEQUtCZ1lxaFFNQ0FoTUZBQVJBeVlLU0tQU3dSYkhkSnFMYk94Y3dXajVmdUszNk8rbUErWm1DQ05NZlJWTCtGdWt5M0pMdDZiSFNnOEhwN3AwV0ZvQ045RzFSbm5XbDFzWXY2eURUK0E9PQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments>";
	
    String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=4641600006625555123</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + customer1EisNumber + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><!--роль заказчика--><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo>доп инфо в блоке об организаторе закупки</ns2:addInfo></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo> доп инфо Информация о проведении аукциона в электронной форме</ns2:addInfo></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:standardContractNumber>1231234567891234</ns2:standardContractNumber><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:interbudgetaryTransfer>true</ns2:interbudgetaryTransfer><ns2:quantityUndefined>false</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + customer2EisNumber + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3 указывается единожды</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>14</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined><!--При приеме элемент должен содержать вариант «undefined» если для лота закупки невозможно определить количество товара, объем подлежащих выполнению работ, оказанию услуг--></ns2:quantity><ns2:sum>15</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><ns2:code>8361039</ns2:code><!--<ns2:shortName>3G44</ns2:shortName>--><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><ns2:code>8361040</ns2:code><!--<ns2:shortName>ET44</ns2:shortName>--><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments>";
    String procedureXmlEdit = "<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>Устранение неточностей в документации электронного аукциона</ns2:info><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-02-21T00:00:00Z</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification></data></fcsNotificationEF>";
	
	operatorLogin();

	open(baseUrl + "/integration/events/import");

	
	waitForElementPresent(By.id("content"));
	
	WebElement element = driver.findElement(By.id("content"));
    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, procedureXml+procedureXmlEdit);
    print(procedureXml+procedureXmlEdit);
    driver.findElement(By.id("send")).click();
    
              
    open(baseUrl + "/integration/events/index/packageId/" + IDPack + "/objectId/" + procedureNumber);

    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));

}
	

	
	
	protected void prescriptionsToCustomerAllAllowed() throws InterruptedException {
		//this.procedureNumber = "1079179706";
		operatorLogin();
		open(demoUrl + "/procedure/catalog/procedures/operator?q=" + procedureNumber + "&simple-search=Найти");
		waitForElementPresentRefresh(By.xpath("//td[contains(.,'Подведение итогов')]"));
		compactToNormalView();
		click(By.xpath("//a[contains(.,'Предоставить возможность исполнения предписания организатору закупки')]"));
		select("Рассмотрение заявок", By.xpath("//select[@id='ProcedureEnablePrescriptions-statusAfter']"));
		//Thread.sleep(10000);
		waitForElementPresent(By.xpath("//input[@id='ProcedureEnablePrescriptions-continueWithLastOffer']"));
		jsClick(By.xpath("//input[@id='ProcedureEnablePrescriptions-continueWithLastOffer']"));
		click(By.xpath("//input[@data-type='signAndSubmit']"));
		waitForElementPresent(By.xpath("//h1[contains(.,'Предоставление возможности исполнения предписания организатору закупки')]"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains(procedureNumber + " предоставление возможности исполнения предписания выполнено успешно"));
	}
	
	protected void prescriptionsToCustomer1stNotAllAllowed() throws InterruptedException {
		//this.procedureNumber = "1079179706";
		operatorLogin();
		open(demoUrl + "/procedure/catalog/procedures/operator?q=" + procedureNumber + "&simple-search=Найти");
		waitForElementPresentRefresh(By.xpath("//td[contains(.,'Подведение итогов')]"));
		compactToNormalView();
		click(By.xpath("//a[contains(.,'Предоставить возможность исполнения предписания организатору закупки')]"));
		select("Рассмотрение заявок", By.xpath("//select[@id='ProcedureEnablePrescriptions-statusAfter']"));
		waitForElementPresent(By.xpath("//input[@id='ProcedureEnablePrescriptions-continueWithLastOffer']"));
		jsClick(By.xpath("//input[@id='ProcedureEnablePrescriptions-continueWithLastOffer']"));
		
		select("Возвращена", By.xpath("//select[@name='ProcedureEnablePrescriptions[requestList][3][newStatus]']"));
		
		click(By.xpath("//input[@data-type='signAndSubmit']"));
		waitForElementPresent(By.xpath("//h1[contains(.,'Предоставление возможности исполнения предписания организатору закупки')]"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains(procedureNumber + " предоставление возможности исполнения предписания выполнено успешно"));
	}
	
	
	protected void customerPrescriptionExecution() throws InterruptedException {
		//this.procedureNumber = "868350533";

		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=Найти");
		compactToNormalView();
		click(By.xpath("//a[contains(.,'Исполнение предписания')]"));
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		time=time.plusDays(1);
		type(time.format(formatter), By.xpath("//input[@id='requestReviewDateTime']"));
		type(time.format(formatter), By.xpath("//input[@id='conditionalHoldingDateTime']"));
		type("123", By.xpath("//input[@name='ProcedureEnablePrescriptions[documents][0][title]']"));
		type(fileForLoadPath, By.xpath("//input[@name='ProcedureEnablePrescriptions[documents][0][file][upload]']"));
		click(By.xpath("//button[@name='ProcedureEnablePrescriptions[documents][0][file]-append']"));
		waitForElementPresent(By.xpath("//p[@class='upload-progress progress-success text-success']"));
		click(By.xpath("//input[@data-type='signAndSubmit']"));
		waitForElementPresent(By.xpath("//h1[contains(.,'Исполнение предписания')]"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains(procedureNumber + " исполнение предписания выполнено успешно"));
	}
	
	protected void getFirstBestOfferSumm() throws Exception {
		//this.procedureNumber = "868350533";
		loginUser("Z1");
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=Найти");
		compactToNormalView();
		click(By.xpath("//a[contains(.,'Просмотреть события')]"));
        click(By.xpath("(//a[contains(text(),'Просмотр')])[3]"));	
        this.lastOfferSumm = driver.findElement(By.xpath("//tr[@id='row-1']/td[3]/div")).getText().replace(" ","");
        print("Сумма последнего предложения: " + lastOfferSumm);
	}
	protected void getSecondBestOfferSumm() throws Exception {
		//this.procedureNumber = "868350533";
		loginUser("Z1");
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=Найти");
		compactToNormalView();
		click(By.xpath("//a[contains(.,'Просмотреть события')]"));
        click(By.xpath("(//a[contains(text(),'Просмотр')])[3]"));	
        this.lastOfferSumm = driver.findElement(By.xpath("//html/body/div/section/main/div/div[2]/form/fieldset/fieldset[3]/table/tbody/tr[2]/td[3]/div")).getText().replace(" ","");
        print("Сумма последнего предложения: " + lastOfferSumm);
	}
	
	protected void checkLastOfferSummAllAllowed() throws Exception {
		loginUser("1");
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=Найти");
		waitForElementPresentRefresh(By.xpath("//td[contains(.,'Идут торги')]"));
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Принять участие')]"));
	    open(demoUrl + "/trade/offer/" + procedureid);
	    waitForElementPresent(By.xpath("//div/div/table/tbody/tr[1]/td[3]"));
	    assertTrue(driver.findElement(By.xpath("//div/div/table/tbody/tr[1]/td[3]")).getText().replace(" ","").equals(lastOfferSumm));
		
	}
	
	
	protected void blockProcedure() throws InterruptedException {
		//this.procedureNumber = "";
		operatorLogin();
		open(demoUrl + "/procedure/block");
		type(procedureNumber, By.xpath("//textarea[@class=' form-control']"));
		click(By.xpath("//input[@value='blockContract']"));
		type("123", By.xpath("//input[@type='text']"));
		type(fileForLoadPath, By.xpath("//input[contains(@name,'ProcedureBlock[documents][0][file][upload]')]"));
		click(By.xpath("//button[@name='ProcedureBlock[documents][0][file]-append']"));
		waitForElementPresent(By.xpath("//p[@class='upload-progress progress-success text-success']"));
		click(By.xpath("//input[@data-type='signAndSubmit']"));
		waitForElementPresent(By.cssSelector("main"));
		Thread.sleep(5000);
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains(procedureNumber + " действие выполнено успешно"));
	}
	
	


	public void checkCreditReturn(String numberOfSupplier) throws Exception {
		loginUser(numberOfSupplier);
		open(demoUrl + "/credit/request/registry?&uid=" + creditRequestID + "&");
		waitForElementPresentRefresh(By.xpath("//p[contains(.,'Вывод ДС')]"));
		System.out.println((driver.findElement(By.xpath("//td[@class='row-amount mte-grid-column-money']")).getText()).replace(" ",""));
		System.out.println(creditSumm);
		assertTrue(driver.findElement(By.xpath("//td[@class='row-amount mte-grid-column-money']")).getText().replace(" ","").equals(creditSumm));
	}
	
	public void checkCreditUnblock(String numberOfSupplier) throws Exception {
		//procedureNumber = "409916063";
		//creditRequestID = "1610030002";
		//creditSumm="4813.72";
		loginUser(numberOfSupplier);
		open(demoUrl + "/credit/request/registry?&uid=" + creditRequestID + "&");
		waitForElementPresentRefresh(By.xpath("//p[contains(.,'Разблокирование ДС')]"));
		System.out.println((driver.findElement(By.xpath("//td[@class='row-amount mte-grid-column-money']")).getText()).replace(" ",""));
		System.out.println(creditSumm);
		assertTrue(driver.findElement(By.xpath("//td[@class='row-amount mte-grid-column-money']")).getText().replace(" ","").equals(creditSumm));
	}
	
	public void recallRequest(String numberOfSupplier) throws Exception {
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/procedures/personal?registrationNumber=" + procedureNumber + "&");
		compactToNormalView();
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Отозвать заявку')]"));
		click(By.xpath("//a[contains(.,'Отозвать заявку')]"));
		click(By.xpath("//input[@value='Подписать и отправить']"));
		waitForElementPresent(By.xpath("//h1[contains(.,'Заявка успешно отозвана')]"));
	}
	protected void checkAccessFilePrz(String numberOfSupplier) throws Exception {
		//this.procedureNumber = "434930153";   //файла нет
		//this.procedureNumber = "106505651";
		loginUser(numberOfSupplier);
		deleteAllFilesFolder("C:\\TestDownloads");
		open(demoUrl + "/procedure/catalog/?&registrationNumber=" + procedureNumber +"&");
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Просмотреть события')]"));
		click(By.xpath("//a[contains(.,'Просмотреть события')]"));
		click(By.xpath("(//a[contains(text(),'Просмотр')])[2]"));
		checkFileExist("C:\\TestDownloads");
	}
	
	public static void deleteAllFilesFolder(String path) {
        for (File myFile : new File(path).listFiles())
               if (myFile.isFile()) myFile.delete();
   }
	
	public void checkFileExist(String path) throws InterruptedException {
		Thread.sleep(5000);
	    File file = new File(path);
	    //while(!file.canWrite())
	      //  Thread.sleep(1000);
		File[] listFiles = file.listFiles();
		assertTrue(listFiles.length == 1);
   }
	
	private static void findFiles(String dir, String ext) {
        File file = new File("C:\\Downloads");
        if(file.exists()) file.delete();
        
        
        File[] listFiles = file.listFiles();
        if(listFiles.length == 0){
            System.out.println(dir + " не содержит файлов с расширением " + ext);
        }else{
            for(File f : listFiles)
                System.out.println("Файл: " + dir + File.separator + f.getName());}
        }
	
	protected void getContractInformationRequest() throws Exception {
		//this.procedureNumber = "186548358";
		
		//запрос без файла. Сохраняем в черновик, затем отправляем
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		compactToNormalView();
		waitForElementPresentRefresh(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		click(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		type("123", By.xpath("//textarea[@required='required']"));
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy 23-00-00");
		type(time.format(formatter), By.xpath("//input[@id='informationRequest-executionToDateTime']"));
		click(By.xpath("//input[@value='Сохранить в черновик']"));
		waitForInvisibility(By.xpath("//input[@value='Подписать и отправить']"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Черновик успешно сохранен."));
		click(By.xpath("//button[contains(.,'Вернуться')]"));
		waitForElementPresentRefresh(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		click(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		click(By.xpath("//input[@value='Подписать и отправить']"));
		waitForInvisibility(By.xpath("//input[@value='Подписать и отправить']"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Запрос недостающих сведений по контракту направлен участнику."));
		
		//Участник проверяет что запрос без файла
		loginUser("U2");
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		compactToNormalView();
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Просмотр запроса недостающих сведений по контракту')]"));
		click(By.xpath("//a[contains(.,'Просмотр запроса недостающих сведений по контракту')]"));
		assertTrue(!elementExist(By.xpath("//p[@class='upload-filename']")));
		click(By.xpath("//button[contains(.,'Вернуться')]"));
				
		//Участник подписывает проект
		waitForElementPresentRefresh(By.linkText("Подписать проект контракта"));
		click(By.linkText("Подписать проект контракта"));
		click(By.name("signAndSubmit"));
	    click(By.xpath("//button[contains(.,'Подписать и отправить')]"));
	    waitForInvisibility(By.xpath("//button[contains(.,'Подписать и отправить')]"));
	    assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Проект контракта успешно подписан"));
		
	    loginUser("Z2");
		
		//запрос с прикреплением файла
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		compactToNormalView();
		waitForElementPresentRefresh(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		click(By.xpath("//a[@class=' mte-grid-action action-information-request']"));
		type("123", By.xpath("//textarea[@required='required']"));
		type(time.format(formatter), By.xpath("//input[@id='informationRequest-executionToDateTime']"));
		type("123",By.xpath("//input[@name='informationRequest[documentsBlock][documents][0][title]']"));
		type(fileForLoadPath, By.xpath("//input[@name='informationRequest[documentsBlock][documents][0][file][upload]']"));
		click(By.xpath("//button[contains(.,'Прикрепить')]"));
		waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
		click(By.xpath("//input[@value='Подписать и отправить']"));
		waitForInvisibility(By.xpath("//input[@value='Подписать и отправить']"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Запрос недостающих сведений по контракту направлен участнику."));
		
		//Участник проверяет что запрос с файлом
		loginUser("U2");
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		compactToNormalView();
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Просмотр запроса недостающих сведений по контракту')]"));
		click(By.xpath("//a[contains(.,'Просмотр запроса недостающих сведений по контракту')]"));
		assertTrue(elementExist(By.xpath("//p[@class='upload-filename']")));
				//click(By.xpath("//button[contains(.,'Вернуться')]"));
		
	}


	private void compactToNormalView() throws InterruptedException {
		//if (driver.toString().contains("InternetExplorerDriver"))
	    if (elementExist(By.xpath("//span[contains(.,'Обычный вид')]"))) click(By.xpath("//span[contains(.,'Обычный вид')]"));
	}
	protected void checkReturnOfCreditMoney(String numberOfSupplier) throws Exception {
		loginUser(numberOfSupplier);
		open(demoUrl + "/credit/request/registry?&registrationNumber=" + procedureNumber + "&");
		waitForElementPresentRefresh(By.xpath("//td[contains(.,'Подтверждена (ДС разблокированы)')]"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Подтверждена (ДС разблокированы)"));
	}
	
	protected void finalWinnerNotAdmitted() throws InterruptedException {
		//procedureNumber="419581156";
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8");
	    waitForElementPresentRefresh(By.xpath("//a[@class=' ps-grid-action action-result-secondparts']"));
	    click(By.xpath("//a[@class=' ps-grid-action action-result-secondparts']"));
	    select("Не соответствует", By.xpath("//select[contains(@id,'protocolReview-lot-requestList-0-admittance-select')]")); 
	                    
	    jsClick(By.xpath("//input[@id='protocolReview-lot-requestList-0-notAdmittedReason-request-nonconformity-unreliable-documents-code']"));
	    Thread.sleep(1000);
	    type("123", By.xpath("//textarea[@id='protocolReview-lot-requestList-0-notAdmittedReason-request-nonconformity-unreliable-documents-reason']"));
	                          
	    int i = 1;
	    while (elementExist(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]")))
	    {select("Соответствует", By.xpath("//select[@id='protocolReview-lot-requestList-" + i + "-admittance-select']"));
	    i++;}   
	    type(fileForLoadPath, By.id("protocolReview-documentsBlock-protocolFinalDocument"));
	    Thread.sleep(1000);
	    click(By.xpath("//button[contains(@name,'protocolReview[documentsBlock][protocolFinalDocument]-append')]"));
	    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
	    if (elementExist(By.xpath("//input[@name='signAndSubmitNow']")))
		{click(By.xpath("//input[@name='signAndSubmitNow']"));}
	    else click(By.name("signAndSubmit"));
	    
	    //waitForElementPresent(By.xpath("//h4[contains(.,'Подтверждение публикации')]"));
    	//click(By.xpath("//button[contains(.,'Продолжить')]"));
	    
	    waitForElementPresent(By.xpath("//h1[contains(.,'Протокол подведения итогов успешно опубликован')]"));
	    click(By.xpath("//button[contains(.,'Вернуться')]"));
	    waitForText("Заключение контракта", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	}
	
	
	
	
	
	private void getScreenShot(ITestResult result) throws IOException {
		   LocalDateTime time = LocalDateTime.now();
		   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
		   File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		   FileUtils.copyFile(scrFile, new File("c:\\Java\\Screenshots\\" + result.getName()+" " + time.format(formatter) + ".jpg"));
		   //FileUtils.copyFile(scrFile, new File("/var/lib/jenkins/workspace/new_maven/AutoTests/Screenshot/" + result.getName()+" " + time.format(formatter) + ".jpg"));
	}

	protected void select(String string, By by) {
		new Select(driver.findElement(by)).selectByVisibleText(string);
	}

	protected boolean elementExist(By by) {
		/*
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(by, 0)) != null){return true;}
		else {return false;}*/
		//this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			if (driver.findElements(by).size() > 0) 
		{//this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			return true;}
		
		else {
		//this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			return false;}
	}

	protected void finalNoAdmitted() throws InterruptedException {
		//this.procedureNumber = "798469210";
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8");	    
		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-result-secondparts"));  
		click(By.cssSelector(".ps-grid-action.action-result-secondparts"));  
		
	    type(fileForLoadPath, By.id("protocolReview-documentsBlock-protocolFinalDocument"));
	    Thread.sleep(1000);
	    click(By.xpath("//button[contains(@name,'protocolReview[documentsBlock][protocolFinalDocument]-append')]"));
	    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
	    
	    int i = 0;
	    while (elementExist(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]")))
	    {select("Не соответствует", By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]"));
	    jsClick(By.xpath("//input[@id='protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-unreliable-documents-code']"));
	    click(By.xpath("//textarea[@id='protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-unreliable-documents-reason']"));
	    type("123", By.xpath("//textarea[@id='protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-unreliable-documents-reason']"));
	    i++;}   
	    
	    click(By.name("signAndSubmit"));
	    
	    waitForElementPresent(By.xpath("//h1[contains(.,'Протокол подведения итогов успешно опубликован')]"));
	    click(By.xpath("//button[contains(.,'Вернуться')]"));
	    waitForText("Не состоялась", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	}

	protected void przOneRequestAdmitted() throws InterruptedException {
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
		
		//compactToNormalView();
		//waitForElementPresentRefresh(By.xpath("//a[@class=' mte-grid-action action-request-firstpart-review']"));
		//jsClick(By.xpath("//a[@class=' mte-grid-action action-request-firstpart-review']"));
	    //click(By.xpath("//a[@class=' mte-grid-action action-request-firstpart-review']"));
		
		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-request-firstpart-review"));
	    click(By.cssSelector(".ps-grid-action.action-request-firstpart-review"));
		
		new Select(driver.findElement(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-0-admittance-select')]"))).selectByVisibleText("Допущена");
	    int i = 1;
	    while (driver.findElements(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]")).size() > 0)
	    {
	    	new Select(driver.findElement(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]"))).selectByVisibleText("Не допущена");
	    click(By.id("protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-information-variance-code"));
	    type("123", By.id("protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-information-variance-reason"));
	    i++;}
	    type(fileForLoadPath, By.xpath("//input[@id='protocolReview-documentsBlock-protocolDocument']"));
	    click(By.xpath("//button[@name='protocolReview[documentsBlock][protocolDocument]-append']"));
	    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));  
	    
	    /*type(fileForLoadPath, By.name("protocolReview[documentsBlock][documents][0][file][upload]"));
	    type("123", By.xpath("//input[@required='required']"));
	    click(By.name("protocolReview[documentsBlock][documents][0][file]-append"));
	    waitForText("Успешно загружен", By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"));
	    
	    if (driver.findElements(By.xpath("//input[@value='Опубликовать сейчас']")).size() > 0)
	    	{click(By.xpath("//input[@value='Опубликовать сейчас']"));}
	    else click(By.name("signAndSubmit"));*/
	    
	    click(By.name("signAndSubmit"));
	    
	    //waitForElementPresent(By.xpath("//h4[contains(.,'Подтверждение публикации')]"));
    	//click(By.xpath("//button[contains(.,'Продолжить')]"));
    	
	    waitForElementPresent(By.xpath("//h1[contains(.,'Протокол рассмотрения заявок успешно опубликован')]"));
	    click(By.xpath("//button[@name='back']"));
	    waitForText("Подведение итогов", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	}

	protected void przNotAllowedRequestPublication() throws InterruptedException {
		//this.procedureNumber = "32603668";
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
		//compactToNormalView();
		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-request-firstpart-review"));
	    click(By.cssSelector(".ps-grid-action.action-request-firstpart-review"));
	    
	    int i = 0;
	    while (driver.findElements(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]")).size() > 0)
	    {new Select(driver.findElement(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]"))).selectByVisibleText("Не допущена");
	    click(By.xpath("//input[@id='protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-information-variance-code']"));
	    type("123", By.xpath("//textarea[@id='protocolReview-lot-requestList-" + i + "-notAdmittedReason-request-nonconformity-information-variance-reason']"));
	    i++;}
	    
	    type(fileForLoadPath, By.xpath("//input[@id='protocolReview-documentsBlock-protocolDocument']"));
	    click(By.xpath("//button[@name='protocolReview[documentsBlock][protocolDocument]-append']"));
	    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));  
	    
	    /*type(fileForLoadPath, By.name("protocolReview[documentsBlock][documents][0][file][upload]"));
	    type("123", By.xpath("//input[@required='required']"));
	    click(By.name("protocolReview[documentsBlock][documents][0][file]-append"));
	    waitForText("Успешно загружен", By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"));*/
	    
	    //if (driver.findElements(By.xpath("//input[@value='Опубликовать сейчас']")).size() > 0)
	    //	{click(By.xpath("//input[@value='Опубликовать сейчас']"));}
	    //else click(By.name("signAndSubmit"));
	    
	    click(By.name("signAndSubmit"));
	    
	    //waitForElementPresent(By.xpath("//h4[contains(.,'Подтверждение публикации')]"));
    	//click(By.xpath("//button[contains(.,'Продолжить')]"));
    	
	    waitForElementPresent(By.xpath("//h1[contains(.,'Протокол рассмотрения заявок успешно опубликован')]"));
	    click(By.xpath("//button[@name='back']"));
	    waitForText("Не состоялась", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	}
	

	protected void finalProtocolNoRequestCustomer(String numberOfCustomer) throws Exception {
			
		
		loginUser(numberOfCustomer);
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
			compactToNormalView();
		    waitForElementPresentRefresh(By.xpath("(//a[contains(text(),'Рассмотрение заявок')])[2]"));
	//заполнение формы протокола, не подано заявок
		    click(By.xpath("(//a[contains(text(),'Рассмотрение заявок')])[2]"));
		    type(fileForLoadPath, By.id("protocolReview-documentsBlock-protocolDocument"));
		    click(By.name("protocolReview[documentsBlock][protocolDocument]-append"));
		    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
		    //type(fileForLoadPath, By.name("protocolReview[documentsBlock][documents][0][file][upload]"));
		    //type("123", By.xpath("//input[@required='required']"));
		    //click(By.name("protocolReview[documentsBlock][documents][0][file]-append"));
		    //waitForText("Успешно загружен", By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"));
		    if (driver.findElements(By.xpath("//input[@value='Опубликовать сейчас']")).size() > 0)
		    	{click(By.xpath("//input[@value='Опубликовать сейчас']"));}
		    else click(By.name("signAndSubmit"));
		    
		    //waitForElementPresent(By.xpath("//h4[contains(.,'Подтверждение публикации')]"));
	    	//click(By.xpath("//button[contains(.,'Продолжить')]"));
	    	
		    waitForText("Протокол рассмотрения заявок успешно опубликован",By.xpath("//h1[contains(.,'Протокол рассмотрения заявок успешно опубликован')]"));
		    click(By.xpath("//button[@name='back']"));
		    waitForText("Не состоялась", By.xpath("//td[@class='row-procedureStatusTitle ']"));
		}

	private String creditRequestStatus(String creditRequestID) {
		//Возвращает статус заявки на получение кредита
		open(demoUrl + "/credit/request/registry?&uid=" + creditRequestID + "&");
		return driver.findElement(By.xpath("//td[@class='row-status ']")).getText();
		
		//WebElement parent = driver.findElement(By.xpath("//td[contains(.,'"+ creditRequestID +"')]/.."));
		//return driver.findElement(By.cssSelector("#" + parent.getAttribute("id") + " > td.row-status ")).getText();
	}

	protected void getCreditRequest(String numberOfSupplier) throws Exception {
		getCredit(numberOfSupplier);          //Подать заявку на кредит
		creditOrgLogin();            // Вход пользователя кредитной организации
		creditApprove();             // Подтверждение кредита КО
		createPaymentFile();         // Создание файла с кредитным платежом
		creditEnrollment();          // Загрузка кредитного платежа оператором
		getRequestWithCreditMoney(numberOfSupplier); // Подача заявки с привелечением кредитных средств
	}

	private void getRequestWithCreditMoney(String numberOfSupplier) throws Exception {
		//this.procedureNumber = "963836161";
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/?q=" + procedureNumber  + "&simple-search=Найти");
		waitForElementPresentRefresh(By.linkText("Подать заявку")); 
		click(By.linkText("Подать заявку"));
		type(fileForLoadPath, By.name("request[secondPart][documents][approve-large-transactions][0][file][upload]"));
	    JavascriptExecutor js = (JavascriptExecutor)driver;
	    js.executeScript("scroll(0, 1400)");
	    click(By.name("request[secondPart][documents][approve-large-transactions][0][file]-append"));
	    type("123", By.name("request[secondPart][documents][approve-large-transactions][0][title]"));
	    waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
	    waitForElementPresent(By.xpath("(//input[@name='request[creditRequest]'])[2]"));
	    click(By.xpath("(//input[@name='request[creditRequest]'])[2]"));
	    click(By.name("sign"));
	    click(By.xpath("//button[@class='btn btn-primary']"));
	    click(By.xpath("//button[@class='btn btn-primary']"));
	    click(By.xpath("//button[@class='btn btn-primary']"));
	    waitForText("Заявка успешно подана", By.xpath("//h1[contains(.,'Заявка успешно подана')]"));
	    Thread.sleep(5000);
	    assertEquals("Подтверждена (ДС заблокированы)",creditRequestStatus(creditRequestID));
	}

	protected void createPaymentFile() {
		
		//this.creditRequestID="1609230001"; //номер платежа
		//this.creditSumm="4813.73";
		//this.purposeOfPayment="Средства для обеспечения участия в электронных аукционах (л/с ЭТП: 56000989780000661132 / 1609230001). Без НДС.";
		
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		try(FileWriter writer = new FileWriter(creditFilePath, false))
	    {
	       // запись всей строки
	        String text = 
	       "1CClientBankExchange" + "\n" +
	       "ВерсияФормата=1.01" + "\n" +
	       "Кодировка=Windows" + "\n" +
	       "Получатель=" + "\n" +
	       "ДатаНачала=14.11.2013" + "\n" +
	       "ДатаКонца=20.11.2013" + "\n" +
	       "РасчСчет=40702810600000000103" + "\n" +
	       "СекцияРасчСчет" + "\n" +
	       "ДатаНачала=20.11.2013" + "\n" +
	       "ДатаКонца=20.11.2013" + "\n" +
	       "РасчСчет=40702810600000000103" + "\n" +
	       "НачальныйОстаток=871797.02" + "\n" +
	       "ВсегоПоступило=13542579.45" + "\n" +
	       "ВсегоСписано=13000000.00" + "\n" +
	       "КонечныйОстаток=1414376.47" + "\n" +
	       "КонецРасчСчет" + "\n" +
	       "СекцияРасчСчет" + "\n" +
	       "ДатаНачала=20.11.2013" + "\n" +
	       "ДатаКонца=20.11.2013" + "\n" +
	       "РасчСчет=40702810600000000103" + "\n" +
	       "НачальныйОстаток=23.97" + "\n" +
	       "ВсегоПоступило=0.00" + "\n" +
	       "ВсегоСписано=0.00" + "\n" +
	       "КонечныйОстаток=23.97" + "\n" +
	       "КонецРасчСчет" + "\n" +
	       "СекцияДокумент=Платежное поручение" + "\n" +
	       "Номер=" + creditRequestID + "\n" +
	       "Дата=" + time.format(formatter) + "\n" +
	       "Сумма=" + creditSumm + "\n" +
	       "ПлательщикСчет=34901293012903912040" + "\n" +
	       "ПлательщикИНН=344801023107" + "\n" +
	       "ПлательщикКПП=" + "\n" +
	       "Плательщик1= Сергеев Александр Александрович" + "\n" +
	       "ПлательщикРасчСчет=34901293012903912040" + "\n" +
	       "ПлательщикБанк1=Б ООО \"БИНБАНК\"" + "\n" +
	       "ПлательщикБанк2=Г МОСКВА" + "\n" +
	       "ПлательщикБИК=044525205" + "\n" +
	       "ПлательщикКорсчет=30101810200000000206" + "\n" +
	       "ПолучательСчет=40702810700030004213" + "\n" +
	       "ДатаПоступило=26.08.2016" + "\n" +
	       "ПолучательИНН=7703668940" + "\n" +
	       "ПолучательКПП=770301001" + "\n" +
	       "Получатель1= ЗАО \"ММВБ-ИТ\"" + "\n" +
	       "ПолучательРасчСчет=40702810700030004213" + "\n" +
	       "ПолучательБанк1=ОАО БАНК ВТБ" + "\n" +
	       "ПолучательБанк2=Г МОСКВА" + "\n" +
	       "ПолучательБИК=044525187" + "\n" +
	       "ПолучательКорсчет=30101810700000000187" + "\n" +
	       "ВидПлатежа=электронно" + "\n" +
	       "ВидОплаты=01" + "\n" +
	       "СтатусСоставителя=" + "\n" +
	       "ПоказательКБК=" + "\n" +
	       "ОКАТО=" + "\n" +
	       "ПоказательОснования=" + "\n" +
	       "ПоказательПериода=" + "\n" +
	       "ПоказательНомера=" + "\n" +
	       "ПоказательДаты=" + "\n" +
	       "ПоказательТипа=" + "\n" +
	       "СрокПлатежа=" + "\n" +
	       "Очередность=5" + "\n" +
	       "НазначениеПлатежа=" + purposeOfPayment + "\n" +
	       "КонецДокумента" + "\n" +
	       "КонецФайла";
	        writer.write(text);         
	        writer.flush();
	    }
	    catch(IOException ex){
	         
	        System.out.println(ex.getMessage());
	    } 
		
	}

	protected void creditEnrollment() throws InterruptedException {
		operatorLogin();
		open(demoUrl + "/payment/receipt/import-list/");
		click(By.xpath("//button[contains(.,'Загрузка файла')]"));
		type(creditFilePath, By.xpath("//input[@id='paymentUpload-receiptFile']"));
		click(By.xpath("//button[contains(.,'Прикрепить')]"));
		waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
		click(By.xpath("//input[@value='Подписать и отправить']"));
		//waitForElementPresent(By.xpath("//div[@class='modal-body']"));
		//waitForInvisibility(By.xpath("//div[@class='modal-body']"));
		
		waitForInvisibility(By.xpath("//p[contains(.,'Успешно загружен')]"));
		//click(By.xpath("//a[@href='/payment/receipt/import-list/?lastImport=1']"));
		//waitForElementPresent(By.xpath("//input[@class=' btn btn-primary']"));
		open(demoUrl + "/payment/receipt/import-list/?tab=prepared&");
		//assertTrue(driver.findElements(By.xpath("//td[@class='row-id ']")).size() == 50);
		//print(Integer.toString(driver.findElements(By.xpath("//td[@class='row-id ']")).size()));
		click(By.xpath("//a[contains(.,'Провести')]"));
		waitForText("Платеж успешно проведен.", By.xpath("//div[@class='bootbox-body']"));
		//click(By.xpath("//button[contains(.,'Закрыть')]"));
		//Thread.sleep(10000);
		//assertEquals("Подтверждена (ДС поступили)",creditRequestStatus(creditRequestID));
	}

	public void waitForElementPresent(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected void waitForText(String string, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, string));
	}

	public void open(String string) {
		 //if (driver == null) {
			//   driver = new InternetExplorerDriver();
			  //}
		driver.get(string);
	}

	public void creditOrgLogin() {
		driver.get(demoUrl + "/logout");
	    driver.findElement(By.name("login[password]")).sendKeys("operator");
	    driver.findElement(By.name("login[username]")).sendKeys("ivanov5000");
	    driver.findElement(By.cssSelector("button.btn.btn-danger")).click();
	    WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("h1"), "Добро пожаловать в личный кабинет кредитной организации на Национальной электронной площадке."));
	}

	public void creditApprove() throws InterruptedException {
		//this.procedureNumber = "1076953626";
		open(demoUrl + "/credit/request/registry?&registrationNumber=" + procedureNumber + "&");
		click(By.xpath("//a[contains(.,'Обработка заявки')]"));
		this.purposeOfPayment = driver.findElement(By.xpath("//div[@id='request-recipient']")).getText();
		print(purposeOfPayment);
		this.creditRequestID = driver.findElement(By.xpath("//div[@id='request-uid']")).getText();
		print(creditRequestID);
		this.creditSumm = (driver.findElement(By.xpath("//div[@id='request-amount']")).getText()).replace(" ","");
		click(By.xpath("//button[contains(.,'Подтвердить')]"));
		type("Кредит на обеспечение", By.xpath("//textarea[@id='response-reason']"));
		click(By.xpath("//input[@value='Подтвердить']"));
		assertTrue(driver.findElement(By.cssSelector("div.bootbox-body")).getText().contains("Кредитная заявка одобрена"));
		assertEquals("Подтверждена (ожидание ДС)",creditRequestStatus(creditRequestID));
	}

	protected void print(String string) {
		System.out.println(string);
	}

	public void type(String string, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(string);
		/*waitForElementPresent(by);
		WebElement element = driver.findElement(by);
	    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, string);*/
	}

	protected void getCredit(String numberOfSupplier) throws Exception {
		//this.procedureNumber = "655469445";
		loginUser(numberOfSupplier);
		driver.get(demoUrl + "/procedure/catalog/?q=" + procedureNumber  + "&simple-search=Найти");
		waitForElementPresentRefresh(By.linkText("Заявка на кредит"));
		click(By.linkText("Заявка на кредит"));		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(0, 400)");
		//jse.executeScript("arguments[0].scrollIntoView()", driver.findElement(By.xpath("//div[@id='request-receivers-buttons-element']/button"))); 
		click(By.xpath("//div[@id='request-receivers-buttons-element']/button"));
		click(By.xpath("//input[@name='id']"));
		click(By.xpath("//button[contains(.,'Выбрать организацию')]"));
		waitForInvisibility(By.xpath("//div[@class='ajaxLoader modal-body']"));
		click(By.xpath("//input[@name='signAndSubmit']"));
		click(By.xpath("//button[contains(.,'Продолжить')]"));
		driver.findElement(By.xpath("//div[@class='bootbox-body']"));
		waitForInvisibility(By.xpath("//input[@name='signAndSubmit']"));
	    assertTrue(driver.findElement(By.cssSelector("div.bootbox-body")).getText().contains("Ваша кредитная заявка направлена в кредитные организации"));
	    open(demoUrl + "/credit/request/registry?&registrationNumber=" + procedureNumber + "&");
		assertEquals("Отправлена на рассмотрение",driver.findElement(By.xpath("//td[@class='row-status ']")).getText());
	}

	public void waitForInvisibility(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		//Wait<WebDriver> wait = new WebDriverWait(driver, 30, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	protected void contractProtocolPublicationCustomer(String numberOfCustomer) throws Exception {
			//this.procedureNumber = "749164860";

			loginUser(numberOfCustomer);
			open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
			waitForElementPresentRefresh(By.xpath("//a[contains(.,'Формирование проекта контракта')]"));
			jsClick(By.cssSelector(".ps-grid-action.action-create-project"));
			
			//Файл проекта контракта 
			if (elementDisplayed(By.id("contract-contractInfo-contractDocument")))
			{
			//print("1");
			type(fileForLoadPath, By.id("contract-contractInfo-contractDocument"));
			click(By.name("contract[contractInfo][contractDocument]-append"));
			waitForText("Успешно загружен", By.xpath("//div[@id='contract-contractInfo-contractDocument-element']/div/div/p[2]"));
			}
			
			//Обоснование отклонений позиций протокола разногласий
			if (elementExist(By.name("contract[deviationRejection][deviationRejectionDocuments][0][file][upload]")))
			{
			//print("2");
			type("123", By.name("contract[deviationRejection][deviationRejectionDocuments][0][title]"));
			type(fileForLoadPath, By.name("contract[deviationRejection][deviationRejectionDocuments][0][file][upload]"));
			click(By.name("contract[deviationRejection][deviationRejectionDocuments][0][file]-append"));
			waitForText("Успешно загружен", By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"));
			}
			//Thread.sleep(5000);
			//Иные документы по контракту 
			//if (elementDisplayed(By.name("contract[documentsInfo][customerDocuments][0][file][upload]")))
			if (elementDisplayed(By.name("contract[documentsInfo][customerDocuments][0][file][upload]")))
			{
			//print("3");
			type("123", By.name("contract[documentsInfo][customerDocuments][0][title]"));
			type(fileForLoadPath, By.name("contract[documentsInfo][customerDocuments][0][file][upload]"));
			click(By.name("contract[documentsInfo][customerDocuments][0][file]-append"));
			waitForText("Успешно загружен", By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"));
			}
			jsClick(By.name("signAndSubmit"));
		    waitForText("Проект контракта успешно отправлен участнику закупки", By.cssSelector("main"));
	}




	private boolean elementDisplayed(By by) {

		if (driver.findElement(by).isDisplayed())
		{
			return true; 
		}
		else return false;
	}


	protected void сontractRejectionSupplier(String numberOfSupplier) throws Exception {
		
		//this.procedureNumber = "420465246";
		
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Формирование протокола разногласий')]"));
		assertTrue(driver.findElement(By.xpath("//td[@class='row-statusTitle ']")).getText().contains("Подписание контракта участником"));
		click(By.cssSelector(".ps-grid-action.action-rejection-protocol-create"));
		type("123", By.name("contractRejection[documents][0][title]"));
		type(fileForLoadPath, By.name("contractRejection[documents][0][file][upload]"));
		click(By.name("contractRejection[documents][0][file]-append"));
		waitForText("Успешно загружен", By.xpath("//p[2]"));
		jsClick(By.name("signAndSubmit"));
		waitForInvisibility(By.name("signAndSubmit"));
		assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Протокол разногласий по проекту контракта направлен заказчику."));
	}

	public void click(By by) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		wait.until(ExpectedConditions.elementToBeClickable(by));
		//Actions actions = new Actions(driver);
		//actions.moveToElement(driver.findElement(by)).click().perform();
	    //Thread.sleep(1000);
		driver.findElement(by).click();
	}

	protected void contractSubscribeTimeStartForCustomerSQL() throws InterruptedException {
		//procedureNumber = "420465246";
		//getProcedureID();
		//int procedureid = 233;
	    
		//String url = "jdbc:mysql://10.99.1.1:9936/sectionks";
		//String url = "jdbc:mysql://77.232.63.5:3003/sectionks";
		
	    final Connection con;
	    final Statement stmt;
	    final ResultSet result;
	    
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		System.out.println("Время с которого заказчик может подписать контракт - " + time.format(formatter));
		String datatime = time.format(formatter);
		
	    //Запрос берет последний id контракта
	    //String query1 = "SELECT id FROM sectionks.procedureContract where procedureid='" + procedureid + "' order by id desc;";
	    
	    try {
	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	    stmt = con.createStatement();
	    //result = stmt.executeQuery(query1);
	    //result.next();
	    //this.contractid = Integer.toString(result.getInt("id"));
	    //String query2 = "UPDATE sectionks.procedureContract SET signedFromDataTime='" + datatime + "' where id='" + contractid + "';";
	    String query2 = "UPDATE sectionks.procedureContract SET signedFromDataTime='" + datatime + "' where procedureId='" + procedureid + "';";
	    stmt.executeUpdate(query2);
	    result1 = stmt.executeUpdate(query2);
	    con.close();
	    stmt.close();
	    //result.close();
	    } catch (SQLException sqlEx) {
	    	sqlEx.printStackTrace();}
	    System.out.println(result1);
	    System.out.println(time.format(formatter));
	    //System.out.println(contractid);
	}

	protected void сontractSubscribeCustomer(String numberOfCustomer) throws Exception {
		
		loginUser(numberOfCustomer);
		driver.get(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		waitForElementPresentRefresh(By.linkText("Подписать контракт"));
		driver.findElement(By.linkText("Подписать контракт")).click();
		driver.findElement(By.xpath("//input[@value='Подписать и отправить']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//h4[contains(.,'Подписать файл проекта контракта')]")));
		assertTrue(driver.findElement(By.xpath("//h4[contains(.,'Подписать файл проекта контракта')]")).getText().contains("Подписать файл проекта контракта"));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(.,'Подписать и отправить')]")));
		driver.findElement(By.xpath("//button[contains(.,'Подписать и отправить')]")).click();
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//button[contains(.,'Подписать и отправить')]")));
		waitForInvisibility(By.xpath("//button[contains(.,'Подписать и отправить')]"));
	    assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Контракт заключен."));
	}

	protected void сontractSubscribeSupplier(String numberOfSupplier) throws Exception {
		//int procedureNumber = 420465246;
		
		loginUser(numberOfSupplier);
		
		open(demoUrl + "/procedure/catalog/contract?registrationNumber=" + procedureNumber + "&");
		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-supplier-sign"));
		click(By.cssSelector(".ps-grid-action.action-supplier-sign"));
		
		driver.findElement(By.name("contract[contractInfo][executionRequirementDocuments][0][file][upload]")).clear();
	    driver.findElement(By.name("contract[contractInfo][executionRequirementDocuments][0][file][upload]")).sendKeys(fileForLoadPath);
	    driver.findElement(By.name("contract[contractInfo][executionRequirementDocuments][0][file]-append")).click();
	    driver.findElement(By.name("contract[contractInfo][executionRequirementDocuments][0][title]")).clear();
	    driver.findElement(By.name("contract[contractInfo][executionRequirementDocuments][0][title]")).sendKeys("123");
	    WebDriverWait wait = new WebDriverWait(driver, 20);
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tr[@id='row-1']/td[2]/div/p[2]"))));
	    assertTrue(driver.findElement(By.xpath("//tr[@id='row-1']/td[2]/div/p[2]")).getText().contains("Успешно загружен"));
	    driver.findElement(By.name("contract[documentsInfo][supplierDocuments][0][file][upload]")).clear();
	    driver.findElement(By.name("contract[documentsInfo][supplierDocuments][0][file][upload]")).sendKeys(fileForLoadPath);
	    driver.findElement(By.name("contract[documentsInfo][supplierDocuments][0][file]-append")).click();
	    driver.findElement(By.name("contract[documentsInfo][supplierDocuments][0][title]")).clear();
	    driver.findElement(By.name("contract[documentsInfo][supplierDocuments][0][title]")).sendKeys("123");
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//tr[@id='row-1']/td[2]/div/p[2])[2]"))));
	    assertTrue(driver.findElement(By.xpath("(//tr[@id='row-1']/td[2]/div/p[2])[2]")).getText().contains("Успешно загружен"));
	    driver.findElement(By.name("signAndSubmit")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(.,'Подписать и отправить')]")));
	    driver.findElement(By.xpath("//button[contains(.,'Подписать и отправить')]")).click();
	    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//button[contains(.,'Подписать и отправить')]")));
	    assertTrue(driver.findElement(By.cssSelector("main")).getText().contains("Проект контракта успешно подписан"));
	}

	protected void finalProtocolAllAdmittedCustomer(String numberOfCustomer) throws Exception {
		
		//this.procedureNumber = "899434231";
		//getProcedureID();
		
		loginUser(numberOfCustomer);
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8");

		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-result-secondparts"));  
		jsClick(By.cssSelector(".ps-grid-action.action-result-secondparts"));  
		
		 int i = 0;                          
		 while (driver.findElements(By.id("protocolReview-lot-requestList-" + i + "-admittance-select")).size() > 0)
		    {new Select(driver.findElement(By.id("protocolReview-lot-requestList-" + i + "-admittance-select"))).selectByVisibleText("Соответствует");
		    i++;}
		
	    type(fileForLoadPath, By.id("protocolReview-documentsBlock-protocolFinalDocument"));
	    Thread.sleep(1000);
	    jsClick(By.xpath("//button[contains(@name,'protocolReview[documentsBlock][protocolFinalDocument]-append')]"));
	    waitForText("Успешно загружен", By.xpath("//p[@class='upload-progress progress-success text-success']"));
	    click(By.name("signAndSubmit"));
	    waitForElementPresent(By.xpath("//h1[contains(.,'Протокол подведения итогов успешно опубликован')]"));
	    jsClick(By.xpath("//button[contains(.,'Вернуться')]"));
	    waitForText("Заключение контракта", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	}

	protected void waitForElementPresentRefresh(By by) throws InterruptedException {
		int i=0;
		int howManyRefresh=50;
		
		while (i<howManyRefresh) {
			if (driver.findElements(by).size()==0)
			//if (isElementPresent(by)==false)
			{
			driver.navigate().refresh();
			i++;
			if (i==howManyRefresh) { System.out.println("Элемент " + by + " не найден.");}
			}
			else break; 
			} 
	}

	protected void loadProcedure() throws InterruptedException {
		
		System.out.println();

		String purchaseName = "Закупка компьютеров для цеха №" + randomDigits(5);
		//String purchaseName = "Реестр Проблемных УЗ";
		String maxPrice = "236.15"; //1000.99
	    
	    LocalDateTime time = LocalDateTime.now();
		String dateToday = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +  "T" + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"))+"+03:00";
		String dateEndZayav = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T23:00:00+03:00";
		System.out.println(dateToday);

		String IDPack = randomDigits(36);
	    this.procedureNumber = randomDigits(19);

		//System.out.println(dateToday);
		//System.out.println(dateEndZayav);	    
    
	    //String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>2017-03-06T13:20:24.653+03:00</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>2017-03-06T13:21:29.167+03:00</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=464160000154139</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo/></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>2017-05-15T13:22:00+03:00</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo/></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:quantityUndefined>true</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + Zak + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>2000</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><!--<ns2:name>Начальная (максимальная) цена единицы услуги: Стоимость сутодачи</ns2:name>--><ns2:price>20000</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined></ns2:quantity><ns2:sum>10</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><!--<ns2:code>8403975</ns2:code>--><ns2:shortName>TR44</ns2:shortName><ns2:name>Требование об отсутствии в предусмотренном Федеральным законом № 44-ФЗ реестре недобросовестных поставщиков (подрядчиков, исполнителей) информации об участнике закупки, в том числе информации об учредителях, о членах коллегиального исполнительного органа, лице, исполняющем функции единоличного исполнительного органа участника закупки - юридического лица (в соответствии с частью 1.1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><!--<ns2:code>8632410</ns2:code>--><ns2:shortName>ET44</ns2:shortName><ns2:name>Единые требования к участникам (в соответствии  с пунктом 1 части 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>не установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	    // предыдущая String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=4641600006625555123</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole> <!--роль заказчика--><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo>доп инфо в блоке об организаторе закупки</ns2:addInfo></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate><ns2:addInfo>Дополнительная информация по заявкам</ns2:addInfo></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo> доп инфо Информация о проведении аукциона в электронной форме</ns2:addInfo></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><standardContractNumber>Тип усл конт-та</standardContractNumber><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><interbudgetaryTransfer>true</interbudgetaryTransfer> <ns2:quantityUndefined>false</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + Zak + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3 указывается единожды</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>14</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined>  <!--При приеме элемент должен содержать вариант «undefined» если для лота закупки невозможно определить количество товара, объем подлежащих выполнению работ, оказанию услуг--></ns2:quantity><ns2:sum>15</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><ns2:code>8361039</ns2:code><!--<ns2:shortName>3G44</ns2:shortName>--><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><ns2:code>8361040</ns2:code><!--<ns2:shortName>ET44</ns2:shortName>--><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	    //String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=4641600006625555123</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + Org + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><!--роль заказчика--><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo>доп инфо в блоке об организаторе закупки</ns2:addInfo></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate><ns2:addInfo>Дополнительная информация по заявкам</ns2:addInfo></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo> доп инфо Информация о проведении аукциона в электронной форме</ns2:addInfo></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><!--<standardContractNumber>Тип усл конт-та</standardContractNumber>--><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><interbudgetaryTransfer>true</interbudgetaryTransfer><ns2:quantityUndefined>false</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + Zak + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3 указывается единожды</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>14</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined><!--При приеме элемент должен содержать вариант «undefined» если для лота закупки невозможно определить количество товара, объем подлежащих выполнению работ, оказанию услуг--></ns2:quantity><ns2:sum>15</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><ns2:code>8361039</ns2:code><!--<ns2:shortName>3G44</ns2:shortName>--><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><ns2:code>8361040</ns2:code><!--<ns2:shortName>ET44</ns2:shortName>--><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	    String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>TEST</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=4641600006625555123</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + customer1EisNumber + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><!--роль заказчика--><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo>доп инфо в блоке об организаторе закупки</ns2:addInfo></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo> доп инфо Информация о проведении аукциона в электронной форме</ns2:addInfo></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:standardContractNumber>1231234567891234</ns2:standardContractNumber><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:interbudgetaryTransfer>true</ns2:interbudgetaryTransfer><ns2:quantityUndefined>false</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + customer2EisNumber + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3 указывается единожды</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>14</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined><!--При приеме элемент должен содержать вариант «undefined» если для лота закупки невозможно определить количество товара, объем подлежащих выполнению работ, оказанию услуг--></ns2:quantity><ns2:sum>15</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><ns2:code>8361039</ns2:code><!--<ns2:shortName>3G44</ns2:shortName>--><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><ns2:code>8361040</ns2:code><!--<ns2:shortName>ET44</ns2:shortName>--><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	    operatorLogin();
		
		open(baseUrl + "/integration/events/import");
		waitForElementPresent(By.id("content"));

		WebElement element = driver.findElement(By.id("content"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, procedureXml);
	    
	    driver.findElement(By.id("send")).click();
	    open(baseUrl + "/integration/events/index/objectId/" + procedureNumber + "/");
	    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));
	    System.out.println("Номер загруженной процедуры: " + procedureNumber);
	    assertTrue(!driver.findElement(By.cssSelector(".row-status")).getText().contains("Обработано с ошибками"));
	    
	    getProcedureID();
	    /*
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p.success")));
	    assertEquals("Пакет успешно импортирован. Результат обработки можно увидеть в мониторинге пакетов.", driver.findElement(By.cssSelector("p.success")).getText());
	    
	    driver.get(baseUrl + "/integration/monitor?&regNumber=" + procedureNumber + "&receiveDateTime-from=" + dateToday + "&");
	    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработан успешно')]"));
	    assertEquals("Обработан успешно", driver.findElement(By.xpath("//td[@class='row-statusIntegration ']")).getText());
	    */
	}

	
protected void loadProcedureNewCustomer() throws InterruptedException {
		
		System.out.println();

		String purchaseName = "Закупка бумаги новый заказчик для цеха №" + randomDigits(5);
	    String maxPrice = "139.65"; //1000.99
	    String rememberCustomer1EisOldNumber = customer1EisNumber;
	    String rememberCustomer2EisOldNumber = customer2EisNumber;
	    customer1EisNumber = eisNumberNewCustomer;
	    customer2EisNumber = eisNumberNewCustomer;		
	    
	    LocalDateTime time = LocalDateTime.now();
		String dateToday = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +  "T" + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"))+"+03:00";
		String dateEndZayav = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T23:00:00+03:00";
		System.out.println(dateToday);

		String IDPack = randomDigits(36);
	    this.procedureNumber = randomDigits(19);
	    
	    String procedureXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationEF xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>EF</objectType><objectId>" + procedureNumber + "</objectId><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>3243</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber><ns2:docPublishDate>" + dateToday + "</ns2:docPublishDate><ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=4641600006625555123</ns2:href><ns2:printForm><ns2:url>&lt;![CDATA[http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeId=3243&amp;xml=true]]&gt;</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTnpVMFdqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ25nU2wwSFhHVjBrdmR3RGhKZHNlZHNSZFN4ZW1UM1diaFZXVnpCa2x5dVl3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUU9GTUg2aUJicEEzTDV0UjMzTEtHQVEwV1NnMlZMVGpBa3IvUnpGaitUUkltTEc5ZUhDcmYxL29WUUNuQURYWVF3djl0bkt0UzJtWHk5bGtRRUJZUjRVPQ==</ns2:signature></ns2:printForm><ns2:purchaseObjectInfo>" + purchaseName  + "</ns2:purchaseObjectInfo><ns2:purchaseResponsible><ns2:responsibleOrg><ns2:regNum>" + customer1EisNumber + "</ns2:regNum><ns2:fullName>Государственное бюджетное учреждение «Заказчик 02»</ns2:fullName><ns2:postAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:postAddress><ns2:factAddress>Российская Федерация, 143005, Московская обл, Одинцовский р-н, Одинцово г, Чикина, 3</ns2:factAddress><ns2:INN>1222411099</ns2:INN><ns2:KPP>503211333</ns2:KPP></ns2:responsibleOrg><ns2:responsibleRole>CU</ns2:responsibleRole><!--роль заказчика--><ns2:responsibleInfo><ns2:orgPostAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgPostAddress><ns2:orgFactAddress>Российская Федерация, 662544, Красноярский край, Лесосибирск г, ПОБЕДЫ, 46</ns2:orgFactAddress><ns2:contactPerson><ns2:lastName>Медведчикова</ns2:lastName><ns2:firstName>Галина</ns2:firstName><ns2:middleName>Михайловна</ns2:middleName></ns2:contactPerson><ns2:contactEMail>lcgb-zakup@yandex.ru</ns2:contactEMail><ns2:contactPhone>8-39145-63374</ns2:contactPhone><ns2:contactFax>8-39145-63015</ns2:contactFax><ns2:addInfo>доп инфо в блоке об организаторе закупки</ns2:addInfo></ns2:responsibleInfo></ns2:purchaseResponsible><ns2:placingWay><ns2:code>EAP44</ns2:code><ns2:name>Электронный аукцион</ns2:name></ns2:placingWay><ns2:ETP><ns2:code>ETP_RAD</ns2:code><ns2:name>РАД</ns2:name><ns2:url>http://gz.lot-online.ru</ns2:url></ns2:ETP><ns2:procedureInfo><ns2:collecting><ns2:startDate>" + dateToday + "</ns2:startDate><ns2:place>Заявки на участие в аукционе направляется оператору электронной площадки по адресу электронной площадки в информационно-телекоммуникационной сети «Интернет» http://gz.lot-online.ru  </ns2:place><ns2:order>Заявка на участие в электронном аукционе направляется участником аукциона оператору электронной площадки в форме двух электронных документов, содержащих части заявки, предусмотренные п. 2.2. раздела 2 документации об аукционе. Указанные электронные документы подаются одновременно. </ns2:order><ns2:endDate>" + dateEndZayav + "</ns2:endDate></ns2:collecting><ns2:scoring><ns2:date>" + dateEndZayav + "</ns2:date></ns2:scoring><ns2:bidding><ns2:date>" + dateEndZayav + "</ns2:date><ns2:addInfo> доп инфо Информация о проведении аукциона в электронной форме</ns2:addInfo></ns2:bidding></ns2:procedureInfo><ns2:lot><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:standardContractNumber>1231234567891234</ns2:standardContractNumber><ns2:currency><ns2:code>RUB</ns2:code><ns2:name>Российский рубль</ns2:name></ns2:currency><ns2:financeSource>Федеральный бюджет</ns2:financeSource><ns2:interbudgetaryTransfer>true</ns2:interbudgetaryTransfer><ns2:quantityUndefined>false</ns2:quantityUndefined><ns2:customerRequirements><ns2:customerRequirement><ns2:customer><ns2:regNum>" + customer2EisNumber + "</ns2:regNum><ns2:fullName>Организация с ограниченной ответственностью \"Акапулько\"</ns2:fullName></ns2:customer><ns2:maxPrice>" + maxPrice + "</ns2:maxPrice><ns2:kladrPlaces><ns2:kladrPlace><ns2:kladr><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>24000008000</ns2:kladrCode><ns2:fullName>Российская федерация, Красноярский край, Лесосибирск г</ns2:fullName></ns2:kladr><ns2:deliveryPlace>улица Привокзальная, 47, пом. 3 указывается единожды</ns2:deliveryPlace></ns2:kladrPlace></ns2:kladrPlaces><ns2:deliveryTerm>до 15.12.2016 первая поставка – в течение 10 дней со дня заключения контракта, остальные ежеквартально равными частями.</ns2:deliveryTerm><ns2:applicationGuarantee><ns2:amount>20.00</ns2:amount><ns2:part>1.0</ns2:part><ns2:procedureInfo>обеспечение должно быть предоставлено до момента подачи заявки на участие в электронном аукционе. За несвоевременное предоставление обеспечения заявки отвечает участник закупки. </ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:applicationGuarantee><ns2:contractGuarantee><ns2:amount>100</ns2:amount><ns2:part>5.0</ns2:part><ns2:procedureInfo>Обеспечение исполнения контракта предоставляется участником закупки до подписания контракта.</ns2:procedureInfo><ns2:settlementAccount>40601810200003000002</ns2:settlementAccount><ns2:personalAccount>76192К72141</ns2:personalAccount><ns2:bik>040407001</ns2:bik></ns2:contractGuarantee><ns2:nonbudgetFinancings><ns2:nonbudgetFinancing><ns2:kvrCode>340</ns2:kvrCode><ns2:year>2016</ns2:year><ns2:sum>1683232.10</ns2:sum></ns2:nonbudgetFinancing><ns2:totalSum>1683232.10</ns2:totalSum></ns2:nonbudgetFinancings></ns2:customerRequirement></ns2:customerRequirements><ns2:purchaseObjects><ns2:purchaseObject><ns2:OKPD2><ns2:code>80.20.10.000</ns2:code><ns2:name>Услуги систем обеспечения безопасности</ns2:name></ns2:OKPD2><ns2:name>Техническое обслуживание и ремонт установок пожарной сигнализации, систем оповещения и управления эвакуацией людей при пожаре, систем дымоудаления при пожаре</ns2:name><ns2:OKEI><ns2:code>362</ns2:code><ns2:nationalCode>МЕС</ns2:nationalCode></ns2:OKEI><ns2:price>14</ns2:price><ns2:quantity><ns2:undefined>true</ns2:undefined><!--При приеме элемент должен содержать вариант «undefined» если для лота закупки невозможно определить количество товара, объем подлежащих выполнению работ, оказанию услуг--></ns2:quantity><ns2:sum>15</ns2:sum></ns2:purchaseObject><ns2:totalSum>10</ns2:totalSum></ns2:purchaseObjects><ns2:preferenses><ns2:preferense><ns2:code>8361032</ns2:code><!--<ns2:shortName>IN44</ns2:shortName>--><ns2:name>Организациям инвалидов (в соответствии со статьей 29 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361033</ns2:code><!--<ns2:shortName>UG44</ns2:shortName>--><ns2:name>Учреждениям и предприятиям уголовно-исполнительной системы (в соответствии со статьей 28 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361034</ns2:code><!--<ns2:shortName>MP44</ns2:shortName>--><ns2:name>Субъектам малого предпринимательства и социально ориентированным некоммерческим организациям (в соответствии со статьей 30 Федерального закона № 44-ФЗ)</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8361036</ns2:code><!--<ns2:shortName>MPSP44</ns2:shortName>--><ns2:name>Участникам, привлекающим Субъекты малого предпринимательства в качестве соисполнителей, субподрядчиков для исполнения контракта, и участникам, привлекающим Социально ориентированные некоммерческие организации в качестве соисполнителей, субподрядчиков для </ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense><ns2:preferense><ns2:code>8445051</ns2:code><!--<ns2:shortName>RBK44</ns2:shortName>--><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:prefValue>10</ns2:prefValue></ns2:preferense></ns2:preferenses><ns2:requirements><ns2:requirement><ns2:code>8361039</ns2:code><!--<ns2:shortName>3G44</ns2:shortName>--><ns2:name>Требование о выполнении участниками запроса котировок за последние три года, предшествующие дате размещения извещения о проведении запроса котировок, работ по строительству, реконструкции, капитальному ремонту объектов капитального строительства (в соотве</ns2:name><ns2:content>Установлено</ns2:content></ns2:requirement><ns2:requirement><ns2:code>8361040</ns2:code><!--<ns2:shortName>ET44</ns2:shortName>--><ns2:name>Единые требования к участникам (в соответствии с частью 1 Статьи 31 Федерального закона № 44-ФЗ)</ns2:name><ns2:content>Единые требования к участникам</ns2:content></ns2:requirement></ns2:requirements><ns2:restrictions><ns2:restriction><ns2:shortName>RBK44</ns2:shortName><ns2:name>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:name><ns2:content>Участникам, заявки или окончательные предложения которых содержат предложения о поставке товаров в соответствии с приказом Минэкономразвития России № 155 от 25.03.2014</ns2:content></ns2:restriction></ns2:restrictions><ns2:restrictInfo>не установлено</ns2:restrictInfo><ns2:restrictForeignsInfo>установлено</ns2:restrictForeignsInfo><ns2:noPublicDiscussion>false</ns2:noPublicDiscussion></ns2:lot><ns2:attachments><ns2:attachment><ns2:publishedContentId>290DF15BC6F70036E053AC1107251AE1</ns2:publishedContentId><ns2:fileName>АД  на смеси.doc</ns2:fileName><ns2:fileSize>311 Кб</ns2:fileSize><ns2:docDescription>АД  </ns2:docDescription><ns2:url>http://zakupki.gov.ru/44fz/filestore/public/1.0/download/priz/file.html?uid=290DF15BC6F70036E053AC1107251AE1</ns2:url><ns2:cryptoSigns><ns2:signature type=\"CAdES-BES\">TUlJTldnWUpLb1pJaHZjTkFRY0NvSUlOU3pDQ0RVY0NBUUV4RERBS0JnWXFoUU1DQWdrRkFEQUxCZ2txaGtpRzl3MEJCd0dnZ2drcU1JSUpKakNDQ05XZ0F3SUJBZ0lERFJuL01BZ0dCaXFGQXdJQ0F6Q0NBVjB4R0RBV0Jna3Foa2lHOXcwQkNRSVRDVk5sY25abGNpQkRRVEVnTUI0R0NTcUdTSWIzRFFFSkFSWVJkV05mWm10QWNtOXphMkY2Ym1FdWNuVXhIREFhQmdOVkJBZ01FemMzSU5DekxpRFFuTkMrMFlIUXV0Q3kwTEF4R2pBWUJnZ3FoUU1EZ1FNQkFSSU1NREEzTnpFd05UWTROell3TVJnd0ZnWUZLb1VEWkFFU0RURXdORGMzT1Rjd01UazRNekF4TERBcUJnTlZCQWtNSTlHRDBMdlF1TkdHMExBZzBKalF1OUdNMExqUXZkQzYwTEFzSU5DMDBMN1F2Q0EzTVJVd0V3WURWUVFIREF6UW5OQyswWUhRdXRDeTBMQXhDekFKQmdOVkJBWVRBbEpWTVRnd05nWURWUVFLREMvUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0QzFJTkM2MExEUXQ5QzkwTERSaDlDMTBMblJnZEdDMExMUXZqRS9NRDBHQTFVRUF3dzIwS1BRcGlEUXBOQzEwTFRRdGRHQTBMRFF1OUdNMEwzUXZ0Q3owTDRnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0Q3dNQjRYRFRFMU1EWXhPVEF6TXpneU4xb1hEVEUyTURreE9UQXpNemd5TjFvd2dnSFFNUm93R0FZSUtvVURBNEVEQVFFU0REQXdNalExTkRBd01qVXdNREVXTUJRR0JTcUZBMlFERWdzd016WXdNelk0TURjek5URVlNQllHQlNxRkEyUUJFZzB4TURJeU5EQXhOVEE0TnpnNE1SOHdIUVlKS29aSWh2Y05BUWtCRmhCc1pYTmpaMkpBZVdGdVpHVjRMbkoxTVFzd0NRWURWUVFHRXdKU1ZURXRNQ3NHQTFVRUNBd2tNalFnMEpyUmdOQ3cwWUhRdmRDKzBZL1JnTkdCMExyUXVOQzVJTkM2MFlEUXNOQzVNUjh3SFFZRFZRUUhEQmJRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNk1UVXdNd1lEVlFRS0RDelFtdENUMEpIUW85Q1hJQ0xRbTlDMTBZSFF2dEdCMExqUXNkQzQwWURSZ2RDNjBMRFJqeURRbk5DUklqRXFNQ2dHQTFVRUtnd2gwSlBRc05DNzBMalF2ZEN3SU5DYzBMalJoZEN3MExuUXU5QyswTExRdmRDd01TRXdId1lEVlFRRURCalFuTkMxMExUUXN0QzEwTFRSaDlDNDBMclF2dEN5MExBeE56QTFCZ05WQkF3TUx0QzkwTERSaDlDdzBMdlJqTkM5MExqUXVpRFF2dEdDMExUUXRkQzcwTEFnMExmUXNOQzYwWVBRdjlDKzBMb3hRekJCQmdOVkJBTU1PdENjMExYUXROQ3kwTFhRdE5HSDBMalF1dEMrMExMUXNDRFFrOUN3MEx2UXVOQzkwTEFnMEp6UXVOR0YwTERRdWRDNzBMN1FzdEM5MExBd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQTNUM01zQUdjVHdxa2Y0bXV0KytjUWRNWVIyL0VlQ2RqMEFQaU0rU2ZQOEFkdWc1dUdGTzhmNlV4Z2tJT3hqZ0ZWMHVybGtqa3NFa2xvSDgrMU94K1c2T0NCUU13Z2dUL01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSMGdCQll3RkRBSUJnWXFoUU5rY1FFd0NBWUdLb1VEWkhFQ01IY0dBMVVkRVFSd01HNmdFZ1lEVlFRTW9Bc1RDVFl4TnpFeE5ERTFNNkFaQmdvcWhRTURQWjdYTmdFSG9Bc1RDVEkwTlRRd01UQXdNYUFiQmdvcWhRTURQWjdYTmdFRm9BMFRDekF6TVRrek1EQXdOelkwb0IwR0NpcUZBd005bnRjMkFRaWdEeE1OTURNeE9UTXdNREF3TVRjNE9ZWUJNREEyQmdVcWhRTmtid1F0RENzaTBKclJnTkM0MEwvUmd0QyswSi9SZ05DK0lFTlRVQ0lnS05DeTBMWFJnTkdCMExqUmp5QXpMallwTUlJQllRWUZLb1VEWkhBRWdnRldNSUlCVWd4RUl0Q2EwWURRdU5DLzBZTFF2dENmMFlEUXZpQkRVMUFpSUNqUXN0QzEwWURSZ2RDNDBZOGdNeTQyS1NBbzBMalJnZEMvMEw3UXU5QzkwTFhRdmRDNDBMVWdNaWtNYUNMUW45R0EwTDdRczlHQTBMRFF2TkM4MEwzUXZpM1FzTkMvMEwvUXNOR0EwTERSZ3RDOTBZdlF1U0RRdXRDKzBMelF2OUM3MExYUXV0R0JJQ0xRcnRDOTBMalJnZEMxMFlEUmdpM1FrOUNlMEtIUW9pSXVJTkNTMExYUmdOR0IwTGpSanlBeUxqRWlERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJMExUSXlNemdnMEw3UmdpQXdOQzR4TUM0eU1ERXpERS9Rb2RDMTBZRFJndEM0MFlUUXVOQzYwTERSZ2lEUmdkQyswTDdSZ3RDeTBMWFJndEdCMFlMUXN0QzQwWThnNG9TV0lOQ2gwS1F2TVRJNExUSXhOelVnMEw3UmdpQXlNQzR3Tmk0eU1ERXpNQTRHQTFVZER3RUIvd1FFQXdJRDZEQnJCZ05WSFNVRVpEQmlCZ2dyQmdFRkJRY0RBZ1lJS3dZQkJRVUhBd1FHRGlxRkF3TTludGMyQVFZREJBRUJCZzRxaFFNRFBaN1hOZ0VHQXdRQkFnWU9Lb1VEQXoyZTF6WUJCZ01FQVFRR0RTcUZBd005bnRjMkFRWURCUUVHRFNxRkF3TTludGMyQVFZREJRSXdLd1lEVlIwUUJDUXdJb0FQTWpBeE5UQTJNVGd3TWpFMU16ZGFnUTh5TURFMk1Ea3hPREF5TVRVek4xb3dnZ0dQQmdOVkhTTUVnZ0dHTUlJQmdvQVVubkVPRDlxMEFTaGZQK0xMajJVVmx3SkhqS3VoZ2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3Z2dFQk1GNEdBMVVkSHdSWE1GVXdLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjbTl6YTJGNmJtRXVjblV2WTNKc0wyWnJNREV1WTNKc01DaWdKcUFraGlKb2RIUndPaTh2WTNKc0xtWnpabXN1Ykc5allXd3ZZM0pzTDJack1ERXVZM0pzTUIwR0ExVWREZ1FXQkJUKzJKMU0rbVpJSnNkbjlmYkdoY1pCcGR6cXF6QUlCZ1lxaFFNQ0FnTURRUUFNKzJ5Sk1mdWkrK2VSbTdyKythemdOOHdyUGk2NktGdkV1U3RWVmFFY01mSC81NHIxK0lIRkJ5ZFE5RzJSbXN6VnV4Q1NCbVJiZ1o5WmFSWk9MazB5TVlJRDl6Q0NBL01DQVFFd2dnRm1NSUlCWFRFWU1CWUdDU3FHU0liM0RRRUpBaE1KVTJWeWRtVnlJRU5CTVNBd0hnWUpLb1pJaHZjTkFRa0JGaEYxWTE5bWEwQnliM05yWVhwdVlTNXlkVEVjTUJvR0ExVUVDQXdUTnpjZzBMTXVJTkNjMEw3UmdkQzYwTExRc0RFYU1CZ0dDQ3FGQXdPQkF3RUJFZ3d3TURjM01UQTFOamczTmpBeEdEQVdCZ1VxaFFOa0FSSU5NVEEwTnpjNU56QXhPVGd6TURFc01Db0dBMVVFQ1F3ajBZUFF1OUM0MFliUXNDRFFtTkM3MFl6UXVOQzkwTHJRc0N3ZzBMVFF2dEM4SURjeEZUQVRCZ05WQkFjTUROQ2MwTDdSZ2RDNjBMTFFzREVMTUFrR0ExVUVCaE1DVWxVeE9EQTJCZ05WQkFvTUw5Q2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFVnMExyUXNOQzMwTDNRc05HSDBMWFF1ZEdCMFlMUXN0QytNVDh3UFFZRFZRUURERGJRbzlDbUlOQ2swTFhRdE5DMTBZRFFzTkM3MFl6UXZkQyswTFBRdmlEUXV0Q3cwTGZRdmRDdzBZZlF0ZEM1MFlIUmd0Q3kwTEFDQXcwWi96QUtCZ1lxaFFNQ0Fna0ZBS0NDQWlnd0dBWUpLb1pJaHZjTkFRa0RNUXNHQ1NxR1NJYjNEUUVIQVRBY0Jna3Foa2lHOXcwQkNRVXhEeGNOTVRZd01URXhNVEUwTmpJeldqQXZCZ2txaGtpRzl3MEJDUVF4SWdRZ0hheVhoVTY1dUU3MTRERXVCM0pWeS9QbFZ0QnRBQi9tRUxVaU5FTFRYMEV3Z2dHN0Jnc3Foa2lHOXcwQkNSQUNMekdDQWFvd2dnR21NSUlCb2pDQ0FaNHdDQVlHS29VREFnSUpCQ0RaVGJQRU5GRDJIK0lLTlhjSmVISVJYQm1xSnJMYVUxRHQ5LzA0TXRETHl6Q0NBVzR3Z2dGbHBJSUJZVENDQVYweEdEQVdCZ2txaGtpRzl3MEJDUUlUQ1ZObGNuWmxjaUJEUVRFZ01CNEdDU3FHU0liM0RRRUpBUllSZFdOZlptdEFjbTl6YTJGNmJtRXVjblV4SERBYUJnTlZCQWdNRXpjM0lOQ3pMaURRbk5DKzBZSFF1dEN5MExBeEdqQVlCZ2dxaFFNRGdRTUJBUklNTURBM056RXdOVFk0TnpZd01SZ3dGZ1lGS29VRFpBRVNEVEV3TkRjM09UY3dNVGs0TXpBeExEQXFCZ05WQkFrTUk5R0QwTHZRdU5HRzBMQWcwSmpRdTlHTTBMalF2ZEM2MExBc0lOQzAwTDdRdkNBM01SVXdFd1lEVlFRSERBelFuTkMrMFlIUXV0Q3kwTEF4Q3pBSkJnTlZCQVlUQWxKVk1UZ3dOZ1lEVlFRS0RDL1FwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEMxSU5DNjBMRFF0OUM5MExEUmg5QzEwTG5SZ2RHQzBMTFF2akUvTUQwR0ExVUVBd3cyMEtQUXBpRFFwTkMxMExUUXRkR0EwTERRdTlHTTBMM1F2dEN6MEw0ZzBMclFzTkMzMEwzUXNOR0gwTFhRdWRHQjBZTFFzdEN3QWdNTkdmOHdDZ1lHS29VREFnSVRCUUFFUUg3KzdsQVBuQ05vcTVhRDFUeTd5REpUakRyd1lKNlJQTjVIM0VlcHRpS1N1elpNV1FOTnlERTdCRzNtMEJyVER6L0h6dERWVzFoZm0vY002ZUtveEdRPQ==</ns2:signature></ns2:cryptoSigns></ns2:attachment></ns2:attachments><!--<ns2:modification><ns2:modificationNumber>1</ns2:modificationNumber><ns2:info>ошибочно не указано ограничение в отношении участников в извещении и документации</ns2:info><ns2:addInfo/><ns2:reason><ns2:responsibleDecision><ns2:decisionDate>2016-03-04T00:00:00+12:00</ns2:decisionDate></ns2:responsibleDecision></ns2:reason></ns2:modification>--></data></fcsNotificationEF>";
	    operatorLogin();
		
		open(baseUrl + "/integration/events/import");
		waitForElementPresent(By.id("content"));

		WebElement element = driver.findElement(By.id("content"));
	    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, procedureXml);
	    
	    driver.findElement(By.id("send")).click();
	    open(baseUrl + "/integration/events/index/objectId/" + procedureNumber + "/");
	    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));
	    System.out.println("Номер загруженной процедуры: " + procedureNumber);
	    getProcedureID();
	    customer1EisNumber = rememberCustomer1EisOldNumber;
	    customer2EisNumber = rememberCustomer2EisOldNumber;
	}
	
	
	
	protected void auctionEndSQL() throws SQLException, InterruptedException {
	//this.procedureNumber = "995613042";
	//getProcedureID();
	Thread.sleep(10000);
	LocalDateTime time = LocalDateTime.now();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	System.out.println("Время окончания аукциона - " + time.format(formatter));
	
	
	Connection con;
	Statement stmt;
	//final ResultSet rs;
	
	String endPhaseOneDateTime = "UPDATE sectionks_trade.auction SET endPhaseOneDateTime='" + time.format(formatter) + "' WHERE etpId=" + procedureid +";";
	String endPhaseTwoDateTime = "UPDATE sectionks_trade.auction SET endPhaseTwoDateTime='" + time.plusSeconds(10).format(formatter) + "' WHERE etpId=" + procedureid +";";
	String endHoldingDateTime = "UPDATE sectionks.procedures SET endHoldingDateTime='" + time.minusHours(1).format(formatter) + "' WHERE id=" + procedureid +";";
	
	try {

	con = DriverManager.getConnection(urlSectionks_trade, loginForDataBase, passwordForDataBase);
	stmt = con.createStatement();
	stmt.executeUpdate(endPhaseOneDateTime);
	Thread.sleep(25000);
	stmt.executeUpdate(endPhaseTwoDateTime);
	Thread.sleep(10000);
	
	con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	stmt = con.createStatement();
	stmt.executeUpdate(endHoldingDateTime);

	con.close();
	stmt.close();
	
	//rs = stmt.executeUpdate(query);
	} catch (SQLException sqlEx) {
		sqlEx.printStackTrace();
	} 

	
	}

	protected void operatorLogin() throws InterruptedException {
		
		open(baseUrl);
		open(baseUrl + "/admin/account/access/logout/");
		driver.get(baseUrl + "/admin/account/access/login/");
		//open("http://94-test.platformasoft.ru/admin/account/access/login/");
		waitForElementPresent(By.id("login-login")); //input[@id='login-password']
	    //type("operator1", By.id("login-login"));
		//driver.findElement(By.id("login-login")).sendKeys("operator1");
		driver.findElement(By.id("login-login")).sendKeys(operatorLogin);
		waitForElementPresent(By.id("login-password"));
	    
		driver.findElement(By.id("login-password")).sendKeys(operatorPassword);
	    
		//driver.findElement(By.cssSelector(".btn.btn-default")).click();
		jsClick(By.cssSelector(".btn.btn-default"));
	    waitForElementPresent(By.id("requestSupplier"));

	}

	protected void getProcedureID() throws InterruptedException {
		//procedureNumber = "0110500000417000026";
		
	
	    final Connection con;
	    final Statement stmt;
	    final ResultSet result;
	    Thread.sleep(10000);
	    String query = "SELECT id FROM sectionks.procedures where registrationNumber='" + procedureNumber + "';";
	    
	    try {
	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	    stmt = con.createStatement();
	    result = stmt.executeQuery(query);
	    result.next();
	    int procedureidInt = result.getInt("id");
	    this.procedureid = Integer.toString(procedureidInt);
	    System.out.println("ID процедуры: "+procedureid);
	    //rs = stmt.executeQuery(query);
	    con.close();
	    stmt.close();
	    result.close();
	    } catch (SQLException sqlEx) {
	    	sqlEx.printStackTrace();
	    	System.out.println("Загруженная процедура не появилась в БД"); 
	    	assertTrue(sqlEx == null);
	    } 
		
	}

	protected void getOfferAuctionSupplier(String numberOfSupplier) throws Exception {
		//this.procedureNumber = "0846432861873115803";
		//getProcedureID();
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber + "&simple-search=Найти");
		waitForElementPresentRefresh(By.xpath("//td[contains(.,'Идут торги')]"));
		waitForElementPresentRefresh(By.xpath("//a[contains(.,'Принять участие')]"));
		//waitForText("Идут торги", By.xpath("//td[@class='row-procedureStatusTitle ']"));
	    
		
		open(demoUrl + "/trade/offer/" + procedureid);
		
		waitForElementPresentRefresh(By.xpath("//div[@id='trade-offerInfo-offerWithStep']/a[1]"));
		
	    jsClick(By.xpath("//div[@id='trade-offerInfo-offerWithStep']/a[1]"));
	    //jsclick(By.xpath("//div[@id='trade-offerInfo-offerWithStep']/a[2]"));
	    Thread.sleep(1000);
	    click(By.id("saveOfferBtn"));
	    click(By.xpath("//button[contains(.,'Продолжить')]"));
	    click(By.id("close-window-alert-dialog"));		
	}

	protected void loginUser(String numberOfSupplier) throws Exception  {
		String loginName = null;
		String getPasswordForAllSuppliers = supplierPassword; //запоминаем пароль для всех юзеров, т.к. для новых юзеров пароль другой
		
		switch (numberOfSupplier) {
		case "IP": loginName=loginForNewSupplierIP; supplierPassword = passwordForNewSupplierIP; break;
		case "FL": loginName=loginForNewSupplierFL; supplierPassword = passwordForNewSupplierFL; break;
		case "UL": loginName=loginForNewSupplierUL; supplierPassword = passwordForNewSupplierUL; break;
		case "ULIG": loginName=loginForNewSupplierULIG; supplierPassword = passwordForNewSupplierULIG; break;
		case "U1": loginName=supplierLogin1; break;
		case "U2": loginName=supplierLogin2; break;
		case "U3": loginName=supplierLogin3; break;
		case "U4": loginName=supplierLogin4; break;
		case "U5": loginName=supplierLogin5; break;
		case "Z1": loginName=customerLogin1; break;
		case "Z2": loginName=customerLogin2; break;
		case "Z3": loginName=customerLogin3; break;
		case "newCustomer": loginName=loginForNewCustomer; break;
		}
		open(demoUrl + "/login");
		loginPagePassword = new LoginPagePassword(driver);
		//loginPagePassword.logout();
		loginPagePassword.userLogin(loginName,supplierPassword);
		
		//open(baseUrl + "/admin/account/access/logout/");
		//open(demoUrl + "/logout");
		//open(demoUrl + "/login");
		
		//waitForElementPresent(By.id("login-username"));
		//type(loginName, By.id("login-username"));
		
		//waitForElementPresent(By.id("login-password"));
		//type(supplierPassword, By.id("login-password"));
		//jsClick(By.name("submit"));
		//System.out.println(supplierPassword);*/
		waitForElementPresent(By.cssSelector(".header-block-text"));
		//если оператор решил создать окно-уведомление - ждать его появления и нажать "ознакомлен"
		if (elementExist(By.name("alert-read"))) {click(By.name("alert-read")); }
		//для новых юзеров устанавливаются новые пароли, после их логина возвращаются пароли для старых юзеров
		supplierPassword=getPasswordForAllSuppliers;
	}

	
	protected void auctionStartSQL() throws InterruptedException {
		//this.procedureNumber = "714712187";
		//getProcedureID();

		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		System.out.println("Время начала аукциона - " + time.format(formatter));
		                             
		Connection con;
	    Statement stmt;
	    String datatime = time.format(formatter);

	    String query2 = "UPDATE sectionks.procedures SET conditionalHoldingDateTime='" + datatime + "' WHERE id='" + procedureid +"';";
	    
	    try {
		    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
		    stmt = con.createStatement();
		    stmt.executeUpdate(query2);
		    con.close();
		    stmt.close();
		    } catch (SQLException sqlEx) {
		    	sqlEx.printStackTrace();
		    } 
	    Thread.sleep(15000);
	    String query1 = "UPDATE sectionks_trade.auction SET startHoldingDateTime='" + datatime + "' WHERE etpId='" + procedureid +"';";
	    try {
		    con = DriverManager.getConnection(urlSectionks_trade, loginForDataBase, passwordForDataBase);
		    stmt = con.createStatement();
		    stmt.executeUpdate(query1);
		    con.close();
		    stmt.close();
		    } catch (SQLException sqlEx) {
		    	sqlEx.printStackTrace();
		    } 
	    Thread.sleep(10000);
	}

	protected void requestProtocolAllAdmittedCustomer(String numberOfCustomer) throws Exception {
		    //procedureNumber = "926829748";
		    //getProcedureID();
			loginUser(numberOfCustomer);
			open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
		    int howmanywait = 20;
		    int i=0;	//обновление страницы до появления ссылки на протокол
		    while (i<howmanywait){
		    	if (elementExist(By.cssSelector(".ps-grid-action.action-request-firstpart-review")))	
		        {
		    	click(By.cssSelector(".ps-grid-action.action-request-firstpart-review"));
			    type(fileForLoadPath, By.id("protocolReview-documentsBlock-protocolDocument"));
		    	click(By.name("protocolReview[documentsBlock][protocolDocument]-append"));
		    	waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
		    	
		    	int j = 0;
			    while (elementExist(By.id("protocolReview-lot-requestList-" + j + "-admittance-select")))
			    {select("Допущена", By.id("protocolReview-lot-requestList-" + j + "-admittance-select"));
			    j++;}
		    	
		    	jsClick(By.name("signAndSubmit"));
			    waitForText("Протокол рассмотрения заявок успешно опубликован",By.cssSelector("h1"));
		    	break;}
		    	
		    	//заполнение формы для протокола с одной заявкой
		    	if (elementExist(By.cssSelector(".ps-grid-action.action-result-secondparts-single-request")))
		    	{	
		    	jsClick(By.cssSelector(".ps-grid-action.action-result-secondparts-single-request"));
		    	new Select(driver.findElement(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-0-admittance-select')]"))).selectByVisibleText("Соответствует");
		    	driver.findElement(By.xpath("//input[@id='protocolReview-documentsBlock-protocolFinalDocument']")).sendKeys(fileForLoadPath);
			    jsClick(By.xpath("//button[@name='protocolReview[documentsBlock][protocolFinalDocument]-append']"));
			    //waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
			    waitForElementPresent(By.name("signAndSubmit"));
			    jsClick(By.name("signAndSubmit"));
			    waitForText("Протокол рассмотрения единственной заявки успешно опубликован",By.cssSelector("h1"));
			    break;
			    }
		    	
		    open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
		    i++;}
		    
	if (i==howmanywait) {print("Ссылки на ПРЗ не обнаружено.");assertTrue(!(i==howmanywait));}
		    }		   
	
	//заполнение формы протокола 
		    /*i = 0;
		    while (elementExist(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]")))
		    {new Select(driver.findElement(By.xpath("//select[contains(@id,'protocolReview-lot-requestList-" + i + "-admittance-select')]"))).selectByVisibleText("Соответствует");
		    i++;}
		    //click(By.xpath("(//a[contains(text(),'Рассмотрение заявок')])[2]"));
		    //WebDriverWait wait = new WebDriverWait(driver, 15);
			//wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("setAllAdmittance")));
			//driver.findElement(By.id("setAllAdmittance")).sendKeys(Keys.ENTER);
		    /*WebElement element = driver.findElement(By.cssSelector("button.btn.btn-primary"));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();*/
		    //driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		    //click(By.cssSelector("button.btn.btn-primary"));
		    
		    //заполнение формы для протокола с несколькими заявками 
		    /*if (elementExist(By.id("protocolReview-documentsBlock-protocolDocument"))){
		    driver.findElement(By.id("protocolReview-documentsBlock-protocolDocument")).sendKeys(fileForLoadPath);
		    click(By.name("protocolReview[documentsBlock][protocolDocument]-append"));
		    waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
		    if (isElementPresent(By.xpath("//input[@value='Опубликовать сейчас']"))==true) {driver.findElement(By.xpath("//input[@value='Опубликовать сейчас']")).click();}
		    else {driver.findElement(By.name("signAndSubmitNow")).click();
		    waitForText("Протокол рассмотрения заявок успешно опубликован",By.cssSelector("h1"));}
		    }
		    //заполнение формы для протокола с одной заявкой
		    else 
		    if (elementExist(By.xpath("//input[@id='protocolReview-documentsBlock-protocolFinalDocument']"))){
		    	driver.findElement(By.xpath("//input[@id='protocolReview-documentsBlock-protocolFinalDocument']")).sendKeys(fileForLoadPath);
			    click(By.xpath("//button[@name='protocolReview[documentsBlock][protocolFinalDocument]-append']"));
			    waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
			    if (isElementPresent(By.xpath("//input[@value='Опубликовать сейчас']"))==true) {driver.findElement(By.xpath("//input[@value='Опубликовать сейчас']")).click();}
			    else driver.findElement(By.name("signAndSubmitNow")).click();
			    waitForText("Протокол рассмотрения единственной заявки успешно опубликован",By.cssSelector("h1"));
		    }
		    
		    
		    /*for (int second = 0;; second++) {
		    	if (second >= 60) fail("timeout");
		    	try { if (isElementPresent(By.linkText("1.txt"))) break;Thread.sleep(100);} catch (Exception e) {}}*/
		    
		    /*driver.findElement(By.name("protocolReview[documentsBlock][documents][0][file][upload]")).sendKeys(fileForLoadPath);
		    click(By.name("protocolReview[documentsBlock][documents][0][file]-append"));
		    driver.findElement(By.name("protocolReview[documentsBlock][documents][0][title]")).sendKeys("123");*/
		    
		    //if (isElementPresent(By.xpath("//input[@value='Опубликовать сейчас']"))==true) {driver.findElement(By.xpath("//input[@value='Опубликовать сейчас']")).click();}
		    //else driver.findElement(By.name("signAndSubmitNow")).click();
	
		    //waitForText("Протокол рассмотрения заявок успешно опубликован",By.cssSelector("h1"));*/
	


	public void jsClick(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		wait.until(ExpectedConditions.elementToBeClickable(by));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", driver.findElement(by));
	} 

	

	
	
	
	protected void requestTimeEndSQL() {
		
		//this.procedureNumber="2017041117483352286";
		
		
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		time=time.minusHours(1);
			
		System.out.println("Время окончания подачи заявок - " + time.format(formatter));
		
		//ftest
		//String url = "jdbc:mysql://10.99.1.1:9936/sectionks";
		
		//dev
		//String url = "jdbc:mysql://10.31.66.1:3306/sectionks";
		//RAD
		//String url = "jdbc:mysql://77.232.63.5:3003/sectionks";
		
		//RADboy
		//String url = "jdbc:mysql://185.147.81.17:3004/sectionks_trade";
		//String urlSectionks = "jdbc:mysql://185.147.81.17:3001/sectionks";
		
	    final Connection con;
	    final Statement stmt;
	    String datatime = time.format(formatter);
	    
	    String query = "UPDATE sectionks.procedures SET requestEndGiveDateTime='" + datatime + "' WHERE registrationNumber='" + procedureNumber +"';";
	    
	    try {
	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	    stmt = con.createStatement();
	    stmt.executeUpdate(query);
	    con.close();
	    stmt.close();
	    } catch (SQLException sqlEx) {
	    	sqlEx.printStackTrace();
	    } 
		
	}

	public void giveMoneyForSupplier(String numberOfSupplier) throws Exception {
		loginUser(numberOfSupplier);
		open(demoUrl + "/payment/transaction/list/");
		String moneyAccounNumber = driver.findElement(By.xpath(".//*[@class='container container-wrapper']/section/main/b")).getText();
		print(moneyAccounNumber);
		String howMuchMoney = "1000000";
		
		Connection con = null;
	    Statement stmt = null;
	    
	    try 
	    {
	  	    	con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
		  	    stmt = con.createStatement();
		  	    stmt.executeUpdate("UPDATE sectionks.paymentAccount SET amount='" + howMuchMoney + "' where number= '" + moneyAccounNumber + "';");
	  	    	
	  	  con.close();
		  stmt.close();

		  } 
		    catch (SQLException sqlEx) 
		  {
		    sqlEx.printStackTrace();
		  }
		
	}
	
	public void getRequestSupplier(String numberOfSupplier) throws Exception {
		//this.procedureNumber = "838025377";
		//getProcedureID();
		
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/?q=" + procedureNumber  + "&simple-search=Найти");
		waitForElementPresentRefresh(By.linkText("Подать заявку")); 
		jsClick(By.linkText("Подать заявку"));
		
	    waitForElementPresent(By.id("request-signature-digitalSignature"));
	    if (elementExist(By.name("request[secondPart][documents][approve-large-transactions][0][file][upload]")))
	    {
		if (elementDisplayed(By.name("request[secondPart][documents][approve-large-transactions][0][file][upload]")))
		{
	    
		driver.findElement(By.name("request[secondPart][documents][approve-large-transactions][0][file][upload]")).sendKeys(fileForLoadPath);	
	    Thread.sleep(1000);
		jsClick(By.name("request[secondPart][documents][approve-large-transactions][0][file]-append"));
	    type("123", By.name("request[secondPart][documents][approve-large-transactions][0][title]"));
	    waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
	    }	    
	    }
	    /*if (elementExist(By.name("request[secondPart][documents][approve-large-transactions][0][title]")))
	    {
	    	type("123", By.name("request[secondPart][documents][approve-large-transactions][0][title]"));
		    waitForElementPresent(By.xpath("//p[contains(.,'Успешно загружен')]"));
	    }*/

	    if (elementExist(By.id("request-secondPart-organizationInfo-organizationInfo-passportData")))
	    {
	    	type("123456789", By.id("request-secondPart-organizationInfo-organizationInfo-passportData"));
	    }
	    jsClick(By.name("sign"));
	    Thread.sleep(1000);
	    jsClick(By.xpath("//button[@class='btn btn-primary']"));
	    jsClick(By.xpath("//button[@class='btn btn-primary']"));
	    jsClick(By.xpath("//button[@class='btn btn-primary']"));
	    waitForText("Заявка успешно подана", By.cssSelector("h1"));
	    if (driver.findElement(By.cssSelector("main")).getText().contains("В связи с тем, что недостаточно средств для проведения операции, заявка отклонена оператором.")) {System.out.println("В связи с тем, что недостаточно средств для проведения операции, заявка отклонена оператором.");};
	    assertTrue(!driver.findElement(By.cssSelector("main")).getText().contains("В связи с тем, что недостаточно средств для проведения операции, заявка отклонена оператором."));
	}
	
	
	@org.testng.annotations.AfterClass
	public void tearDown() throws Exception {
		//Thread.sleep(500000);
		driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      
	    }
	  }


	protected void cancelProcedure() throws InterruptedException  {

		    
		    
		    String IDPack = randomDigits(36);
		    System.out.println("id пакета: " + IDPack);
		    
			String cancelProcedure ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><fcsNotificationCancel xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\" xmlns:ns3=\"http://zakupki.gov.ru/oos/integration/rest/api/1\"><index><id>" + IDPack + "</id> <sender>OOC</sender><receiver>ETP_RAD</receiver><createDateTime>2016-06-20T19:30:00.983+03:00</createDateTime> <objectType>EF</objectType><objectId>" + procedureNumber + "</objectId> <indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:id>41911</ns2:id><ns2:purchaseNumber>" + procedureNumber + "</ns2:purchaseNumber> <ns2:docNumber>ФИО1</ns2:docNumber><ns2:docDate>2017-02-21T19:31:00.492+03:00</ns2:docDate> <ns2:docPublishDate>2017-03-17T19:32:00.145+03:00</ns2:docPublishDate> <ns2:href>http://zakupki.gov.ru/epz/order/notice/ea44/view/documents.html?regNumber=464160000154160</ns2:href> <ns2:printForm><ns2:url>http://zakupki.gov.ru/epz/order/notice/printForm/view.html?noticeCancelId=41911</ns2:url><ns2:signature type=\"CAdES-BES\">TUlJTTB3WU</ns2:signature></ns2:printForm><ns2:cancelReason> <ns2:responsibleDecision><ns2:decisionDate>2017-03-17T19:32:00.145+03:00</ns2:decisionDate> </ns2:responsibleDecision></ns2:cancelReason> <ns2:addInfo>Ошибочно указан  неверный адрес электронной площадки</ns2:addInfo></data></fcsNotificationCancel>";
			
			operatorLogin();

			open(baseUrl + "/integration/events/import");

			waitForElementPresent(By.id("content"));
			
			WebElement element = driver.findElement(By.id("content"));
		    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, cancelProcedure);
		    
		    driver.findElement(By.id("send")).click();   
		    
		    open(baseUrl + "/integration/events/index/packageId/" + IDPack + "/objectId/" + procedureNumber);
		    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));
		}

	protected void loadFile(By locatorFilename, By locatorFileLoad, By locatorAppendButton) throws Exception {

		type("documentForLoad", locatorFilename);
		driver.findElement(locatorFileLoad).sendKeys(fileForLoadPath);
		jsClick(locatorAppendButton);
		waitForInvisibility(locatorAppendButton);
		//print(driver.findElement(locatorAppendButton).getText());
		//waitForText("Удалить", locatorAppendButton);
	}
	
	protected void getCertificateData(String certificateName) throws InterruptedException{
		open(baseUrl + "/admin/account/access/logout/");
		open(demoUrl + "/logout");
		open(demoUrl + "/login-ecp");
		select(certificateName, By.id("login-ecp-certificateSelect"));
		jsClick(By.name("checkEcp"));
		waitForElementPresent(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[6]/td[2]"));
		this.certificateUserLastName = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[2]/td[2]")).getText().replace("\"", "");
		this.certificateUserFirstName = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[3]/td[2]")).getText();
		this.certificateUserMiddleName = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[4]/td[2]")).getText().replace("\"", "");
		this.organisationName = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[5]/td[2]")).getText().replace("\"", "");
		this.certificateInn = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[6]/td[2]")).getText().replace("\"", "");
		this.certificateOgrn = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[7]/td[2]")).getText().replace("\"", "");
		this.certificateSerial = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[8]/td[2]")).getText();
		this.certificateIssuer = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[9]/td[2]")).getText();
		print(certificateInn);
		this.orgRole = driver.findElement(By.xpath(".//*[@class='ps-grid-table table table-bordered table-striped']/tbody/tr[13]/td[2]")).getText();
		this.orgRole = orgRole.substring(0, orgRole.indexOf("."));
		print(orgRole);
		
		
		print(certificateSerial);
		print(organisationName);
	}
	
	
	protected void accreditationSupplier(String certificateName) throws Exception {
		
		getCertificateData (certificateName);
		
		String operatorCerificateName = "SN = Оператор (до: Tue Aug 15 11:14:12 UTC+0300 2017)";
		
		//Почта для подтверждения 
		String mail = "finsky@platformasoft.ru";
		this.loginForNewSupplier = "TestLogin" + randomDigits(4) + "!";
		print(loginForNewSupplier);
		this.passwordForNewSupplier = "Qwerty12!";
		String contactLastName = certificateName.substring(0,certificateName.indexOf(" "));//"Иванов";
		String contactFirstName = certificateName.substring(certificateName.indexOf(" ")+1, certificateName.indexOf(" ", certificateName.indexOf(" ")+1));//"Жора";


		open(demoUrl + "/logout");
		open(baseUrl + "/account/registration/introduction/formType/supplier/");
		select(orgRole, By.id("type"));
	    //driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(loginForNewSupplier);
	    //driver.findElement(By.id("password")).clear();
	    driver.findElement(By.id("password")).sendKeys(passwordForNewSupplier);
	    //driver.findElement(By.id("passwordRepeat")).clear();
	    driver.findElement(By.id("passwordRepeat")).sendKeys(passwordForNewSupplier);
	    //driver.findElement(By.id("codePhrase")).clear();
	    driver.findElement(By.id("codePhrase")).sendKeys(loginForNewSupplier);
	    //driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(mail);
		//String cerificateName = driver.findElement(By.id("certificate")).getText();
		//System.out.println(cerificateName);
	    select(certificateName, By.id("certificate"));
		//new Select(driver.findElement(By.id("certificate")).selectByVisibleText(driver.findElement(By.id("certificate")).getText().contains("Иванов Иван Дмитриевич")));
		//driver.findElement(By.id("certificate")).getText().contains(* + "Иванов Иван Дмитриевич" + *);
		jsClick(By.id("save-introduction"));
		type(loginForNewSupplier, By.id("login-login"));
		type(passwordForNewSupplier, By.id("login-password"));
		jsClick(By.cssSelector(".btn.btn-default"));
		
		
		if (orgRole.equalsIgnoreCase("Юридическое лицо"))
		{
			type(randomDigits(9), By.id("mainData-kpp"));
			type(orgRole, By.id("mainData-formOfIncorporation"));		
			type("17.02.2017", By.id("mainData-registrationDate"));
			//type(mail, By.id("mainData-mainEmail"));	
			((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", driver.findElement(By.id("mainData-mainEmail")), mail);
			type("55555", By.name("mainData[telephone][citycode]"));
			type("5555555", By.name("mainData[telephone][phonenumber]"));	
			type(contactLastName, By.name("mainData[contactPerson][contactLastName]"));
			type(contactFirstName, By.name("mainData[contactPerson][contactFirstName]"));	
			type(organisationName, By.name("paymentProperties[bankRecipient]"));
			type("04017360412314212414", By.name("paymentProperties[paymentAccount]"));
			type("040173604", By.name("paymentProperties[bik]"));	
			select("Нижегородская обл.", By.id("placement-subjectRfId"));
			type("Нижний Новгород г.", By.id("placement-cityOrArea"));
			type("Ленина пл.", By.id("placement-street"));
			type("12", By.id("placement-house"));	
			type("23423524525", By.id("placement-okato"));
			type("123456", By.id("placement-postCode"));		
			jsClick(By.id("correspondencePostAddress-samePlacement-1"));		
			type("55555", By.name("identicalData[telephone][citycode]"));
			type("5555555", By.name("identicalData[telephone][phonenumber]"));	
			type("10000000.00", By.id("registerDocuments-maxSumm"));	
			loadFile(By.id("registerDocuments-documents-0-title"), By.id("registerDocuments-documents-0-file"), By.cssSelector("button.btn"));
			loadFile(By.id("registerDocuments-documents2-0-title"), By.id("registerDocuments-documents2-0-file"), By.xpath("(//button[@id=''])[3]"));
			loadFile(By.id("registerDocuments-documents4-0-title"), By.id("registerDocuments-documents4-0-file"), By.xpath("(//button[@id=''])[7]"));
			loadFile(By.id("registerDocuments-documents5-0-title"), By.id("registerDocuments-documents5-0-file"), By.xpath("(//button[@id=''])[9]"));
			loadFile(By.id("registerDocuments-documents6-0-title"), By.id("registerDocuments-documents6-0-file"), By.xpath("(//button[@id=''])[11]"));
			this.loginForNewSupplierUL = loginForNewSupplier;
			this.passwordForNewSupplierUL = passwordForNewSupplier;
		}
		

		if (orgRole.equalsIgnoreCase("Индивидуальный предприниматель")){
			type("17.02.2017", By.id("mainData-registrationDate"));
			type(mail, By.id("mainData-mainEmail"));
			type(randomDigits(5), By.name("mainData[telephone][citycode]"));
			type(randomDigits(7), By.name("mainData[telephone][phonenumber]"));	
			type(contactLastName, By.name("mainData[contactPerson][contactLastName]"));
			type(contactFirstName, By.name("mainData[contactPerson][contactFirstName]"));	
			type(organisationName, By.name("paymentProperties[bankRecipient]"));
			type(randomDigits(20), By.name("paymentProperties[paymentAccount]"));
			type("040173604", By.name("paymentProperties[bik]"));
			select("Нижегородская обл.", By.id("placement-subjectRfId"));
			type("Нижний Новгород г.", By.id("placement-cityOrArea"));
			type("Ленина пл.", By.id("placement-street"));
			type("12", By.id("placement-house"));	
			type("23423524525", By.id("placement-okato"));
			type("123456", By.id("placement-postCode"));		
			jsClick(By.id("correspondencePostAddress-samePlacement-1"));
			type(randomDigits(5), By.name("identicalData[telephone][citycode]"));
			type(randomDigits(7), By.name("identicalData[telephone][phonenumber]"));
			loadFile(By.id("registerDocuments-documents-0-title"), By.id("registerDocuments-documents-0-file"), By.cssSelector("button.btn"));
			loadFile(By.id("registerDocuments-documents2-0-title"), By.id("registerDocuments-documents2-0-file"), By.xpath("(//button[@id=''])[3]"));
			this.loginForNewSupplierIP = loginForNewSupplier;
			this.passwordForNewSupplierIP = passwordForNewSupplier;
		}
		
		
		if (orgRole.equalsIgnoreCase("Физическое лицо")){
			type(randomDigits(11), By.id("mainData-snils"));
			type(mail, By.id("mainData-mainEmail"));
			type(randomDigits(5), By.name("mainData[telephone][citycode]"));
			type(randomDigits(7), By.name("mainData[telephone][phonenumber]"));	
			type(contactLastName, By.name("mainData[contactPerson][contactLastName]"));
			type(contactFirstName, By.name("mainData[contactPerson][contactFirstName]"));
			type(organisationName, By.name("paymentProperties[bankRecipient]"));
			type(randomDigits(20), By.name("paymentProperties[paymentAccount]"));
			type("040173604", By.name("paymentProperties[bik]"));
			select("Нижегородская обл.", By.id("placement-subjectRfId"));
			type("Нижний Новгород г.", By.id("placement-cityOrArea"));
			type("Ленина пл.", By.id("placement-street"));
			type("12", By.id("placement-house"));	
			type("23423524525", By.id("placement-okato"));
			type("123456", By.id("placement-postCode"));		
			jsClick(By.id("correspondencePostAddress-samePlacement-1"));
			type(randomDigits(5), By.name("identicalData[telephone][citycode]"));
			type(randomDigits(7), By.name("identicalData[telephone][phonenumber]"));
			loadFile(By.id("registerDocuments-documents-0-title"), By.id("registerDocuments-documents-0-file"), By.cssSelector("button.btn"));
			this.loginForNewSupplierFL = loginForNewSupplier;
			this.passwordForNewSupplierFL = passwordForNewSupplier;
		}
		
		
		jsClick(By.id("save-organisation"));
		waitForInvisibility(By.id("save-organisation"));
		waitForText("Подтверждение адреса электронной почты", By.cssSelector("h3"));
		
		getSecureCode();
		
		open(baseUrl+"/account/registration/confirm/code/"+ emailSecureCode);
		type(loginForNewSupplier, By.id("login"));
		type(passwordForNewSupplier, By.id("password"));
		jsClick(By.id("send"));
		waitForText("Ваша заявка принята на рассмотрение", By.cssSelector("h1"));
		
		operatorLogin();
		
		open(baseUrl + "/organisation/request/supplier/inn/" + certificateInn + "/registrationStatusId/1144,1008/");
		click(By.cssSelector(".grid-action.action-accreditate"));
		select(operatorCerificateName, By.id("organisationView-certificate"));
		jsClick(By.id("confirm"));
		jsClick(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus"));
		waitForInvisibility(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus"));
		//waitForElementPresent(By.cssSelector(".floatLeft.content>div"));		
		assertTrue(driver.findElement(By.cssSelector(".floatLeft.content>div")).getText().contains("Заявка успешно подтверждена."));

		open(demoUrl + "/logout");
		open(baseUrl+"/admin/account/access/login-ecp/");
		select(certificateName, By.id("login-certificate"));
		jsClick(By.id("login-button"));
		waitForAccreditationUserPresentInDataBase44();
	
		switch (orgRole) {
		case "Юридическое лицо": giveMoneyForSupplier("UL");break;
		case "Индивидуальный предприниматель": giveMoneyForSupplier("IP");break;
		case "Физическое лицо": giveMoneyForSupplier("FL"); break;
		}
	}

	
	protected void accreditationSupplierULIG(String certificateName) throws Exception {
		
		getCertificateData (certificateName);
		
		String operatorCerificateName = "Оператор Оператор Оператор (до: Sat May 13 13:08:27 UTC+0300 2017)";
		
		//Почта для подтверждения 
		String mail = "finsky@platformasoft.ru";
		this.loginForNewSupplier = "TestLogin" + randomDigits(4) + "!";
		print(loginForNewSupplier);
		this.passwordForNewSupplier = "Qwerty12!";
		String contactLastName = certificateName.substring(0,certificateName.indexOf(" "));//"Иванов";
		String contactFirstName = certificateName.substring(certificateName.indexOf(" ")+1, certificateName.indexOf(" ", certificateName.indexOf(" ")+1));//"Жора";	
		
		open(demoUrl + "/logout");
		open(baseUrl + "/account/registration/introduction/formType/supplier/");
		select("Юридическое лицо (иностранное государство)", By.id("type"));
		driver.findElement(By.id("login")).sendKeys(loginForNewSupplier);
	    driver.findElement(By.id("password")).sendKeys(passwordForNewSupplier);
	    driver.findElement(By.id("passwordRepeat")).sendKeys(passwordForNewSupplier);
	    driver.findElement(By.id("codePhrase")).sendKeys(loginForNewSupplier);
		driver.findElement(By.id("email")).sendKeys(mail);
	    select(certificateName, By.id("certificate"));
		click(By.id("save-introduction"));
		type(loginForNewSupplier, By.id("login-login"));
		type(passwordForNewSupplier, By.id("login-password"));
		click(By.cssSelector(".btn.btn-default"));			
		
		type(orgRole, By.id("mainData-formOfIncorporation"));
		type(mail, By.id("mainData-mainEmail"));
		type(randomDigits(5), By.name("mainData[telephone][citycode]"));
		type(randomDigits(7), By.name("mainData[telephone][phonenumber]"));
		type(contactLastName, By.name("mainData[contactPerson][contactLastName]"));
		type(contactFirstName, By.name("mainData[contactPerson][contactFirstName]"));
		type(organisationName, By.id("paymentProperties-bankRecipient"));
		type(randomDigits(11), By.id("paymentProperties-paymentAccount"));
		type("Иностранный Банк", By.id("paymentProperties-scoreOrganistaionTitle"));
		type("Бейкер стрит, 222б", By.id("paymentProperties-scoreOrganistaionAddress"));
		type("Британия", By.id("placement-country"));
		type("Бейкер стрит, 222б", By.id("placement-address"));
		jsClick(By.id("correspondencePostAddress-samePlacement-1"));
		type(randomDigits(5), By.name("identicalData[telephone][citycode]"));
		type(randomDigits(7), By.name("identicalData[telephone][phonenumber]"));
		type("10000000",By.id("registerDocuments-maxSumm"));
		loadFile(By.id("registerDocuments-documents3-0-title"), By.id("registerDocuments-documents3-0-file"), By.xpath("(//button[@id=''])[5]"));
		
		this.loginForNewSupplierULIG = loginForNewSupplier;
		this.passwordForNewSupplierULIG = passwordForNewSupplier;
		
		click(By.id("save-organisation"));
		waitForText("Подтверждение адреса электронной почты", By.cssSelector("h3"));
		
		getSecureCode();
		
		open(baseUrl+"/account/registration/confirm/code/"+ emailSecureCode);
		type(loginForNewSupplier, By.id("login"));
		type(passwordForNewSupplier, By.id("password"));
		click(By.id("send"));
		waitForText("Ваша заявка принята на рассмотрение", By.cssSelector("h1"));
		
		operatorLogin();
		
		open(baseUrl + "/organisation/request/supplier/inn/" + certificateInn + "/registrationStatusId/1144,1008/");
		click(By.cssSelector(".grid-action.action-accreditate"));
		select(operatorCerificateName, By.id("organisationView-certificate"));
		click(By.id("confirm"));
		click(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus"));	
		assertTrue(driver.findElement(By.cssSelector(".floatLeft.content>div")).getText().contains("Заявка успешно подтверждена."));

		open(demoUrl + "/logout");
		open(baseUrl+"/admin/account/access/login-ecp/");
		select(certificateName, By.id("login-certificate"));
		click(By.id("login-button"));
		waitForAccreditationUserPresentInDataBase44();
		giveMoneyForSupplier("ULIG");
	}
	
	
	protected void setCertificateIssuerForCustomer (String certificateIssuer) throws Exception {
		Connection con = null;
	    Statement stmt = null;
	    ResultSet result = null;
	    try 
	    {
	    	con = DriverManager.getConnection(urlAuction, loginForDataBase, passwordForDataBase);
	    	stmt = con.createStatement();
	  	    stmt.executeUpdate("UPDATE " + baseName94 + ".user SET certificateIssuer='" + certificateIssuer + "' WHERE certificateSerial='" + certificateSerial + "';");
	  	    print("CertificateIssuer94 ok");

	  	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	  	    int i=0;
	  	    int howManyTimes = 60;
	  	    while (i < howManyTimes) 
	  	    {
		  	    stmt = con.createStatement();
	  	    	result = stmt.executeQuery("select * from sectionks.organizationMember where certificateSerial = '" + certificateSerial + "';");
	  	    	if (result.next() == false)
	  	    	{Thread.sleep(5000);
	  	    	i++;
	  	    	}
	  	    	else {stmt.executeUpdate("UPDATE sectionks.organizationMember SET certificateIssuer='" + certificateIssuer + "' where certificateSerial = '" + certificateSerial + "';");break;}
	  	    }
	  	    result = stmt.executeQuery("select * from sectionks.organizationMember where certificateSerial = '" + certificateSerial + "';");
	  	    if ((i==howManyTimes) & (result.next() == false)) {print("Данные заказчика не перенеслись в секцию 44.");};
	  	    assertTrue(!(i==howManyTimes));
	  	    print("CertificateIssuer44 ok");
	  	   
	  	   con.close();
		   stmt.close();
		   result.close();
		  } 
		    catch (SQLException sqlEx) 
		  {
		    sqlEx.printStackTrace();
		  }
	}
	
	protected void accreditationCustomer (String certificateName) throws Exception {
		getCertificateData(certificateName);
		
		operatorLogin();
		open(baseUrl + "/integration/events/import");
		waitForElementPresent(By.id("content"));
		
		LocalDateTime time = LocalDateTime.now();
		String IDPack = randomDigits(36);
		String dateToday = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +  "T" + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"))+"+03:00";
		this.eisNumberNewCustomer = randomDigits(11);
		this.loginForNewCustomer = "customer" + randomDigits(4);
		this.passwordForNewCustomer = this.customerPassword;
		String organizationCodeRole="CU";
		
		switch (orgRole) {
		case "Заказчик": organizationCodeRole = "CU"; break;
		case "Уполномоченный орган": organizationCodeRole = "RA"; break;
		case "Уполномоченное учреждение": organizationCodeRole = "AI"; break;
		case "Специализированная организация": organizationCodeRole = "SO"; break;
		}
		
		
		
		
		String customerXmlForLoad = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><masterData xmlns=\"http://zakupki.gov.ru/oos/integration/1\" xmlns:ns2=\"http://zakupki.gov.ru/oos/types/1\"><index><id>" + IDPack + "</id><sender>OOC</sender><receiver>ETP_MMVB</receiver><createDateTime>" + dateToday + "</createDateTime><objectType>MD</objectType><indexNum>1</indexNum><signature type=\"CAdES-BES\"/><mode>PROD</mode></index><data schemeVersion=\"7.1\"><ns2:nsiOrganization><ns2:regNumber>" + eisNumberNewCustomer + "</ns2:regNumber><ns2:shortName>" + organisationName + "</ns2:shortName><ns2:fullName>" + organisationName + "</ns2:fullName><ns2:factualAddress><ns2:OKATO>65476000000</ns2:OKATO><ns2:addressLine>Российская Федерация, 622022, Нижегородская обл, Нижний Новгород г, Мира, 31, -</ns2:addressLine><ns2:building>31</ns2:building><ns2:country><ns2:countryCode>643</ns2:countryCode><ns2:countryFullName>Российская Федерация</ns2:countryFullName></ns2:country><ns2:filledManually>false</ns2:filledManually><ns2:office>-</ns2:office><ns2:region><ns2:kladrType>R</ns2:kladrType><ns2:kladrCode>66000000000</ns2:kladrCode><ns2:fullName>Нижегородская обл</ns2:fullName></ns2:region><ns2:city><ns2:kladrType>C</ns2:kladrType><ns2:kladrCode>66000023000</ns2:kladrCode><ns2:fullName>Нижний Новгород г</ns2:fullName></ns2:city><ns2:shortStreet>Мира</ns2:shortStreet><ns2:zip>622022</ns2:zip></ns2:factualAddress><ns2:postalAddress>Российская Федерация, 622022, Нижегородская обл, Нижний Новгород г, Мира, 31, -</ns2:postalAddress><ns2:email>finsky@platformasoft.ru</ns2:email><ns2:phone>7-45345-3453453</ns2:phone><ns2:fax>7-3435-243510</ns2:fax><ns2:contactPerson><ns2:lastName>" + certificateUserLastName + "</ns2:lastName><ns2:firstName>" + certificateUserFirstName + "</ns2:firstName><ns2:middleName>" + certificateUserMiddleName + "</ns2:middleName></ns2:contactPerson><ns2:accounts><ns2:account><ns2:bankAddress>г.Казань</ns2:bankAddress><ns2:bankName>ГРКЦ НБ РТ</ns2:bankName><ns2:bik>049205001</ns2:bik><ns2:paymentAccount>40701810692053000045</ns2:paymentAccount><!--<ns2:corrAccount>30101810000000000805</ns2:corrAccount>--><ns2:personalAccount>ЛБВ308007684</ns2:personalAccount></ns2:account>		    </ns2:accounts>		   <ns2:INN>" + certificateInn + "</ns2:INN><ns2:KPP>310201001</ns2:KPP><ns2:OGRN>" + certificateOgrn + "</ns2:OGRN><ns2:OKOPF><ns2:code>20903</ns2:code><ns2:fullName>Бюджетные учреждения</ns2:fullName></ns2:OKOPF><ns2:OKPO>83421124</ns2:OKPO><ns2:OKVED>80.10.3</ns2:OKVED><ns2:organizationRole>" + organizationCodeRole + "</ns2:organizationRole><ns2:organizationType><ns2:code>03</ns2:code><ns2:name>бюджетное учреждение</ns2:name></ns2:organizationType><ns2:subordinationType>3</ns2:subordinationType><ns2:timeZone>2</ns2:timeZone><ns2:timeZoneUtcOffset>UTC+03:00</ns2:timeZoneUtcOffset><ns2:timeZoneOlson>Europe/Moscow</ns2:timeZoneOlson><ns2:actual>true</ns2:actual><ns2:register>true</ns2:register></ns2:nsiOrganization><ns2:nsiUser><ns2:login>" + loginForNewCustomer + "</ns2:login><ns2:password>" + passwordForNewCustomer + "</ns2:password><ns2:firstName>" + certificateUserFirstName + "</ns2:firstName><ns2:middleName>" + certificateUserMiddleName + "</ns2:middleName><ns2:lastName>" + certificateUserLastName + "</ns2:lastName><ns2:codePhrase>sidorov01!</ns2:codePhrase><ns2:position>Директор</ns2:position><ns2:phone>7-777-7916831</ns2:phone><ns2:email>sidorov@test.ru</ns2:email><ns2:organization><ns2:regNum>" + eisNumberNewCustomer + "</ns2:regNum><ns2:fullName>" + organisationName + "</ns2:fullName></ns2:organization><ns2:certificateSN>" + certificateSerial + "</ns2:certificateSN><ns2:esIssuerDN>" + certificateIssuer + "</ns2:esIssuerDN><ns2:esIssuerSN>40401076ed18649b1a3c3ac250a09b55</ns2:esIssuerSN><ns2:userOrganizationRole>CU</ns2:userOrganizationRole><ns2:status>A</ns2:status></ns2:nsiUser></data></masterData>";
	    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", driver.findElement(By.id("content")), customerXmlForLoad);
	    driver.findElement(By.id("send")).click();
	    
	    open(baseUrl + "/integration/events/index/packageId/" + IDPack);
	    waitForElementPresentRefresh(By.xpath("//td[contains(.,'Обработано успешно')]"));
	    waitForAccreditationUserPresentInDataBase44();
	    setCertificateIssuerForCustomer(certificateIssuer);
	    open(demoUrl + "/logout");
		open(baseUrl+"/admin/account/access/login-ecp/");
		select(certificateName, By.id("login-certificate"));
		jsClick(By.id("login-button"));
		setCustomerPasswordSQLDB();
	}
	
	
	private void setCustomerPasswordSQLDB() throws InterruptedException {
		Connection con = null;
	    Statement stmt = null;
	    ResultSet result = null;
	    try 
	    {
	    	con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	  	    int i=0;
	  	    int howManyTimes = 60;
	  	    while (i < howManyTimes) 
	  	    {
		  	    stmt = con.createStatement();
	  	    	result = stmt.executeQuery("select * from sectionks.user where certificateSerial = '" + certificateSerial + "';");
	  	    	if (result.next() == false) 
	  	    	{Thread.sleep(5000);
	  	    	i++;}
	  	    	else {
	  	    		
	  	    		result = stmt.executeQuery("select sha1('" + customerPassword + "')");
	  	    		result.next();
	  	    		String sqlPassBD = result.getString(1);
	  	    		print(sqlPassBD);
	  	    		con.close();
	  	    		
	  	    		con = DriverManager.getConnection(urlAuction, loginForDataBase, passwordForDataBase);
	  	    		stmt = con.createStatement();
	  	    		stmt.executeUpdate("update " + baseName94 + ".user SET password='" + sqlPassBD + "' WHERE certificateSerial = '" + certificateSerial + "';");
	  	    		con.close();
	  	    		
	  	    		Thread.sleep(20000);
	  	    		con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	  	    		stmt = con.createStatement();
	  	    		stmt.executeUpdate("update sectionks.user SET password='" + sqlPassBD + "' WHERE certificateSerial = '" + certificateSerial + "';");
	  	    		print("44jr");
	  	    		con.close();

	  	    		break;
	  	    	}
	  	    }
	  	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	  	    stmt = con.createStatement();
	  	    result = stmt.executeQuery("select * from sectionks.user where certificateSerial = '" + certificateSerial + "';");
	  	    if ((i==howManyTimes) & (result.next() == false)){print("Данные участника не перенеслись в секцию 44.");}
	  	    assertTrue(!(i==howManyTimes));
	  	    
	  	  con.close();
		  stmt.close();
		  result.close();
		  } 
		    catch (SQLException sqlEx) 
		  {
		    sqlEx.printStackTrace();
		  }
	}

	protected void waitForAccreditationUserPresentInDataBase44 () throws Exception {
		Connection con = null;
	    Statement stmt = null;
	    ResultSet result = null;
	    try 
	    {
	    	con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	  	    int i=0;
	  	    int howManyTimes = 60;
	  	    while (i < howManyTimes) 
	  	    {
		  	    stmt = con.createStatement();
	  	    	result = stmt.executeQuery("select * from sectionks.user where certificateSerial = '" + certificateSerial + "';");
	  	    	if (result.next() == false) 
	  	    	{Thread.sleep(5000);
	  	    	i++;}
	  	    	else break;
	  	    }
	  	    if ((i==howManyTimes) & (result.next() == false)){print("Данные участника не перенеслись в секцию 44.");}
	  	    assertTrue(!(i==howManyTimes));
	  	    
	  	    print("44ок");
	  	    
	  	  con.close();
		  stmt.close();
		  result.close();
		  } 
		    catch (SQLException sqlEx) 
		  {
		    sqlEx.printStackTrace();
		  }
	}
	
	protected void deleteUserSignature(String certificateName) throws Exception {
		
		getCertificateData (certificateName);
		
		//certificateSerial = "12:00:1B:19:B6:9E:1E:9A:32:33:17:0B:36:00:00:00:1B:19:B6";
		//orgRole = "Индивидуальный предприниматель";

		Connection con;
	    Statement stmt;
	    ResultSet result;
	
	    //String query = "select * from sectionks.user where certificateSerial = '" + certificateSerial + "';";
	    try 
	    {
	    con = DriverManager.getConnection(urlSectionks, loginForDataBase, passwordForDataBase);
	    stmt = con.createStatement();
	    
	    result = stmt.executeQuery("select * from sectionks.user where certificateSerial = '" + certificateSerial + "';");
	    if (result.next() == true ) 
	    {
	    	stmt.executeUpdate("UPDATE sectionks.user SET certificateSerial='null' where certificateSerial = '" + certificateSerial + "';");
	    }
	    
	    result = stmt.executeQuery("select * from sectionks.organizationMember where certificateSerial = '" + certificateSerial + "';");
	    if (result.next() == true ) 
	    {
	    	stmt.executeUpdate("UPDATE sectionks.organizationMember SET certificateSerial='null' where certificateSerial = '" + certificateSerial + "';");
	    }
	    
	    //Для ИП дополнительно проверяется совпадение ИНН, его тоже удаляем из БД
	    if (orgRole.equalsIgnoreCase("Индивидуальный предприниматель")){
	    	result = stmt.executeQuery("select * from sectionks.organization where inn = '" + certificateInn + "';");
	    	if (result.next() == true){
	    		stmt.executeUpdate("UPDATE sectionks.organization SET inn='null' where inn = '" + certificateInn + "';");
	    	}
	    }
	    
	    
	    //подключение к БД 94 
	    con = DriverManager.getConnection(urlAuction, loginForDataBase, passwordForDataBase);
	    stmt = con.createStatement();
	    
	    //поиск по номеру сертификата
	    result = stmt.executeQuery("select * from " + baseName94 + ".user where certificateSerial = '" + certificateSerial + "';");
	    if (result.next() == true ) 
	    {
	    	stmt.executeUpdate("UPDATE " + baseName94 + ".user SET certificateSerial='null' where certificateSerial = '" + certificateSerial + "';");
	    }
	    result = stmt.executeQuery("select * from " + baseName94 + ".certificate where serial = '" + certificateSerial + "';");
	    if (result.next() == true ) 
	    {
	    	stmt.executeUpdate("UPDATE " + baseName94 + ".certificate SET serial='null' where serial = '" + certificateSerial + "';");
	    }
	    
	  //Для ИП дополнительно проверяется совпадение ИНН, его тоже удаляем из БД
	    if (orgRole.equalsIgnoreCase("Индивидуальный предприниматель")){
	    	result = stmt.executeQuery("select * from " + baseName94 + ".organisation where inn = '" + certificateInn + "';");
	    	if (result.next() == true) {
	    		stmt.executeUpdate("UPDATE " + baseName94 + ".organisation SET inn='null' where inn = '" + certificateInn + "';");
	    	}	
	    }
	    con.close();
	    stmt.close();
	    result.close();
	    } 
	    catch (SQLException sqlEx) 
	    {
	    sqlEx.printStackTrace();
	    }
	}
	
	
	private void getSecureCode() {
		final Connection con;
	    final Statement stmt;
	    final ResultSet result;
	
	    //String query = "SELECT emailSecureCode FROM auction.organisation where fullltitle='АО «Участник 01»';";
	    String query = "SELECT emailSecureCode FROM " + baseName94 + ".organisation where inn='"+ certificateInn +"' order by id desc;";
	    
	    try {
	    con = DriverManager.getConnection(urlAuction, loginForDataBase, passwordForDataBase);
	   
	    /* if(con != null) 
            System.out.println("Connection Successful !\n");
	    else
            System.exit(0);
	    */
	    
	    stmt = con.createStatement();
	    result = stmt.executeQuery(query);
	    result.next();
	    
	    //int idIndex = result.findColumn("name"); 
	    //int idnum = result.getInt("id");
	    
	    this.emailSecureCode = result.getString("emailSecureCode"); 
	    
	    System.out.println("emailSecureCode: "+emailSecureCode);
	    //System.out.println(idnum);
	    
	    con.close();
	    stmt.close();
	    result.close();
	    
	    } catch (SQLException sqlEx) {
	    	sqlEx.printStackTrace();
	    } 
	}
	}

