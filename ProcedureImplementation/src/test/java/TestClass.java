package test.java;

import java.util.regex.Pattern;
import java.awt.AWTException;
import java.util.Date;
import java.util.List;

//import static org.hamcrest.CoreMatchers.*;
import static org.testng.AssertJUnit.assertTrue;

//import org.junit.AfterClass;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.thoughtworks.selenium.ScreenshotListener;
//import testMethods;

public class TestClass extends TestMethodBase {

	@Test
	  //№0	Публикация извещения	
	  public void procedureScenario00 () throws Exception {
		
		//LoadCoopProcedure();
		loadProcedure();                        //Загрузка процедуры
		//editProcedure();
		
		getRequestSupplier("U1"); 
		getRequestSupplier("U2");

		requestTimeEndSQL();                 //перевод времени окончания подачи преложений на "сейчас" 
	
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		
		auctionStartSQL();                        // Перевод времени начала аукциона на "сейчас"
		
		getOfferAuctionSupplier("U1");                //Подать ЦП в аукцион
		getOfferAuctionSupplier("U2");                  //Подать ЦП в аукцион

		auctionEndSQL();                       //окончание аукциона

		finalProtocolAllAdmittedCustomer("Z1");
		contractProtocolPublicationCustomer("Z1");          //направление контракта участнику
		//сontractRejectionSupplier(2);                    //подписание протокола разногласий участником
		//contractProtocolPublicationCustomer(2);                    
		сontractSubscribeSupplier("U2");                    //Подписание контракта участником
		contractSubscribeTimeStartForCustomerSQL();      //перевод времени подисания заказчиком на "сейчас" 
		сontractSubscribeCustomer("Z2");                    //Подписание контракта заказчиком
	  }
	
	@Test
	//аккредитация участника 
	public void accreditation() throws Exception {
		
		//deleteUserSignature("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)"); Филоненко Евгения Евгеньевна (до: Tue Sep 17 16:20:39 UTC+0300 2019)
		//accreditationSupplier("Фомичев Филипп Филиппович (до: Thu Jul 6 17:18:51 UTC+0300 2017)");
		
		
		
		//deleteUserSignature("Германов Владимир Васильевич (до: Wed Jul 5 14:52:53 UTC+0300 2017)");
		//deleteUserSignature("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)");
		//deleteUserSignature("Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017)");
		//deleteUserSignature("Чеканкин Федор Леонидович (до: Thu Jul 6 11:36:14 UTC+0300 2017)");	
		//deleteUserSignature("Заказов Петр Валерьянович (до: Thu Jul 6 14:01:01 UTC+0300 2017)");
		

		//deleteUserSignature("Фомичев Филипп Филиппович (до: Thu Jul 6 17:18:51 UTC+0300 2017)");
		//deleteUserSignature("Менделеев Федор Константинович (до: Mon Jul 10 12:24:04 UTC+0300 2017)");
		
		//accreditationCustomer("Заказов Петр Валерьянович (до: Thu Jul 6 14:01:01 UTC+0300 2017)");
		//deleteUserSignature("Заказов Петр Валерьянович (до: Thu Jul 6 14:01:01 UTC+0300 2017)");
		//accreditationSupplier("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)");
		//accreditationCustomer("Головачева Марина Евгеньевна (до: Thu Jun 15 12:04:34 UTC+0300 2017)");
		//accreditationCustomer("Савинков Фома Фомич (до: Thu Jun 15 11:09:50 UTC+0300 2017)");
		
		
		//loadProcedureNewCustomer();
		
		accreditationSupplier("Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017)");
		accreditationSupplier("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)");
		accreditationSupplierULIG("Германов Владимир Васильевич (до: Wed Jul 5 14:52:53 UTC+0300 2017)");
		accreditationSupplier("Чеканкин Федор Леонидович (до: Thu Jul 6 11:36:14 UTC+0300 2017)");
		
		
		
		getRequestSupplier("IP");
		getRequestSupplier("FL");
		getRequestSupplier("UL");
		getRequestSupplier("ULIG");
		
		requestTimeEndSQL();
		
		requestProtocolAllAdmittedCustomer("newCustomer");
		auctionStartSQL();
		
		getOfferAuctionSupplier("FL");
		getOfferAuctionSupplier("IP");
		getOfferAuctionSupplier("ULIG");
		getOfferAuctionSupplier("UL");

		
		auctionEndSQL();
		finalProtocolAllAdmittedCustomer("newCustomer");
		contractProtocolPublicationCustomer("newCustomer");          //направление контракта участнику        
		сontractSubscribeSupplier("UL");                    //Подписание контракта участником
		contractSubscribeTimeStartForCustomerSQL();      //перевод времени подисания заказчиком на "сейчас" 
		сontractSubscribeCustomer("newCustomer");
		
		//loginUser("Z1");
		
		/*
		loadProcedure();   //Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017) ЮЛ "Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)"
		deleteUserSignature("Германов Владимир Васильевич (до: Wed Jul 5 14:52:53 UTC+0300 2017)");
		deleteUserSignature("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)");
		deleteUserSignature("Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017)");
		deleteUserSignature("Чеканкин Федор Леонидович (до: Thu Jul 6 11:36:14 UTC+0300 2017)");
		accreditationSupplier("Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017)");
		accreditationSupplier("Глазунова Юлия (до: Sat May 20 11:30:41 UTC+0300 2017)");
		accreditationSupplierULIG("Германов Владимир Васильевич (до: Wed Jul 5 14:52:53 UTC+0300 2017)");
		accreditationSupplier("Чеканкин Федор Леонидович (до: Thu Jul 6 11:36:14 UTC+0300 2017)");
		//giveMoneyForSupplier("0");
		getRequestSupplier("FL");
		getRequestSupplier("IP");
		getRequestSupplier("ULIG");
		getRequestSupplier("UL");
		requestTimeEndSQL();
		requestProtocolAllAdmittedCustomer("Z1");
		auctionStartSQL();
		getOfferAuctionSupplier("FL");
		getOfferAuctionSupplier("IP");
		getOfferAuctionSupplier("ULIG");
		getOfferAuctionSupplier("UL");
		auctionEndSQL();
		finalProtocolAllAdmittedCustomer("Z1");
		contractProtocolPublicationCustomer("Z2");          //направление контракта участнику                   
		сontractSubscribeSupplier("UL");                    //Подписание контракта участником
		contractSubscribeTimeStartForCustomerSQL();      //перевод времени подисания заказчиком на "сейчас" 
		сontractSubscribeCustomer(2);
		
		
	//deleteUserSignature("Григорьев Валентин Петрович (до: Fri Jun 30 18:00:10 UTC+0300 2017)");*/
	}
	
	@Test
	public void checkOrdinaryForTables() throws Exception {

		//checkSortOrderForOrg();
		//checkSortOrderForContractPrice();
		checkSortOrderForDate("Дата и время публикации");
		checkSortOrderForDate("Дата и время окончания подачи заявок");
		checkSortOrderForDate("Дата и время начала торгов");
	}
	
	
	private void cancelRequestSupplier(String numberOfSupplier) throws Exception {
		loginUser(numberOfSupplier);
		open(demoUrl + "/procedure/catalog/procedures/personal?q=" + procedureNumber  + "&simple-search=Найти");
		waitForElementPresentRefresh(By.cssSelector(".ps-grid-action.action-request-refuse"));
		click(By.cssSelector(".ps-grid-action.action-request-refuse"));
		click(By.name("send"));
		waitForText("Заявка успешно отозвана", By.cssSelector("h1"));
	}

	@Test
	public void procedureLoadEditBlockCancel() throws Exception {
		print("hi");
		loadProcedure();
		editProcedure();
		blockProcedure();
		cancelProcedure();
	}

@Test
public void eisProcedure () throws AWTException, InterruptedException{
	eis();
	
}


	
	@Test
	  //№1	Публикация извещения	Заявок не подано	Торги не состоялись
	  public void procedureScenario01 () throws Exception {

		//loadProcedure();                 //Загрузка процедуры
		//Thread.sleep(10000);    
		getProcedureID();
		Thread.sleep(10000);
		requestTimeEndSQL();       //перевод времени окончания подачи преложений на "сейчас"
		finalProtocolNoRequestCustomer("Z1");  //Публикация протокола рассмотрения заявок - признание закупки несостоявшейся
		}


	@Test
	  //№2	Публикация извещения	Подана 1 заявка	Протокол рассмотрения единственной заявки
	  public void procedureScenario02 () throws Exception {
		//LoadCoopProcedure();
		loadProcedure();                  //Загрузка процедуры
		getProcedureID();                 //Взять из БД id процедуры
	    getRequestSupplier("1");  
		requestTimeEndSQL();         //перевод времени окончания подачи преложений на "сейчас"
		//customer01Login();                //Вход заказчика 1
		requestProtocolAllAdmittedCustomer("Z1");   //Публикация протокола рассмотрения единственной заявки
	  }
	  @Test
	  //№3	Публикация извещения	Подано 4 заявки	ПРЗ: все не допущены	Торги не состоялись
	  public void procedureScenario03 () throws Exception {
		//loadProcedure();                  //Загрузка процедуры
		
		getProcedureID();                 //Взять из БД id процедуры
		getRequestSupplier("U1");               //Вход участника 2
		getRequestSupplier("U2");
		//getRequest(9);
		//getRequest(4);

    	requestTimeEndSQL();         //перевод времени окончания подачи преложений на "сейчас"
		
    	loginUser("Z1");               //Вход заказчика 1
		przNotAllowedRequestPublication();   //Публикация протокола рассмотрения заявок - допущенных нет
	  }
	  @Test
	  //№4	Публикация извещения	Подано 4 заявки	ПРЗ: допущена одна заявка	ППИ: соответствует Заключение контракта
	  public void procedureScenario04 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getRequestSupplier("U1");                  //Вход участника 2
		getRequestSupplier("U2");
		//getRequest(3);
		//getRequest(4);  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		loginUser("Z1");                    //Вход заказчика 1
		przOneRequestAdmitted();             //Публикация протокола рассмотрения заявок - 1 допущенная заявка
		finalProtocolAllAdmittedCustomer("Z1");                  //ППИ - все соответствуют          
	  }
	  @Test
	  //№5	Публикация извещения	Подано 4 заявки	 ПРЗ: допущена одна заявка	ППИ: не соответствует	Торги не состоялись
	  public void procedureScenario05 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                  //Вход участника 2
		getRequestSupplier("U2");
		//getRequest(3);
		//getRequest(4);  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		loginUser("Z1");                    //Вход заказчика 1
		przOneRequestAdmitted();  //Публикация протокола рассмотрения заявок - 1 допущенная заявка
		finalNoAdmitted();                  //  ППИ все не соответствуют  
	  }
	  @Test
	  //№6	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Не подано ЦП	ППИ: соответствуют   Заключение контракта
	  public void procedureScenario06 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                       
		getRequestSupplier("U2");
		//getRequest(3);
		//getRequest(4);  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");    //Публикация протокола рассмотрения заявок - все допущены
		/*auctionStartNow();
		SQLauctionEnd();
		finalAllAdmitted();*/ 
	  }
	  
	  @Test
	  //№7	Публикация извещения	Подано 4 заявки	   ПРЗ: допущены все заявки	 Не подано ЦП  ППИ: не соответствует	Не состоялась
	  public void procedureScenario07 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                  //Вход участника 2
		getRequestSupplier("U2");
		//getRequest(3);
		//getRequest(4);  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		auctionEndSQL();
		finalNoAdmitted(); 
	  }
	  @Test
	  //№8	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 1 ЦП	ППИ: соответствует	Заключение контракта
	  public void procedureScenario08 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                  //Вход участника 2
		getRequestSupplier("U2");
		getRequestSupplier("U3");
		//getRequestSupplier("U4");  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("U1");
		auctionEndSQL();
		finalProtocolAllAdmittedCustomer("Z1"); 
	  }
	  @Test
	  //№9	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 1 ЦП	ППИ: не соответствует	Не состоялась
	  public void procedureScenario09 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                  //Вход участника 2
		getRequestSupplier("U2");
		getRequestSupplier("U3");
		//getRequest(4);  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("U1");
		auctionEndSQL();
		loginUser("Z1");
		finalNoAdmitted(); 
	  }
	  @Test
	  //№10	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 4 ЦП	ППИ: все соответствуют	Заключение контракта
	  public void procedureScenario10 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                       // подать заявку участником 1
		getRequestSupplier("U2");                       // подать заявку участником 2
		getRequestSupplier("U3");                       // подать заявку участником 3
		//getRequestSupplier("4");                       // подать заявку участником 4
		requestTimeEndSQL();            //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("U1");
		getOfferAuctionSupplier("U2");
		getOfferAuctionSupplier("U3");
		//getOfferAuctionSupplier("4");
		auctionEndSQL();
		finalProtocolAllAdmittedCustomer("Z1");
	  }
	  @Test
	  //№11	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 4 ЦП	ППИ: все не соответствуют	Не состоялась
	  public void procedureScenario11 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                       // подать заявку участником 1
		getRequestSupplier("U2");                       // подать заявку участником 2
		//getRequest(3);                       // подать заявку участником 3
		//getRequest(4);                       // подать заявку участником 4
		requestTimeEndSQL();            //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("U1");
		getOfferAuctionSupplier("U2");
		//getOfferAuction(3);
		//getOfferAuction(4);
		auctionEndSQL();
		loginUser("Z1");
		finalNoAdmitted(); 
	  }
	  @Test
	  //№12	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 4 ЦП	ППИ: победитель не соответствует, остальные соответствуют	Заключение контракта
	  public void procedureScenario12 () throws Exception {
		//loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getRequestSupplier("U1");                       // подать заявку участником 1
		getRequestSupplier("U2");                       // подать заявку участником 2
		//getRequestSupplier("U3");                       // подать заявку участником 3
		//getRequestSupplier("4");                       // подать заявку участником 4
		requestTimeEndSQL();            //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
	    getOfferAuctionSupplier("U1");// подать ЦП участником 1
		getOfferAuctionSupplier("U2");// подать ЦП участником 2
		//getOfferAuctionSupplier("3");// подать ЦП участником 3
		//getOfferAuctionSupplier("4");// подать ЦП участником 4
		auctionEndSQL();
		loginUser("Z1");
		finalWinnerNotAdmitted();            //ППИ победитель не соответствует, остальные соответствуют
	  }
	  @Test
	  //№13	Публикация извещения	Подано 4 заявки	ПРЗ: допущены все заявки	Подано 4 ЦП	ППИ: все соответствуют	Заключение контракта
	  public void procedureScenario13 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getCreditRequest("1");                       // подать заявку участником 1
		getCreditRequest("2");                       // подать заявку участником 2
		getCreditRequest("3");                       // подать заявку участником 3
		getCreditRequest("4");                       // подать заявку участником 4
		requestTimeEndSQL();            //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("1");
		getOfferAuctionSupplier("2");
		getOfferAuctionSupplier("3");
		getOfferAuctionSupplier("4");
		auctionEndSQL();
		finalProtocolAllAdmittedCustomer("Z1");
		checkReturnOfCreditMoney("1");
		checkReturnOfCreditMoney("2");
		checkReturnOfCreditMoney("3");
	  }
	  @Test
	  //№14	Публикация извещения	Подано 4 кредитные заявки,	ПРЗ: допущены все заявки	Подано 4 ЦП	ППИ: все не соответствуют	Не состоялась
	  public void procedureScenario14 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getCreditRequest("1");                       // подать заявку участником 1
		getCreditRequest("2");                       // подать заявку участником 2
		getCreditRequest("3");                       // подать заявку участником 3
		getCreditRequest("4");                       // подать заявку участником 4
		requestTimeEndSQL();            //перевод времени окончания подачи преложений на "сейчас"
		requestProtocolAllAdmittedCustomer("Z1");  //Публикация протокола рассмотрения заявок - все допущены
		auctionStartSQL();
		getOfferAuctionSupplier("1");
		getOfferAuctionSupplier("2");
		getOfferAuctionSupplier("3");
		getOfferAuctionSupplier("4");
		auctionEndSQL();
		loginUser("Z1");
		finalNoAdmitted();
		checkReturnOfCreditMoney("1");
		checkReturnOfCreditMoney("2");
		checkReturnOfCreditMoney("3");
		checkReturnOfCreditMoney("4");
	  }

	@Test
	  //№15	Публикация извещения	Подано 4 кредитные заявки	ПРЗ: допущена одна заявка	ППИ: соответствует Заключение контракта
	  public void procedureScenario15 () throws Exception {
		loadProcedure();                     //Загрузка процедуры
		getProcedureID();                    //Взять из БД id процедуры
		getCreditRequest("1");
		getCreditRequest("2");
		getCreditRequest("3");
		getCreditRequest("4");  
		requestTimeEndSQL();             //перевод времени окончания подачи преложений на "сейчас"
		loginUser("Z1");                    //Вход заказчика 1
		przOneRequestAdmitted();             //Публикация протокола рассмотрения заявок - 1 допущенная заявка
		finalProtocolAllAdmittedCustomer("Z1");                  //ППИ - все соответствуют
		checkReturnOfCreditMoney("2");
		checkReturnOfCreditMoney("3");
		checkReturnOfCreditMoney("4");          
	  }
	@Test
	public void SITE4492()throws Exception { //33847 МО: возможность прикрепления файла к запросу недостающих сведений
		loadProcedure();             //Загрузка процедуры
		getProcedureID();              //Взять из БД id процедуры
	    getRequestSupplier("1");                  // Подача заявки на участие
	    getRequestSupplier("2");                 // Подача заявки на участие
	    requestTimeEndSQL();      //перевод времени окончания подачи преложений на "сейчас"
	    requestProtocolAllAdmittedCustomer("Z1");  // Публикация протокола подведения итогов
	    auctionStartSQL();             // Перевод времени начала erwbjyf на "сейчас"
	    getOfferAuctionSupplier("1");              //Подать ЦП в аукцион
	    getOfferAuctionSupplier("2");              //Подать ЦП в аукцион
	    auctionEndSQL();                  //перевод времени окончания аукциона на "сейчас"
	    finalProtocolAllAdmittedCustomer("Z1");                 // публикация финального протокола
		contractProtocolPublicationCustomer("Z2");    //направление проекта контракта участнику
		сontractSubscribeSupplier("2");            //Подписание контракта участником
		loginUser("Z1");
		getContractInformationRequest();         //проверка возможности прикрепления файла к запросу недостающих сведений
	}
	@Test
	public void SITE4474()throws Exception { //открыть доступ к протоколу рассмотрению заявок сразу после публикации данного протокола
		loadProcedure();             //Загрузка процедуры
		getProcedureID();              //Взять из БД id процедуры
	    getRequestSupplier("1");                  // Подача заявки на участие
	    getRequestSupplier("2");                 // Подача заявки на участие
	    requestTimeEndSQL();      //перевод времени окончания подачи преложений на "сейчас"
	    loginUser("Z1");            //Вход заказчика 1
	    przOneRequestAdmitted();      // Публикация протокола подведения итогов
	    checkAccessFilePrz("0");             // проверка доступа к протоколу рассмотрения заявок
	    checkAccessFilePrz("1");
	    checkAccessFilePrz("2");
	    checkAccessFilePrz("3");
	    
	}
	@Test
	public void tradesOnLastOfferAllAllowed34442()throws Exception { //открыть доступ к протоколу рассмотрению заявок сразу после публикации данного протокола
		loadProcedure();             //Загрузка процедуры
		getProcedureID();              //Взять из БД id процедуры
	    getRequestSupplier("1");                  // Подача заявки на участие
	    getRequestSupplier("2");
	    getRequestSupplier("3");
	    getRequestSupplier("4");  // Подача заявки на участие
	    requestTimeEndSQL();      //перевод времени окончания подачи преложений на "сейчас"
	    requestProtocolAllAdmittedCustomer("Z1");
	    auctionStartSQL();
	    getOfferAuctionSupplier("1");
		getOfferAuctionSupplier("2");
		getOfferAuctionSupplier("3");
		getOfferAuctionSupplier("4");
		auctionEndSQL();
		blockProcedure();
	    prescriptionsToCustomerAllAllowed();
	    getFirstBestOfferSumm();
	    loginUser("Z1");  
	    customerPrescriptionExecution(); 
	    requestProtocolAllAdmittedCustomer("Z1");
	    auctionStartSQL();
	    checkLastOfferSummAllAllowed();
	}
	@Test
	public void tradesOnLastOffer1stNotAllAllowed34442()throws Exception { //открыть доступ к протоколу рассмотрению заявок сразу после публикации данного протокола
		loadProcedure();             //Загрузка процедуры
		getProcedureID();              //Взять из БД id процедуры
	    getRequestSupplier("1");                  // Подача заявки на участие
	    getRequestSupplier("2");
	    getRequestSupplier("3");
	    getRequestSupplier("4");  // Подача заявки на участие
	    requestTimeEndSQL();      //перевод времени окончания подачи преложений на "сейчас"
	    loginUser("Z1");
	    requestProtocolAllAdmittedCustomer("Z1");
	    auctionStartSQL();
	    getOfferAuctionSupplier("1");
		getOfferAuctionSupplier("2");
		getOfferAuctionSupplier("3");
		getOfferAuctionSupplier("4");
		auctionEndSQL();
		blockProcedure();
		prescriptionsToCustomer1stNotAllAllowed();
		getSecondBestOfferSumm();
		loginUser("Z1");  
	    customerPrescriptionExecution(); //Вход заказчика 1
	    requestProtocolAllAdmittedCustomer("Z1");
	    auctionStartSQL();
	    checkLastOfferSummAllAllowed();  
	}
	
	
	/*private void findParentFromElement() {
	String id = "1609060004";
	supplier02Login();
	open("https://ftest44.etp-micex.ru/credit/request/registry");
	//WebElement myElement = driver.findElement(By.id("myDiv"));
	//WebElement parent = myElement.findElement(By.xpath(".."));	
	WebElement parent = driver.findElement(By.xpath("//td[contains(.,'"+ id +"')]/.."));
	System.out.println(parent.getAttribute("id"));
	System.out.println(driver.findElement(By.cssSelector("#" + parent.getAttribute("id") + " > td.row-status ")).getText());
	//System.out.println(driver.findElement(By.cssSelector("#rowId-38 > td.row-status ")).getText());
	creditRequestStatus(creditRequestID);
}*/

}