package fw;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helpers {
	
	WebDriver driver;

	public Helpers (WebDriver driver) {
		this.driver = driver;
	}
	
	
	
	
	public void type(String string, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(string);
		/*waitForElementPresent(by);
		WebElement element = driver.findElement(by);
	    ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, string);*/
	}
	
	public void jsClick(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		wait.until(ExpectedConditions.elementToBeClickable(by));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", driver.findElement(by));
	} 
	
	protected void waitForElementPresentRefresh(By by) throws InterruptedException {
		int i=0;
		int howManyRefresh=50;
		
		while (i<howManyRefresh) {
			if (driver.findElements(by).size()==0)
			//if (isElementPresent(by)==false)
			{
			driver.navigate().refresh();
			i++;
			if (i==howManyRefresh) { System.out.println("Элемент " + by + " не найден.");}
			}
			else break; 
			} 
	}
	
	public void waitForElementPresent(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected void waitForText(String string, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, string));
	}

	public void open(String string) {
		driver.get(string);
	}
}
